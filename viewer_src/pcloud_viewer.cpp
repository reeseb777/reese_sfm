
#include "pcloud_viewer.h"
//#include "points_init/points_init.h"
PCloudViewer *PCloudViewer::instance;

/* Constructor:
	 * 	Initializes the class member variables.
	 */
PCloudViewer::PCloudViewer(){
		main_win=0;					// The main window identifier created by glut
		path="";
		subpath="";
		algorithm_state=0;
		visEyeRange = 5,			// Distance between eye and scene origin
		eyeSeparation=.15, 			// Distance between eyes in stereoscopic viewing mode
		eyeSepStep=.02, 			// Increment by which user can change eyeSeparation
		clipNear=0.01, 				// Distance from eye to near Z clip plane
		clipFar=100.0, 				// Distance from eye to far  Z clip plane
		visFOV=40.0;				// Angular Width of screen in degrees
		STEREO=0;					// Defines the stereoscopic viewing mode
									// -set to 0 for no stereo display
									// -set to 1 for quad-buffer stereoscopic output
									// -set to 2 for two-buffer stereoscopic output with the
									//	 right eye written to the right half and left eye
									//	 written to the left half of the screen
		spinning = 0,				// Indicates whether display is being scaled with the mouse (by holding SHIFT key)
		moving = 0; 				// Indicates whether display is being spun with the mouse
		W = 600, H = 600;			// Width and height of viewport
		newModel = 1;				// Indicates that model should be recomputed due to a perspective change
		scalefactor = 1.0;			// Storage for model scaling state

		// Location of each scene light -- Currently only light 0 is used (as an ambient source)
		lightPositions[0][0]=0.0;lightPositions[0][1]=1.0;lightPositions[0][2]=0.0;lightPositions[0][3]=0.0;
		lightPositions[1][0]=0.0;lightPositions[1][1]=-1.0;lightPositions[1][2]=0.0;lightPositions[1][3]=0.0;
		lightPositions[2][0]=1.0;lightPositions[2][1]=0.0;lightPositions[2][2]=0.0;lightPositions[2][3]=0.0;
		lightPositions[3][0]=-1.0;lightPositions[3][1]=0.0;lightPositions[3][2]=0.0;lightPositions[3][3]=0.0;
		lightPositions[4][0]=0.0;lightPositions[4][1]=0.0;lightPositions[4][2]=1.0;lightPositions[4][3]=0.0;
		lightPositions[5][0]=0.0;lightPositions[5][1]=0.0;lightPositions[5][2]=-1.0;lightPositions[5][3]=0.0;

		// Color of all lights
		lightsColor[0] = 1.0; lightsColor[1] = 1.0; lightsColor[2] = 1.0; lightsColor[3] = 1.0;

		// Normals of unit cube
		n[0][0]=-1; n[0][1]= 0; n[0][2]= 0;
		n[1][0]= 0; n[1][1]= 1; n[1][2]= 0;
		n[2][0]= 1; n[2][1]= 0; n[2][2]= 0;
		n[3][0]= 0; n[3][1]=-1; n[3][2]= 0;
		n[4][0]= 0; n[4][1]= 0; n[4][2]= 1;
		n[5][0]= 0; n[5][1]= 0; n[5][2]=-1;

		// Faces of unit cube (indices of v)
		faces[0][0]=0; faces[0][1]=1; faces[0][2]=2; faces[0][3]=3;
		faces[1][0]=3; faces[1][1]=2; faces[1][2]=6; faces[1][3]=7;
		faces[2][0]=7; faces[2][1]=6; faces[2][2]=5; faces[2][3]=4;
		faces[3][0]=4; faces[3][1]=5; faces[3][2]=1; faces[3][3]=0;
		faces[4][0]=5; faces[4][1]=6; faces[4][2]=2; faces[4][3]=1;
		faces[5][0]=7; faces[5][1]=4; faces[5][2]=0; faces[5][3]=3;

		// Color of camera visualization (front plane)
		cameraVisColor[0] = 0.8; cameraVisColor[1] = 0.9; cameraVisColor[2] = 0.15; cameraVisColor[3] = 0.5;
		// Color of camera visualization (lines)
		cameraLineColor[0]= 1.0; cameraLineColor[1]= 0.3; cameraLineColor[2]= 0.3;  cameraLineColor[3]= 1.0;

		camera_num=0;					// Number of drawn cameras
		camera_label_text="cam%d";		// customizable text label for cameras
		label_cameras=1;				// Indicates whether camera labels will be drawn
		cameraFlashPhase=0L;			// Current phase of blinking camera animation
		flashing=GL_FALSE;				// Indicates whether blinking animation is enabled
		showingHelpMenu=0;				// Indicates whether keyboard shortcuts are being drawn
		helpHintFade=2500;				// Timer for initial display fadeaway [units: milliseconds]

		// KEYBOARD SHORTCUT KEYS
		// Shortcut key codes
		shortcutKeys[0]=KEY_HELP;			// Keyboard h keycode
		shortcutKeys[1]=KEY_CAM_LABEL;		// Keyboard c keycode
		shortcutKeys[2]=KEY_QUIT;			// Keyboard escape keycode
		shortcutKeys[3]=KEY_FLASHING;		// Keyboard f keycode
		shortcutKeys[4]=KEY_STEREO;			// Keyboard s keycode
		shortcutKeys[5]=KEY_MINUS_SEP;		// Keyboard - keycode
		shortcutKeys[6]=KEY_LOAD_NEXT;		// Keyboard -> keycode
		shortcutKeys[7]=KEY_LOAD_PREVIOUS;	// Keyboard <- keycode
		shortcutKeys[8]=KEY_RENDER_STYLE;	// Keyboard r keycode
		shortcutKeys[9]=KEY_OUTLIERS;		// Keyboard o keycode
		shortcutKeys[10]=KEY_ALGO;			// Keyboard a keycode
		// Screen display strings
		shortcutKeyNames[0]="'h'";
		shortcutKeyNames[1]="'c'";
		shortcutKeyNames[2]="ESC";
		shortcutKeyNames[3]="'f'";
		shortcutKeyNames[4]="'s'";
		shortcutKeyNames[5]="+/-";
		shortcutKeyNames[6]="->";
		shortcutKeyNames[7]="<-";
		shortcutKeyNames[8]="'r'";
		shortcutKeyNames[9]="'o'";
		shortcutKeyNames[10]="'a'";
		// Screen description strings
		shortcutKeyDesc[0] = "Hide/Unhide Keyboard Commands";
		shortcutKeyDesc[1] = "Hide/UnHide Camera Labels";
		shortcutKeyDesc[2] = "QUIT";
		shortcutKeyDesc[3] = "Enable/Disable Camera Flashing";
		shortcutKeyDesc[4] = "Enable/Disable STEREO mode";
		shortcutKeyDesc[5] = "Increase/Decrease Eye Separation";
		shortcutKeyDesc[6] = "Load next file [sim mode (-1) only]";
		shortcutKeyDesc[7] = "Load previous file [sim mode (-1) only]";
		shortcutKeyDesc[8] = "Toggle render style [sim mode (-1) only]";
		shortcutKeyDesc[9] = "Toggle outlier display [sim mode (-1) only]";
		shortcutKeyDesc[10] = "Toggle SURF/SIFT result displayed [sim mode (-1) only]";

		pointRenderOption=RENDER_DOTS;	// Indicates how drawPointCloud will represent a point graphically

		fps_clock=clock();				// storage for framerate calculation
		fps_count=0;
		fps_filter=0.3;				// low pass filter strength for fps calculation ( see animate() ): lower value -> lower filter frequency
		fps=60;
		show_fps=GL_TRUE;
		show_outliers=GL_TRUE;
		get_video_frames=0;			// indicates how many video frames to save.  (0 - no video output, n - save every nth frame)
		current_frame;				// the next frame to be written
		sim_state=-2; /*
					   *	state variable for code demo;
					   *	set sim_state to -2: dont run sim
					   *	set sim_state to -1: run sim (advance using the left and right keys)
					   *
					   *	state 0: display only camera 0
					   *
					   *	state 1: add camera 1
					   *	state 2: show triangulated points (raw)
					   *	state 3: show triangulated points (averaged)
					   *
					   *	state 4: add camera 2
					   *	state 5: show new triangulated points (raw)
					   *	etc, etc, etc.
					   */
}

/* drawCameraVis
 *  Called by external code to add a "camera" display to the 3D environment.  Assigned to graphics list CAMERA_VIS+cameraN
 *
 *  inputs:
 *  	location is a three element vector providing the global coordinates of the camera eye
 *  	orientation is specified in one of two ways (see below)
 *  	cameraN is the integer index of this camera (0 < cameraN < camera_num)
 *
 *  precondition: addCamera must be called before this function in order to set up the display geometry
 *  postcondition: new camera has been drawn and stored in a display list.
 *  	Translation to location[] occurs first, then rotation
 *  	Camera is depicted by a rectangular pyramid with the image plane as its base
 *
 */
void PCloudViewer::drawCameraVis(const vector<GLfloat> location, const vector<GLfloat> quat, int cameraN){
	// Option 1a: specify a location and quaternion (vector) orientation
	//	quaternion should specify rotation relative to initial orientation pointing in direction <0,0,-1> with the up direction being <0,1,0>
	//	q[0], q[1], q[2] specifies axis of rotation
	//	q[3] specifies (cosine of) rotation about this axis
	GLfloat loc[3]={location[0], location[1], location[2]};
	GLfloat q[4]={quat[0], quat[1], quat[2], quat[3]};
	drawCameraVis(loc, q, cameraN);
}
void PCloudViewer::drawCameraVis(const GLfloat location[], const GLfloat rotM[4][4], int cameraN){
	// Option 2: specify a location and matrix orientation
	//	input matrix should specify rotation relative to initial orientation pointing in direction <0,0,-1> with the up direction being <0,1,0>
	//	it is expected that the final row and column are equal to that of the 4x4 identity matrix (translation is handled separately -- specify with location[])
	glNewList(CAMERA_VIS+cameraN, GL_COMPILE);
		  glPushMatrix();
		  glTranslatef(location[0], location[1], location[2]);
		  glMultTransposeMatrixf(&rotM[0][0]);
		  	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, cameraLineColor);
		  	glBegin( GL_LINES );
				glVertex3fv(&pent_v[0][0]);
				glVertex3fv(&pent_v[1][0]);
		  	glEnd();
		  	glBegin( GL_LINES );
				glVertex3fv(&pent_v[0][0]);
				glVertex3fv(&pent_v[2][0]);
		  	glEnd();
		  	glBegin( GL_LINES );
				glVertex3fv(&pent_v[0][0]);
				glVertex3fv(&pent_v[3][0]);
		  	glEnd();
		  	glBegin( GL_LINES );
				glVertex3fv(&pent_v[0][0]);
				glVertex3fv(&pent_v[4][0]);
		  	glEnd();

			/* pent_v[1] is top left
			 * pent_v[2] is top right
			 */
		  	float top_point[3]={( pent_v[1][0]+pent_v[2][0] )/2,
								( pent_v[1][1]+pent_v[2][1] )/2,
								( pent_v[1][2]+pent_v[2][2] )/2};
		  	glBegin( GL_LINES );
				glVertex3fv(&pent_v[0][0]);
				glVertex3fv(&top_point[0]);
		  	glEnd();
		  	if((int)cameraFlashFrequencies.size()>cameraN){
		  		GLfloat clr[]={cameraVisColor[0], cameraVisColor[1], cameraVisColor[2],
		  				cameraVisColor[3]*pow(cos(PI*cameraFlashPhase*cameraFlashFrequencies[cameraN]), 2)   };
				glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, clr);
		  	}
		  	else{
		  		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, cameraVisColor);
		  	}
		  	glBegin(GL_QUADS);
				glNormal3fv(&pent_n[4][0]);				// surface 1 is 1,2,3,4
				glVertex3fv(&pent_v[4][0]);
				glVertex3fv(&pent_v[3][0]);
				glVertex3fv(&pent_v[2][0]);
				glVertex3fv(&pent_v[1][0]);
			glEnd();
		    glBegin(GL_QUADS);
				glNormal3fv(&pent_n[4][0]);				// render both sides
				glVertex3fv(&pent_v[1][0]);
				glVertex3fv(&pent_v[2][0]);
				glVertex3fv(&pent_v[3][0]);
				glVertex3fv(&pent_v[4][0]);
			glEnd();
		  glPopMatrix();
		  glEndList();
}
void PCloudViewer::drawCameraVis(const GLfloat location[], const GLfloat quat[], int cameraN){
	// Option 1b: specify a location and quaternion (array) orientation
	//	quaternion should specify rotation relative to initial orientation pointing in direction <0,0,-1> with the up direction being <0,1,0>
	//	q[0], q[1], q[2] specifies axis of rotation
	//	q[3] specifies (cosine of) rotation about this axis
	  glNewList(CAMERA_VIS+cameraN, GL_COMPILE);
	  glPushMatrix();
	  glTranslatef(location[0], location[1], location[2]);
	  glRotatef(quat[0], quat[1], quat[2], quat[3]);
	  	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, cameraLineColor);
	  	glBegin( GL_LINES );
			glVertex3fv(&pent_v[0][0]);
			glVertex3fv(&pent_v[1][0]);
	  	glEnd();
	  	glBegin( GL_LINES );
			glVertex3fv(&pent_v[0][0]);
			glVertex3fv(&pent_v[2][0]);
	  	glEnd();
	  	glBegin( GL_LINES );
			glVertex3fv(&pent_v[0][0]);
			glVertex3fv(&pent_v[3][0]);
	  	glEnd();
	  	glBegin( GL_LINES );
			glVertex3fv(&pent_v[0][0]);
			glVertex3fv(&pent_v[4][0]);
	  	glEnd();
	  	if((int)cameraFlashFrequencies.size()>cameraN){
	  		GLfloat clr[]={cameraVisColor[0], cameraVisColor[1], cameraVisColor[2],
	  				cameraVisColor[3]*pow(cos(PI*cameraFlashPhase*cameraFlashFrequencies[cameraN]), 2)   };
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, clr);
	  	}
	  	else{
	  		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, cameraVisColor);
	  	}
	  	glBegin(GL_QUADS);
			glNormal3fv(&pent_n[4][0]);				// surface 1 is 1,2,3,4
			glVertex3fv(&pent_v[4][0]);
			glVertex3fv(&pent_v[3][0]);
			glVertex3fv(&pent_v[2][0]);
			glVertex3fv(&pent_v[1][0]);
		glEnd();
	    glBegin(GL_QUADS);
			glNormal3fv(&pent_n[4][0]);				// render both sides
			glVertex3fv(&pent_v[1][0]);
			glVertex3fv(&pent_v[2][0]);
			glVertex3fv(&pent_v[3][0]);
			glVertex3fv(&pent_v[4][0]);
		glEnd();
	  glPopMatrix();
	  glEndList();
}
void PCloudViewer::drawBox(void)
{
  int i;

  for (i = 0; i < 6; i++) {
    glBegin(GL_QUADS);
    glNormal3fv(&n[i][0]);
    glVertex3fv(&v[faces[i][0]][0]);
    glVertex3fv(&v[faces[i][1]][0]);
    glVertex3fv(&v[faces[i][2]][0]);
    glVertex3fv(&v[faces[i][3]][0]);
    glEnd();
  }
}


/* drawBox
 *  Called by makeSpherePointCloud to add a cubical "point" to the 3D environment.
 *
 *  inputs:
 *  	location is specified as three floating point coordinates
 *  	sidelength, the length of the cube side is specified as a floating point length
 *  	color is optionally specified as three floating point values
 *
 *  precondition: if color is unspecified, glMaterial should be set beforehand
 *  postcondition: cube has been drawn (any saving to display lists happens external to this function)
 *
 */
void PCloudViewer::drawBox(GLfloat posx, GLfloat posy, GLfloat posz, GLfloat side){
	addCube(posx, posy, posz, side);
	drawBox();
}
void PCloudViewer::drawBox(GLfloat posx, GLfloat posy, GLfloat posz,
		GLfloat r, GLfloat g, GLfloat b, GLfloat side){
	GLfloat boxColor[] = {r, g, b, 1.0};
	  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, boxColor);
	addCube(posx, posy, posz, side);
	drawBox();
}

/* drawSpherePoints
 *  Called by external code to add a spherical pointcloud to the 3D environment. Assigned to graphics list POINTCLOUD
 *
 *  Only intended for debugging use, so geometric parameters are all hard-coded.
 *
 *  precondition: POINTCLOUD graphics list should be unused
 *  postcondition: new pointcloud has been drawn and stored in a display list.
 */
void PCloudViewer::drawSpherePoints(){
	glNewList(POINT_CLOUD, GL_COMPILE);
	  glPushMatrix();
	  glTranslatef(0, 0, 0);
	GLfloat res=PI/100, radius=1;
	for(GLfloat theta=0; theta<PI*2; theta+=res)
		for(GLfloat phi=0; phi<PI; phi+=res)
			/* Setup cube vertex data. */
			drawBox(radius*cos(theta)*sin(phi),
					radius*sin(theta)*sin(phi),
					radius*cos(phi),
					cos(theta*6 + phi*8),
					0.4,
					sin(theta*6),
					radius*res/4);
	  glPopMatrix();
	glEndList();
}

/* makeSpherePointCloud
 *  Called by external code to generate a spherical pointcloud.
 *
 *  inputs:
 *  	radius is specified as a single positive floating point value
 *  	nSlices is the number of discrete points to draw along any given parallel
 *  	nStacks is the number of discrete points to draw along any given longitude
 *
 *  Only intended for debugging use, so other geometric and color parameters are hard-coded.
 *
 *  precondition:
 *  postcondition: new pointcloud (with nSlices*nStacks points) has been generated and stored in returned vector.
 *
 *  return value: returns a pointcloud (a vector of x-y-z-red-green-blue 6-vectors)
 */
vector<vector<GLfloat> > PCloudViewer::makeSpherePointCloud( GLfloat radius=1, int nSlices=100, int nStacks=50){
	GLfloat theta, phi;
	vector<vector<GLfloat> > xyzrgb;
	vector< GLfloat > next_point;
	for(GLfloat j=0; j<nSlices; ++j){
		for(GLfloat k=0; k<nStacks; ++k){
			theta=2*PI*j/nSlices;
			phi=PI*k/nStacks;
			next_point.clear();
			next_point.push_back( radius*cos(theta)*sin(phi) );
			next_point.push_back( radius*sin(theta)*sin(phi) );
			next_point.push_back( radius*cos(phi) );
			next_point.push_back( pow(cos(theta*6 + phi*2), 2) );
			next_point.push_back( pow(cos(theta*6 + phi*2), 2) );
			next_point.push_back( pow(sin(theta*6 + phi*2), 2));
			xyzrgb.push_back(next_point);
		}
	}
	return xyzrgb;
}


/* drawPointCloud
 *  Called by external code to add an arbitrary pointcloud to the 3D environment. Assigned to graphics list POINTCLOUD
 *
 *
 *  inputs:
 *  	side, the cubic side length is specified as a single floating point value
 *  	nPoints, the number of points to draw ( <= xyzrgb.size() ) is specified by a single long integer
 *
 *  precondition: POINTCLOUD graphics list should be unused; xyzrgb should be initialized to an n by 6 nested vector (with
 * 	 makeSpherePointCloud, readPointCloudTextFile, or some other method)
 *  postcondition: new pointcloud has been drawn and stored in a display list.
 */
void PCloudViewer::drawPointCloud(vector<vector<GLfloat> > xyzrgb,
					GLfloat side=PI/100/4,
					long nPoints=-1){
	if(nPoints<0) nPoints=xyzrgb.size();
	glNewList(POINT_CLOUD, GL_COMPILE);
	printf("FOUR\n");
	  glPushMatrix();
	  glTranslatef(0, 0, 0);
	  if(pointRenderOption==RENDER_DOTS){
		  glBegin(GL_POINTS);
		  	  for(long i=0; i<nPoints; ++i){
		  		GLfloat dotColor[] = {xyzrgb[i][3], xyzrgb[i][4], xyzrgb[i][5], 1.0};
		  		  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, dotColor);
			      glVertex3fv(&xyzrgb[i][0]);
		  	  }
		  glEnd();
	  }
	  else if(pointRenderOption==RENDER_CUBES){
		for(long i=0; i<nPoints; ++i)
				drawBox(xyzrgb[i][0],
						xyzrgb[i][1],
						xyzrgb[i][2],
						xyzrgb[i][3],
						xyzrgb[i][4],
						xyzrgb[i][5],
						side);
	  }
	  glPopMatrix();
	printf("FIVE\n");
	glEndList();
}

/* addCube
 *	Called by drawbox to set vertex locations of the cube to be drawn.
 */
void PCloudViewer::addCube(GLfloat posx, GLfloat posy, GLfloat posz, GLfloat side){
	/* Setup cube vertex data. */
	  v[0][0] = v[1][0] = v[2][0] = v[3][0] = -1*side/2+posx;
	  v[4][0] = v[5][0] = v[6][0] = v[7][0] = 1*side/2+posx;
	  v[0][1] = v[1][1] = v[4][1] = v[5][1] = -1*side/2+posy;
	  v[2][1] = v[3][1] = v[6][1] = v[7][1] = 1*side/2+posy;
	  v[0][2] = v[3][2] = v[4][2] = v[7][2] = 1*side/2+posz;
	  v[1][2] = v[2][2] = v[5][2] = v[6][2] = -1*side/2+posz;
}

/* addCamera
 *	Called by readCameraTextFile or external code to set geometry of camera display to be drawn.
 *
 *	inputs:
 *		fp, the distance to the drawn camera focal plane
 *		fpw, the width of the drawn camera focal plane
 *		fph, the height of the drawn camera focal plane
 *
 *	precondition:
 *	postcondition: geometry has been set.  Still need to call drawCamerVis to put one (or several) of
 *		these cameras somewhere in the 3D enviroment
 */
void PCloudViewer::addCamera(GLfloat fp, GLfloat fpw, GLfloat fph){	 // camera dist focal plane FOV size (length units)
	/* Setup pentahedron vertex data. */
	pent_v[0][0] = 0;
	pent_v[0][1] = 0;
	pent_v[0][2] = 0;
	pent_v[1][0] = pent_v[4][0] = -1*fpw/2;
	pent_v[2][0] = pent_v[3][0] = 1*fpw/2;
	pent_v[1][1] = pent_v[2][1] = 1*fph/2;
	pent_v[3][1] = pent_v[4][1] = -1*fph/2;
	pent_v[1][2] = pent_v[2][2] = pent_v[3][2] = pent_v[4][2] = 1*fp;
	/*
	 * pent_v[0] is camera center
	 * pent_v[1] is top left
	 * pent_v[2] is top right
	 * pent_v[3] is bottom right
	 * pent_v[4] is bottom left
	 */
	/* Setup pentahedron normal vectors. */
	pent_n[0][0]=0;   pent_n[0][1]=fp;  pent_n[0][2]=-1*fph/2;	// surface 0 is 0,1,2
	pent_n[1][0]=fp;  pent_n[1][1]=0;   pent_n[1][2]=-1*fph/2;	// surface 1 is 0,2,3
	pent_n[2][0]=0;   pent_n[2][1]=-fp; pent_n[2][2]=-1*fpw/2;	// surface 2 is 0,3,4
	pent_n[3][0]=-fp; pent_n[3][1]=0;   pent_n[3][2]=-1*fpw/2;	// surface 3 is 0,4,1
	pent_n[4][0]=0;   pent_n[4][1]=0;   pent_n[4][2]=-1.0;		// surface 4 is 1,2,3,4
}

/* recalcModelView
 *	Called by redraw to update the orientation/scaling of the GL model.
 */
void PCloudViewer::recalcModelView(void)
{
  GLfloat m[4][4];

  glPopMatrix();
  glPushMatrix();
  build_rotmatrix(m, curquat);
  glMultMatrixf(&m[0][0]);
  if (scalefactor == 1.0) {
    glDisable(GL_NORMALIZE);
  } else {
    glEnable(GL_NORMALIZE);
  }
  glScalef(scalefactor, scalefactor, scalefactor);
  newModel = 0;
}

/* showFixedMessage
 *	Called by redraw to put a string of text to location x,y,z in GLOBAL coordinates.
 *
 *	inputs:
 *		location is specified by three floating point values
 *		relativeSize, the size of the rendered text, is optionally specified as a single floating point value (default 1.0)
 *
 *	precondition: everything else should be draw first, otherwise text could be covered up
 *	postcondition: text is drawn to 3D environment (always oriented to read from left to right on the screen)
 */
void PCloudViewer::showFixedMessage(GLfloat x, GLfloat y, GLfloat z, char *message, GLfloat relativeSize)
{
	  GLfloat m[4][4];
	  float quat[4];
	  glPushMatrix();
	  glDisable(GL_LIGHTING);
	  quat[3]=curquat[3];
	  quat[0]=-curquat[0];
	  quat[1]=-curquat[1];
	  quat[2]=-curquat[2];
	  build_rotmatrix(m, quat);		// undo model rotation to keep the text oriented with screen
	  glMultMatrixf(&m[0][0]);
	  glTranslatef(x/scalefactor, y/scalefactor, z/scalefactor);
	  glScalef(.0004/scalefactor*relativeSize, .0004/scalefactor*relativeSize, .0004/scalefactor*relativeSize);
	  while (*message) {
	    glutStrokeCharacter(GLUT_STROKE_ROMAN, *message);
	    message++;
	  }
	  glEnable(GL_LIGHTING);
	  glPopMatrix();
}
void PCloudViewer::showFixedMessage(GLfloat x, GLfloat y, GLfloat z, char *message)
{
	showFixedMessage(x,y,z,message,1.0);
}

/* showMessage
 *	Called by redraw to put a string of text to location x,y,z in MODEL coordinates.  This text rotates with model
 *
 *	inputs:
 *		location is specified by three floating point values
 *		relativeSize, the size of the rendered text, is optionally specified as a single floating point value (default 1.0)
 *
 *	precondition: everything else should be draw first, otherwise text could be covered up
 *	postcondition: text is drawn to 3D environment (always oriented to read from left to right on the screen)
 */
void PCloudViewer::showMessage(GLfloat x, GLfloat y, GLfloat z, char *message, GLfloat relativeSize)
{
	  GLfloat m[4][4];
	  float quat[4];
	  glPushMatrix();
	  glDisable(GL_LIGHTING);
	  glTranslatef(x, y, z);
	  quat[3]=curquat[3];
	  quat[0]=-curquat[0];
	  quat[1]=-curquat[1];
	  quat[2]=-curquat[2];
	  build_rotmatrix(m, quat);		// undo model rotation to keep the text oriented with screen
	  glMultMatrixf(&m[0][0]);
	  glScalef(.0004/scalefactor*relativeSize, .0004/scalefactor*relativeSize, .0004/scalefactor*relativeSize);
	  while (*message) {
	    glutStrokeCharacter(GLUT_STROKE_ROMAN, *message);
	    message++;
	  }
	  glEnable(GL_LIGHTING);
	  glPopMatrix();
}
void PCloudViewer::showMessage(GLfloat x, GLfloat y, GLfloat z, char *message)
{
	showMessage(x,y,z,message,1.0);
}

/* redraw
 * 	Called periodically by glut loop to refresh the screen.
 */
void PCloudViewer::redraw(void)
{
	long timer=clock();
  if (newModel)
    recalcModelView();
  if(STEREO==1){
  /* for stereo hardware*/
	glDrawBuffer(GL_BACK_LEFT);                                   //draw into both back buffers
	  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);      //clear color and depth buffers
	glDrawBuffer(GL_BACK_RIGHT);
	  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //Right Eye
	glDrawBuffer(GL_BACK_RIGHT);                              //draw into back right buffer
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
  	glTranslatef(-1*eyeSeparation/2, 0.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
	callLists();
  //Left Eye
	glDrawBuffer(GL_BACK_LEFT);                              //draw into back left buffer
	glMatrixMode(GL_PROJECTION);
	GLfloat shift = visEyeRange*tan(visFOV/2*PI/180);
	glTranslatef(eyeSeparation+shift, 0.0, 0.0);
	callLists();
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glutSwapBuffers();
  }
  else if(STEREO==2){
	  /* for stereo hardware*/
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	  //Left Eye -- shifted to the left half of the viewport
	  	glMatrixMode(GL_PROJECTION);
	  	glPushMatrix();
	  	glViewport(0, 0, W, H);
	    glTranslatef(1*eyeSeparation/2, 0.0, 0.0);
	    glMatrixMode(GL_MODELVIEW);
		callLists();


	  //Right Eye -- shifted to the right half of the viewport
	  	glMatrixMode(GL_PROJECTION);
	  	glViewport(W, 0, W, H);
	  	glTranslatef(-eyeSeparation, 0.0, 0.0);
		glMatrixMode(GL_MODELVIEW);
		callLists();

		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
	  	glutSwapBuffers();
  }
  else{
	  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	  callLists();
	  glutSwapBuffers();
  }

  timer=(clock()-timer)*1000/CLOCKS_PER_SEC; // quick fix to keep the FPS somewhat constant
  //printf("%d ms.\n",timer);
  if(timer<13) //usleep(13000-timer*1000);
	//this_thread::sleep_for (chrono::milliseconds(13-timer));
	//sleep(13-timer);
	{
		struct timespec timeOut,remains;

		timeOut.tv_sec = 0;
		timeOut.tv_nsec = (13-timer)*1000000L;

		nanosleep(&timeOut, &remains);
	}
}

/* callLists
 * 	Called by redraw to redisplay each of the display lists
 */
void PCloudViewer::callLists(){
	  char msg[]="Press 'h' Key for Keyboard Commands.";
	  if(showingHelpMenu==0){
		  glCallList(POINT_CLOUD);
		  callCameras();
		  if(helpHintFade > 0){
			  glColor4f(1,1,0,helpHintFade/1000.0);
			  showFixedMessage(-1.5, 0, 0,  msg, 3);
			  //if(cameraFlashPhase > 200L)
			  helpHintFade-=1000.0/fps;	// convert one frame to milliseconds
			  if(helpHintFade<0) helpHintFade=0;
			  glColor4f(1,1,1,1);
		  }
		  if(show_fps){
			  char str[100];
			  glColor4f(1,1,0,1);
			  sprintf(str,"FPS: %f",fps);
			  showFixedMessage(-1.55, -1.7, 0,  str, 2);
			  glColor4f(1,1,1,1);
		  }
	  }
	  if(showingHelpMenu==1){
	 	  drawHelpMenu();
	  }
}

/* callCameras
 * 	Called by redraw to redisplay each of the camera display lists
 */
void PCloudViewer::callCameras(){
	char str[100];
	glDisable(GL_DEPTH_TEST);
		  	  for(int n=0;n<camera_num;++n){
		  		  if(flashing){
		  			  while(cameraFlashFrequencies.size()<=(unsigned)n){
		  				  cameraFlashFrequencies.push_back(0.0);//(0.02+0.02*n/camera_num);
		  				  printf("Warning! Flashing is enabled but no cameraFlashFrequency set for camera %d: 0.0 will be used.\n", n);
		  			  }
		  			  if(cameraQuaternions.size()>(unsigned)n)
		  				  drawCameraVis(cameraLocations[n],cameraQuaternions[n], n);
		  			  else if(cameraRotations.size()>(unsigned)n){
		  				  GLfloat loc[3]={cameraLocations[n][0], cameraLocations[n][1], cameraLocations[n][2]};
		  				  GLfloat cam_rot[4][4];
		  				  for( int m=0;m<4;++m){
		  					  for(int p=0;p<4;++p){
		  						  cam_rot[m][p]=cameraRotations[n][m*4+p];
		  					  }
		  				  }
		  				  drawCameraVis(loc,cam_rot, n);
		  			  }
		  		  }
		  		  glCallList(CAMERA_VIS+n);
		  		  if(label_cameras==1){
		  			sprintf(str,camera_label_text.c_str(),n);
		  			showMessage(cameraLocations[n][0], cameraLocations[n][1], cameraLocations[n][2], str);
		  		  }
		  	  }
		  	  glEnable(GL_DEPTH_TEST);
}

/* myReshape
 *  Called by the glut loop anytime the window changes size.  Sets up the W and H member variables and the GL perspective
 */
void PCloudViewer::myReshape(int w, int h)
{
  H = h;
  GLfloat AR;
  if(STEREO==2){
	  W=w/2;
	  AR=(GLfloat)W/H*2;
  }
  else {
	  W=w;
	  AR=(GLfloat)W/H;
  }
  glViewport(0, 0, W, H);
  newModel = 1;
  /* This enforces the correct aspect ratio */
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(visFOV, AR, clipNear, clipFar);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
}

/* keyPressed
 * 	Called by theh glut loop whenever a normal key has been pressed. (Another function would be necessary to handle
 * 	special key presses such as SHIFT and CONTROL.)
 */
void PCloudViewer::keyPressed (unsigned char key, int x, int y) {
	printf("Key: %d\n", key );
	switch(key){
	case KEY_QUIT:{
			//exit(EXIT_SUCCESS);
			glutDestroyWindow(main_win);
			break;
		}
	case KEY_FLASHING:{		//Enable or disable blinking animations
				flashing=1-flashing;
				cameraFlashPhase=0;
				if(flashing==0){
					// redraw at flashing phase 0 needed
					for(int n=0;n<camera_num;++n){
						  while(cameraFlashFrequencies.size()<=(unsigned)n){
							  cameraFlashFrequencies.push_back(0.0);//(0.02+0.02*n/camera_num);
							  printf("Warning! Flashing is enabled but no cameraFlashFrequency set for camera %d: 0.0 will be used.\n", n);
						  }
						  if(cameraQuaternions.size()>(unsigned)n)
							  drawCameraVis(cameraLocations[n],cameraQuaternions[n], n);
						  else if(cameraRotations.size()>(unsigned)n){
							  GLfloat loc[3]={cameraLocations[n][0], cameraLocations[n][1], cameraLocations[n][2]};
							  GLfloat cam_rot[4][4];
							  for( int m=0;m<4;++m){
								  for(int p=0;p<4;++p){
									  cam_rot[m][p]=cameraRotations[n][m*4+p];
								  }
							  }
							  drawCameraVis(loc,cam_rot, n);
						  }
					  }
					if(!spinning && helpHintFade==0) glutIdleFunc(NULL);
				}
				else{
				      glutIdleFunc(animateWrapper);
				}
				glutPostRedisplay();
				break;
			}
	case KEY_CAM_LABEL:{	//Enable or disable text labels for cameras
				label_cameras=1-label_cameras;
				glutPostRedisplay();
				break;
			}
	case KEY_STEREO:{		//Enable or disable stereo (mode 2)
			  if (STEREO==0){
				  STEREO=2;
				  glutReshapeWindow(W*2, H);
			  }
			  else if(STEREO==2){
				  STEREO=0;
				  glutReshapeWindow(W, H);
			  }
			break;
		}
	case KEY_MINUS_SEP:{
		  	  eyeSeparation-=eyeSepStep;
		  	  printf("eyeSeparation = %f", eyeSeparation);
		  	  glutPostRedisplay();
		  	  break;
	}
	case KEY_PLUS_SEP:{
	  	  	  eyeSeparation+=eyeSepStep;
		  	  printf("eyeSeparation = %f", eyeSeparation);
		  	  glutPostRedisplay();
		  	  break;
	}
	case KEY_RENDER_STYLE:{

		  if(pointRenderOption==RENDER_DOTS) pointRenderOption=RENDER_CUBES;
		  else pointRenderOption=RENDER_DOTS;
		  if(sim_state>-1){
			  simulationRead();
			  glutPostRedisplay();
		  }
				  break;
	}
	case KEY_OUTLIERS:{
			if(sim_state>-2){
				show_outliers=!show_outliers;
				if(sim_state>-1){
					  simulationRead();
					  glutPostRedisplay();
				}
			}
			break;
		}
	case KEY_ALGO:{
			if(sim_state>-2){
				algorithm_state=1-algorithm_state;
				if(algorithm_state==0){
					  subpath="../dino_demo_surf/";
					  simulationRead();
					  glutPostRedisplay();
				}
				else{
					  subpath="../dino_demo_sift/";
					  simulationRead();
					  glutPostRedisplay();
				}
			}
			break;
		}
	case KEY_HELP:{
		  	  showingHelpMenu=1-showingHelpMenu;
		  	  helpHintFade=0;
		  	  glutPostRedisplay();
		  	  break;
		}
	}
}
void PCloudViewer::specialKeyPressed(int key, int x, int y) {

    //if(glutGetModifiers() & GLUT_ACTIVE_SHIFT) { // testing SHIFT status
    //}
    switch(key) {
        case GLUT_KEY_LEFT:
			  if(sim_state > -1){
				  sim_state--;
				  if(sim_state==-1) sim_state=MAX_STATE_N-1;
				  if(glutGetModifiers() & GLUT_ACTIVE_SHIFT) {
					  sim_state--;
					  if(sim_state==-1) sim_state=MAX_STATE_N-1;
					  sim_state--;
					  if(sim_state==-1) sim_state=MAX_STATE_N-1;
				  }
				  simulationRead();
				  glutPostRedisplay();
		  	  }
			  break;
        case GLUT_KEY_RIGHT:
		  	  if(sim_state > -2){
				  if(sim_state==-1){
					  glNewList(POINT_CLOUD, GL_COMPILE);
					  glEndList(); // clear any drawn point clouds
					  flashing=true;
					  glutIdleFunc(animateWrapper);
				  }
				  sim_state++;
				  if( sim_state == MAX_STATE_N) sim_state=0; // wrap around after image 36
				  if(glutGetModifiers() & GLUT_ACTIVE_SHIFT) {
					  sim_state++;
					  if( sim_state == MAX_STATE_N) sim_state=0; // wrap around after image 36
					  sim_state++;
					  if( sim_state == MAX_STATE_N) sim_state=0; // wrap around after image 36
				  }
				  simulationRead();
				  glutPostRedisplay();
				}
			  break;
        case GLUT_KEY_UP:

          break;
        case GLUT_KEY_DOWN:

          break;
        }
}

/* mouse
 * 	Called by the glut loop when mouse input arrives.
 */
void PCloudViewer::mouse(int button, int state, int x, int y)
{
  if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
	if(!(glutGetModifiers() & GLUT_ACTIVE_SHIFT)){
		spinning = 0;
		 if(!flashing && helpHintFade==0) glutIdleFunc(NULL);
	}
    moving = 1;
    beginx = x;
    beginy = y;
    if (glutGetModifiers() & GLUT_ACTIVE_SHIFT) {
      scaling = 1;
    } else {
      scaling = 0;
    }
  }
  if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
    moving = 0;
  }
}

/* motion
 *	Called by the glut loop every iteration.
 */
void PCloudViewer::animate(void)
{
	if(spinning){
	  add_quats(lastquat, curquat, curquat);
	  newModel = 1;
	  glutPostRedisplay();
	}
	if(flashing){
		glutPostRedisplay();
	}
	if(helpHintFade>0){
		glutPostRedisplay();
	}
	//cameraFlashPhase++;
	cameraFlashPhase+=1000000.0/fps;

	if(spinning || flashing || helpHintFade>0){
		long diff=fps_clock;
		fps_clock=time(NULL); // in seconds
		diff=fps_clock-diff;
		if(diff==1){
			fps=fps_count;
			fps_count=0;
		}
		//fps=fps*(1.0-fps_filter)+(1.0/diff*CLOCKS_PER_SEC)*fps_filter;
		//printf("%d\n",diff);
	}
	fps_count++;
}

/* motion
 *	Called by the glut loop when a mouse motion event arrives.
 */
void PCloudViewer::motion(int x, int y)
{
	//float H=1000, W=1000; // override the class members, since they change on resize events
  if (scaling) {
    scalefactor = scalefactor * (1.0 + (((float) (beginy - y)) *5/ H));
    beginx = x;
    beginy = y;
    newModel = 1;
    glutPostRedisplay();
    return;
  }
  if (moving) {
    trackball(lastquat,
      (2.0 * beginx - W) / 1400,
      (H - 2.0 * beginy) / 1400,
      (2.0 * x - W) / 1400,
      (H - 2.0 * y) / 1400
      );
    //printf("lq: %f, %f, %f, %f\n", lastquat[0], lastquat[1], lastquat[2], lastquat[3]);
    beginx = x;
    beginy = y;
    spinning = 1;
    glutIdleFunc(animateWrapper);
  }
}

/* vis
 * 	Called by the glut loop when window gains/loses visibility.
 */
void PCloudViewer::vis(int visible)
{
  if (visible == GLUT_VISIBLE) {
    if (spinning || flashing || helpHintFade>0)
      glutIdleFunc(animateWrapper);
      fps_clock=clock();
  } else {
    if ((spinning || flashing ))
      glutIdleFunc(NULL);
  }
}

/* menuHandler
	 *  Called by the glut loop when a menu item is selected
	 */
void PCloudViewer::menuHandler(int value){
	printf("%d", value);
}

void PCloudViewer::init_openGL(int argc, char **argv){
	  glutInit(&argc, argv);
	  if (STEREO==1)
		  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STEREO);
	  else
		  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	  trackball(curquat, 0.0, 0.0, 0.0, 0.0);
	  if(STEREO==2)
		  glutInitWindowSize(W*2,H);
	  else
		  glutInitWindowSize(W,H);
	  main_win=glutCreateWindow("PointCloudVis");
	  glutDisplayFunc(redrawWrapper); //  Tell GLUT to use the methods below for event handling
	  glutReshapeFunc(myReshapeWrapper);
	  glutVisibilityFunc(visWrapper);
	  glutMouseFunc(mouseWrapper);
	  glutKeyboardFunc(keyPressedWrapper);
	  glutSpecialFunc(specialKeyPressedWrapper);
	  glutMotionFunc(motionWrapper);

	  glEnable(GL_BLEND);									// Enable blending for transparency
	  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	// Define blending function.  Technically, it is correct
	  	  	  	  	  	  	  	  	  	  	  	  	  	  	//  to draw all opaque objects first, and then draw transparent
	  	  	  	  	  	  	  	  	  	  	  	  	  	  	//  items in back-to-front order
	  glEnable(GL_CULL_FACE);
	  glEnable(GL_DEPTH_TEST);
	  glEnable(GL_LIGHTING);
	  glMatrixMode(GL_PROJECTION);
	  gluPerspective( /* field of view in degree */ visFOV,
			  	  	  /* aspect ratio */ 1.0,
			  	  	  /* Z near */ clipNear,
			  	  	  /* Z far */ clipFar);
	  glMatrixMode(GL_MODELVIEW);
	  gluLookAt(0.0, 0.0, visEyeRange,  /* eye is at (0,0,visEyeRange) */
	    0.0, 0.0, 0.0,      /* center is at (0,0,0) */
	    0.0, 1.0, 0.);      /* up is in positive Y direction */
	  glPushMatrix();       /* dummy push so we can pop on model
	                           recalc */

	  /*glutCreateMenu(menuHandlerWrapper);
	  glutAddMenuEntry("Full screen", 0);
	  glutAddMenuEntry("Quit", 1);
	  glutAttachMenu(GLUT_RIGHT_BUTTON);*/

}
void PCloudViewer::start_openGL(){
	  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 1);
	  for(int i=0; i<1; ++i){
		  glLightfv(GL_LIGHT0+i, GL_POSITION, lightPositions[i]);
		  glLightfv(GL_LIGHT0+i, GL_AMBIENT, lightsColor); // using one ambient light
		  float black[]={0,0,0,0};
		  glLightfv(GL_LIGHT0+i, GL_DIFFUSE, black);
		  glEnable(GL_LIGHT0+i);
	  }
	  glLineWidth(1.0);
	  glutIdleFunc(animateWrapper);
	  glutMainLoop();
}
void PCloudViewer::readPointCloudTextFile(vector<vector<float> >&xyzrgb, string fname, int cols){
	char filename[200];//="/Users/bmr27715/Documents/workspace/SFM/images/dino_data/point_data.txt";
	strcpy(filename, fname.c_str());
	vector<vector<float> > result;
	
	printf("fname = ");
	printf("%s", fname.c_str());
	printf("\n\n");

	float x1, x2, x3, x4, x5, x6, x7, x8, x9;
	FILE* file = fopen(filename, "r" );
	
	printf("ONE\n");
	
	while(!feof(file)){
		if(cols==9)				// insight 3D file line format is x y z 0 0 0 r g b
			fscanf(file, "%f %f %f %f %f %f %f %f %f", &x1, &x2, &x3, &x4, &x5, &x6, &x7, &x8, &x9);
		else if(cols==6)		// SfM file line format is x y z r g b
			fscanf(file, "%f %f %f %f %f %f", &x1, &x2, &x3, &x7, &x8, &x9);
		else if(cols==7){		// SfM file line format is x y z r g b error
			fscanf(file, "%f %f %f %f %f %f %f", &x1, &x2, &x3, &x7, &x8, &x9, &x4);

			if(x4 > .5){
				if(!show_outliers) continue;
				x7=1.0; // highlight outliers in red
				x8=0.15;
				x9=0.15;
			}
		}
		vector<float> v;
		v.push_back(-x1);
		v.push_back(-x2);
		v.push_back(x3);
		v.push_back(x7);
		v.push_back(x8);
		v.push_back(x9);
		result.push_back(v);
		//printf("%f\n", x1);
	}
	printf("TWO\n");
	fclose(file);
	printf("THREE\n");
	xyzrgb = result;
}
void PCloudViewer::readCameraTextFile(string fname, int num_to_read=36){
	char filename[200];
	strcpy(filename, fname.c_str());
	int num_cams=36;
	vector<vector<GLfloat>  > cam_centers(num_cams);
	GLfloat cam_rots[num_cams][4][4];
	vector<vector<GLfloat> > cam_fovs(num_cams);
	vector<vector<GLfloat> > cam_dists(num_cams);
	//vector<string> cam_fnames;

	for(int i=0; i<num_cams; i++){
		cam_centers[i].resize(3);
		cam_fovs[i].resize(2);
		cam_dists[i].resize(2);
	}
	FILE* file = fopen(filename, "r" );
	char str[200];
	float num;
	addCamera(0.100,0.25/2,0.15/2);
	for(int i=0; i<num_to_read; ++i){
		fscanf(file, "%s", str);
		//cam_fnames.push_back(str);
		for(int j=0; j<3; ++j){
			fscanf(file, "%f", &num);
			cam_centers[i][j]=num;
		}
		for(int j=0; j<9; ++j){
			fscanf(file, "%f", &num);
			cam_rots[i][j/3][j%3]=num;
		}

		// set remaining matrix elements to identity
		cam_rots[i][3][3]=1;
		cam_rots[i][2][3]=0;
		cam_rots[i][1][3]=0;
		cam_rots[i][0][3]=0;
		cam_rots[i][3][2]=0;
		cam_rots[i][3][1]=0;
		cam_rots[i][3][0]=0;
		//printf("\n");
		for(int j=0; j<2; ++j){
			fscanf(file, "%f", &num);
			cam_fovs[i][j]=num;
		}

		for(int j=0; j<2; ++j){
			fscanf(file, "%f", &num);
			cam_dists[i][j]=num;
		}

		GLfloat loc[3]={cam_centers[i][0], cam_centers[i][1], cam_centers[i][2]};
		drawCameraVis(loc, cam_rots[i], camera_num++);
		vector<GLfloat> l1(loc, loc+3);
		vector<GLfloat> rotMat;
		for( int m=0;m<4;++m){
			for(int n=0;n<4;++n){
				rotMat.push_back(cam_rots[i][m][n]);
			}
		}
		cameraRotations.push_back(rotMat);
		cameraLocations.push_back(l1);
		cameraFlashFrequencies.push_back(0.01);
	}
	fclose(file);
}
void PCloudViewer::drawHelpMenu(){
	GLfloat x=-1.5, y=1.0;
	char line[80];
	for(int i=0; i<numShortcutKeys; ++i){
		strcpy (line,shortcutKeyNames[i].c_str());
		strcat (line," -- ");
		strcat (line,shortcutKeyDesc[i].c_str());
		showFixedMessage(x, y, 0,  line, 2.2);
		y-=.2;
	}
}

void PCloudViewer::simulationRead(){
	  int n_cams=(sim_state+2)/3+1;
	  int phase=(sim_state+2)%3;
	  camera_num=0;
	  string cam_fname=path;
	  cam_fname.append(subpath);
	  cam_fname.append("camera_data.txt");
	  readCameraTextFile(cam_fname, n_cams);
	  //printf("camera_num = %d\n", camera_num);
	  //printf("n_cams = %d\n", n_cams);
	  for(int i=0; i<camera_num;++i){ // set only the active camera to flash
			if(i==n_cams-1) (cameraFlashFrequencies)[i]=0.0000005;
			else (cameraFlashFrequencies)[i]=0.0;
	  }
	  if(sim_state==0) phase=0;

		//reload the point cloud
	  if(phase==1){
		  char num_ext[10];
		  sprintf(num_ext,
		  				  "%d.txt",n_cams-1);
		  string pcloud_fname=path;
		  pcloud_fname.append(subpath);
		  pcloud_fname.append("point_data");
		  pcloud_fname.append(num_ext);
		  vector<vector<GLfloat> > pcloud;
		  readPointCloudTextFile(pcloud, pcloud_fname, 7);
		  drawPointCloud(pcloud, PI/100/6, -1);
	  }else if(phase==2){
		  char num_ext[10];
		  sprintf(num_ext,
		  				  "%d.txt",n_cams-1);
		  string pcloud_fname=path;
		  pcloud_fname.append(subpath);
		  pcloud_fname.append("point_data_averaged");
		  pcloud_fname.append(num_ext);
		  vector<vector<GLfloat> > pcloud;
		  readPointCloudTextFile(pcloud, pcloud_fname, 7);
		  drawPointCloud(pcloud, PI/100/6, -1);
	  }
}
void PCloudViewer::setViewToCamera(int num){
	/*
	 * Transform the 3D view such that the viewport
	 *   contains what is visible to a camera specified by num.
	 */
	if(cameraRotations.size()>(unsigned)(num)){
		// TODO: add implementation
	}
}

/*
 void stereoGeometry(GLfloat f_dist){
 
 // for reference, see http://www.orthostereo.com/geometryopengl.html

	  //gluPerspective(visFOV, (GLfloat)w/h, clipNear, clipFar);
	GLfloat ratio = (GLfloat)w/h;
	GLfloat wd2 = tan(PI/180*visFOV/2)*clipNear;
	GLfloat ndfl = clipNear / f_dist;
}*/
void PCloudViewer::redrawWrapper(){
	instance->redraw();
}
void PCloudViewer::myReshapeWrapper(int w, int h){
	instance->myReshape(w, h);
}
void PCloudViewer::recalcModelViewWrapper(){
	instance->recalcModelView();
}
void PCloudViewer::motionWrapper(int x, int y){
	instance->motion(x, y);
}
void PCloudViewer::visWrapper(int visible){
	instance->vis(visible);
}
void PCloudViewer::mouseWrapper(int button, int state, int x, int y){
	instance->mouse(button, state, x, y);
}
void PCloudViewer::keyPressedWrapper(unsigned char key, int x, int y){
	instance->keyPressed(key, x, y);
}
void PCloudViewer::specialKeyPressedWrapper(int key, int x, int y){
	instance->specialKeyPressed(key, x, y);
}
void PCloudViewer::animateWrapper(){
	instance->animate();
}
void PCloudViewer::setInstance(PCloudViewer *viewer){
	instance=viewer;
}
void PCloudViewer::menuHandlerWrapper(int value){
	instance->menuHandler(value);
}
