//============================================================================
// Name        : vis_class.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "pcloud_viewer.h"
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
using namespace std;


int main(int argc, char **argv) {

	PCloudViewer *pcviewer=new PCloudViewer();
	pcviewer->setInstance(pcviewer);

	/*
	string path="/Users/bmr27715/Desktop/dino_demo_surf/";
	pcviewer->sim_state=-1; // execute simulation
	*/
	//string path="/home/brand/Documents/Aerospace/Archive/reese_working_copy_data/dino_demo_surf/";
	string path="/home/brand/Documents/sfm/reese_working_copy_data/dino_demo_surf/";

	struct stat info;

	if( stat( path.c_str(), &info ) != 0 )
	{
	    printf( "ERROR!!! cannot access %s\nPlease change the hard-coded file path in \"vis_class.cpp\".\n", path.c_str() );
	    return -1;
	}
	else if( info.st_mode & S_IFDIR )  // S_ISDIR() doesn't exist on my windows 
	    printf( "%s is a VALID directory\n", path.c_str() );
	else
	{
		printf( "%s is NOT A VALID directory\n", path.c_str() );
		return -2;	
	}
	
	//pcviewer->sim_state=-2; // don't execute simulation
	pcviewer->sim_state=-1; // execute simulation

	pcviewer->path=path;
	pcviewer->STEREO=0;
	pcviewer->flashing=GL_FALSE;
	pcviewer->pointRenderOption=pcviewer->RENDER_CUBES;
	pcviewer->init_openGL(argc, argv);
	vector<vector<GLfloat> > pcloud;
	string fname=path;
	fname.append("point_data_averaged_sift_12_rough.txt");
	  pcloud=pcviewer->makeSpherePointCloud(1, 100, 50);
	//pcviewer->readPointCloudTextFile(pcloud, fname, 7);
	pcviewer->drawPointCloud(pcloud, PI/100/6, -1);
	string cam_fname=path;
	cam_fname.append("camera_data.txt");
	pcviewer->readCameraTextFile(cam_fname, 36);
	pcviewer->camera_num=0;
	//for(int i=0; i<pcviewer->camera_num;++i){
	//	if(i==4) (pcviewer->cameraFlashFrequencies)[i]=0.0000005;
	//	else (pcviewer->cameraFlashFrequencies)[i]=0.0;
	//}
	  pcviewer->start_openGL();
	  return 0;
}
