----------------------------------------------
| SFM Code Repository                        |
| Original code written by Brandon Reese and |
|                          Nick Richard      |
| August 2011                                |
----------------------------------------------
git repo link: https://reeseb777@bitbucket.org/reeseb777/reese_sfm.git


This is a STRUCTURE FROM MOTION software demo.

The point of this software is to construct a 3D colored point cloud that
represents an object photographed from multiple vantage points.

The processing code is located in reese_working_copy_src/
**The processing code uses the openCV library

The graphical viewer code is located in viewer_src/
**The viewer code uses the glut openGL library

To run the demo,
--first, MODIFY the path variable in viewer_src/vis_class.cpp
--next, compile and link the demo code (commands are found in bin/compile_and_link.sh)
>>bash compile_and_link.sh
--finally, run the code
>> ./viewer

control with the keyboard (type 'h' for help) as well as mouse click/drag/scroll

have fun!
