#ifndef _SFM_DIRECTORY_H
#define _SFM_DIRECTORY_H

#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include "typedef.h"

class SfmDirectory{

public:
	svec files;

	void ReadDirectory(string dir);
};

#endif
