/*
 * pcloud_viewer.h
 *
 *  Created on: Jun 24, 2011
 *      Author: bmr27715
 */

#ifndef PCLOUD_VIEWER_H_
#define PCLOUD_VIEWER_H_

#ifndef PI
#define PI 3.14159265
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <string>
#include "trackball.h"
#include <vector>
#include <GL/glut.h>
#include "cv.h"
#include "highgui.h"
//#include "typedef.h"

using namespace std;

typedef enum { /* index constants for point cloud graphics list, and FIRST (of camera_num)
 	 	 	 	 camera vis graphics list */
  RESERVED, POINT_CLOUD, CAMERA_VIS
} displayLists;

class PCloudViewer{
protected:
	static PCloudViewer *instance;
	void simulationRead();
public:
	/* This series of static methods is necessary to initialize the glut library within a class.
	 *	A typical command sequence to create a PCloudViewer object:
	 *	PCloudViewer *pcviewer=new PCloudViewer();
		pcviewer->setInstance(pcviewer);
		pcviewer->STEREO=2;
		pcviewer->init_openGL(argc, argv);
		vector<vector<GLfloat> > pcloud;
		pcloud=pcviewer->makeSpherePointCloud(0.5, 100, 50);
		pcviewer->drawPointCloud(pcloud, PI/100/8, -1);

	  pcviewer->start_openGL();
	 */
		static void redrawWrapper();
		static void myReshapeWrapper(int w, int h);
		static void recalcModelViewWrapper();
		static void motionWrapper(int x, int y);
		static void visWrapper(int visible);
		static void mouseWrapper(int button, int state, int x, int y);
		static void keyPressedWrapper(unsigned char key, int x, int y);
		static void specialKeyPressedWrapper(int key, int x, int y);
		static void animateWrapper();
		static void menuHandlerWrapper(int value);

		static void setInstance(PCloudViewer * viewer);

		int main_win;					// The main window identifier created by glut
		string path, subpath;
		int algorithm_state;			// 0 - surf, 1 - sift
		GLfloat visEyeRange,			// Distance between eye and scene origin
				eyeSeparation, 			// Distance between eyes in stereoscopic viewing mode
				eyeSepStep,				// Increment by which user can change eyeSeparation
				clipNear, 				// Distance from eye to near Z clip plane
				clipFar, 				// Distance from eye to far  Z clip plane
				visFOV;					// Angular Width of screen in degrees
		int STEREO;						// Defines the stereoscopic viewing mode
										// -set to 0 for no stereo display
										// -set to 1 for quad-buffer stereoscopic output
										// -set to 2 for two-buffer stereoscopic output with the
										//	right eye written to the right half and left eye
										//	written to the left half of the screen
		int scaling;					// Indicates whether display is being scaled with the mouse (by holding SHIFT key)
		int spinning, 					// Indicates whether display is being spun with the mouse
			moving;						// Indicates whether the left mouse button is down
		int beginx,						// Point of click origin x coordinate
			beginy;						// Point of click origin y coordinate
		int W, H;						// Width and height of viewport
		float curquat[4];				// Current viewing orientation stored as a quaternion
		float lastquat[4];				// Previos viewing orientation stored as a quaternion
		int newModel;					// Indicates that model should be recomputed due to a perspective change
		float scalefactor;				// Storage for model scaling state
		GLfloat lightPositions[6][4];	// Location of each scene light
		GLfloat lightsColor[4];			// Color of all lights

		GLfloat n[6][3];				// Normals of unit cube
		GLint faces[6][4];				// Faces of unit cube (indices of v)
		GLfloat v[8][3];  				// Vertices of unit cube
		GLfloat pent_n[5][3];			// Normals of pentahedron
		GLfloat pent_v[5][3];			// Vertices of pentahedron
		GLfloat cameraVisColor[4] ;		// Color of camera visualization (front plane)
		GLfloat cameraLineColor[4] ;	// Color of camera visualization (lines)
		int camera_num;					// Number of drawn cameras
		int label_cameras;				// Indicates whether camera labels will be drawn
		string camera_label_text;		// Customizable text label for cameras
		vector<GLfloat> cameraFlashFrequencies;			// Storage for camera-specific flash frequencies [units: cycles / microsecond (10^-6 Hz)]
		vector<vector<GLfloat> > cameraLocations;		// Storage for camera locations (needed to redraw/animate flashing cameras)
		vector<vector<GLfloat> > cameraQuaternions;		// Storage for camera quaternions (quats OR rots needed for flashing camera redraw)
		vector<vector<GLfloat> > cameraRotations;		// Storage for camera rotation matrices (quats OR rots needed for flashing camera redraw)
		long cameraFlashPhase;							// Current phase of blinking camera animation
		GLboolean flashing,								// Indicates whether blinking animation is enabled
					showingHelpMenu;					// Indicates whether keyboard shortcuts are being drawn
		int helpHintFade;								// Timer for initial display fadeaway [units: milliseconds]
		typedef enum { /* index constants for point cloud graphics list, and FIRST (of camera_num)
						 camera vis graphics list */
		  RESERVED, POINT_CLOUD, CAMERA_VIS
		} displayLists;

		// KEYBOARD SHORTCUT KEYS
		const static int KEY_HELP=104;
		const static int KEY_CAM_LABEL=99;
		const static int KEY_QUIT=27;
		const static int KEY_FLASHING=102;
		const static int KEY_STEREO=115;
		const static int KEY_MINUS_SEP=43;
		const static int KEY_PLUS_SEP=45;
		const static int KEY_LOAD_NEXT=GLUT_KEY_RIGHT;
		const static int KEY_LOAD_PREVIOUS=GLUT_KEY_LEFT;
		const static int KEY_RENDER_STYLE=114;
		const static int KEY_OUTLIERS=111;
		const static int KEY_ALGO=97; // toggle between the results for the algorithms 'SURF' and 'SIFT'

		const static int numShortcutKeys=11;
		int shortcutKeys[numShortcutKeys];			// Shortcut key codes
		string shortcutKeyNames[numShortcutKeys];	// Screen display strings
		string shortcutKeyDesc[numShortcutKeys];	// Screen description strings

		const static int RENDER_DOTS=1,				// Represent points as dots
						RENDER_CUBES=2;				// Represent points as 3D cubes
		int pointRenderOption;						// Indicates how drawPointCloud will represent a point graphically

		long fps_clock;								// storage for framerate calculation
		float fps_filter;							// low pass filter strength for fps calculation
		float fps;							// low pass filter strength for fps calculation
		float fps_count;
		GLboolean show_fps;
		GLboolean show_outliers;
		int current_frame;
		int get_video_frames;
		int sim_state;							  /*
												   *	state variable for code demo;
												   *	set sim_state to -2: dont run sim
												   *	set sim_state to -1: run sim (advance using the left and right keys)
												   *
												   *	state 0: display only camera 0
												   *
												   *	state 1: add camera 1
												   *	state 2: show triangulated points (raw)
												   *	state 3: show triangulated points (averaged)
												   *
												   *	state 4: add camera 2
												   *	state 5: show new triangulated points (raw)
												   *	etc, etc, etc.
												   */
		const static int MAX_STATE_N=36*3-2;	// max states for dino example (36 images)

	/* Constructor:
	 * 	Initializes the class member variables.
	 */
	PCloudViewer();

	/* redraw
	 * 	Called periodically by glut loop to refresh the screen.
	 */
	void redraw(void);

	/* callCameras
	 * 	Called by redraw to redisplay each of the camera display lists
	 */
	void callCameras();

	/* callLists
	 * 	Called by redraw to redisplay each of the display lists
	 */
	void callLists();

	/* myReshape
	 *  Called by the glut loop anytime the window changes size.  Sets up the W and H member variables and the GL perspective
	 */
	void myReshape(int w, int h);

	/* keyPressed
	 * 	Called by theh glut loop whenever a normal key has been pressed. (Another function would be necessary to handle
	 * 	special key presses such as SHIFT and CONTROL.)
	 */
	void keyPressed (unsigned char key, int x, int y);

	/* keyPressed
	 * 	Called by theh glut loop whenever a special key has been pressed.
	 */
	void specialKeyPressed (int key, int x, int y);

	/* mouse
	 * 	Called by the glut loop when mouse input arrives.
	 */
	void mouse(int button, int state, int x, int y);

	/* vis
	 * 	Called by the glut loop when window gains/loses visibility.
	 */
	void vis(int visible);

	/* motion
	 *	Called by the glut loop when a mouse motion event arrives.
	 */
	void motion(int x, int y);

	/* menuHandler
	 *  Called by the glut loop when a menu item is selected
	 */
	void menuHandler(int value);

	/* recalcModelView
	 *	Called by redraw to update the orientation/scaling of the GL model.
	 */
	void recalcModelView(void);

	/*
	 * Adds a camera (represented by a pentahedron) to the GL model at index cameraN.
	 */
	void drawCameraVis(const GLfloat location[], const GLfloat quat[], int cameraN);
	void drawCameraVis(const vector<GLfloat> location, const vector<GLfloat> quat, int cameraN);
	void drawCameraVis(const GLfloat location[], const GLfloat rotM[4][4], int cameraN);


	void drawBox(void);
	void addCube(GLfloat posx, GLfloat posy, GLfloat posz, GLfloat side);
	void drawBox(GLfloat posx, GLfloat posy, GLfloat posz, GLfloat side);
	void drawBox(GLfloat posx, GLfloat posy, GLfloat posz,
			GLfloat r, GLfloat g, GLfloat b, GLfloat side);
	void drawSpherePoints();
	void addCamera(GLfloat fp, GLfloat fpw, GLfloat fph);
	void showMessage(GLfloat x, GLfloat y, GLfloat z, char *message);
	void showMessage(GLfloat x, GLfloat y, GLfloat z, char *message, GLfloat relativeSize);
	void showFixedMessage(GLfloat x, GLfloat y, GLfloat z, char *message);
	void showFixedMessage(GLfloat x, GLfloat y, GLfloat z, char *message, GLfloat relativeSize);
	void drawPointCloud(vector<vector<GLfloat> > xyzrgb,
			GLfloat side,
			long nPoints);
	vector<vector<GLfloat> > makeSpherePointCloud( GLfloat radius, int nSlices, int nStacks);
	void animate(void);
	void readCameraTextFile(string fname, int num_to_read);
	void readPointCloudTextFile(vector<vector<float> >&xyzrgb, string fname, int cols);
	void init_openGL(int argc, char **argv);
	void start_openGL();
	void setViewToCamera(int num);
	void drawHelpMenu();
	void frameOut();
};


#endif /* PCLOUD_VIEWER_H_ */
