#include "sfmDirectory.h"

void SfmDirectory::ReadDirectory(string dirn){

	DIR *dir = NULL;
	struct dirent *drnt = NULL;

	if(dirn.find("/")!=dirn.size()){
		dirn+="/";
	}
	dir = opendir(dirn.data());
	string fn;
	if(dir){
		while((drnt = readdir(dir))){
			printf("%s",drnt->d_name);
			fn.assign(drnt->d_name);
			if((fn.find(".jpg") != string::npos) || (fn.find(".JPG")) != string::npos){
				files.push_back(dirn+fn);
			}
		}
		closedir(dir);
	}
	else{
		printf("Cannot open directory '%s'\n",dirn.data());
		exit(1);
	}
	if(files.size()==0){
		// no jpegs found
	}
}
