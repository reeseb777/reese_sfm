#include "sfmExifExtractor.h"

//#define EXIF_DEBUG "sfmExifExtractor: Step "
SfmExifExtractor::SfmExifExtractor(){
	printf("initializing EXIF\n");
	numDateTimeTags = 0;
	PSEUDO_IMAGE_MARKER = 0x123;
	NumOrientations=0;
	MotorolaOrder=0;
	Sections = NULL;

	//--------------------------------------------------------------------------
	// Describes tag values

	TAG_INTEROP_INDEX          = 0x0001;
	TAG_INTEROP_VERSION        = 0x0002;
	TAG_IMAGE_WIDTH            = 0x0100;
	TAG_IMAGE_LENGTH           = 0x0101;
	TAG_BITS_PER_SAMPLE        = 0x0102;
	TAG_COMPRESSION            = 0x0103;
	TAG_PHOTOMETRIC_INTERP     = 0x0106;
	TAG_FILL_ORDER             = 0x010A;
	TAG_DOCUMENT_NAME          = 0x010D;
	TAG_IMAGE_DESCRIPTION      = 0x010E;
	TAG_MAKE                   = 0x010F;
	TAG_MODEL                  = 0x0110;
	TAG_SRIP_OFFSET            = 0x0111;
	TAG_ORIENTATION            = 0x0112;
	TAG_SAMPLES_PER_PIXEL      = 0x0115;
	TAG_ROWS_PER_STRIP         = 0x0116;
	TAG_STRIP_BYTE_COUNTS      = 0x0117;
	TAG_X_RESOLUTION           = 0x011A;
	TAG_Y_RESOLUTION           = 0x011B;
	TAG_PLANAR_CONFIGURATION   = 0x011C;
	TAG_RESOLUTION_UNIT        = 0x0128;
	TAG_TRANSFER_FUNCTION      = 0x012D;
	TAG_SOFTWARE               = 0x0131;
	TAG_DATETIME               = 0x0132;
	TAG_ARTIST                 = 0x013B;
	TAG_WHITE_POINT            = 0x013E;
	TAG_PRIMARY_CHROMATICITIES = 0x013F;
	TAG_TRANSFER_RANGE         = 0x0156;
	TAG_JPEG_PROC              = 0x0200;
	TAG_THUMBNAIL_OFFSET       = 0x0201;
	TAG_THUMBNAIL_LENGTH       = 0x0202;
	TAG_Y_CB_CR_COEFFICIENTS   = 0x0211;
	TAG_Y_CB_CR_SUB_SAMPLING   = 0x0212;
	TAG_Y_CB_CR_POSITIONING    = 0x0213;
	TAG_REFERENCE_BLACK_WHITE  = 0x0214;
	TAG_RELATED_IMAGE_WIDTH    = 0x1001;
	TAG_RELATED_IMAGE_LENGTH   = 0x1002;
	TAG_CFA_REPEAT_PATTERN_DIM = 0x828D;
	TAG_CFA_PATTERN1           = 0x828E;
	TAG_BATTERY_LEVEL          = 0x828F;
	TAG_COPYRIGHT              = 0x8298;
	TAG_EXPOSURETIME           = 0x829A;
	TAG_FNUMBER                = 0x829D;
	TAG_IPTC_NAA               = 0x83BB;
	TAG_EXIF_OFFSET            = 0x8769;
	TAG_INTER_COLOR_PROFILE    = 0x8773;
	TAG_EXPOSURE_PROGRAM       = 0x8822;
	TAG_SPECTRAL_SENSITIVITY   = 0x8824;
	TAG_GPSINFO                = 0x8825;
	TAG_ISO_EQUIVALENT         = 0x8827;
	TAG_OECF                   = 0x8828;
	TAG_EXIF_VERSION           = 0x9000;
	TAG_DATETIME_ORIGINAL      = 0x9003;
	TAG_DATETIME_DIGITIZED     = 0x9004;
	TAG_COMPONENTS_CONFIG      = 0x9101;
	TAG_CPRS_BITS_PER_PIXEL    = 0x9102;
	TAG_SHUTTERSPEED           = 0x9201;
	TAG_APERTURE               = 0x9202;
	TAG_BRIGHTNESS_VALUE       = 0x9203;
	TAG_EXPOSURE_BIAS          = 0x9204;
	TAG_MAXAPERTURE            = 0x9205;
	TAG_SUBJECT_DISTANCE       = 0x9206;
	TAG_METERING_MODE          = 0x9207;
	TAG_LIGHT_SOURCE           = 0x9208;
	TAG_FLASH                  = 0x9209;
	TAG_FOCALLENGTH            = 0x920A;
	TAG_SUBJECTAREA            = 0x9214;
	TAG_MAKER_NOTE             = 0x927C;
	TAG_USERCOMMENT            = 0x9286;
	TAG_SUBSEC_TIME            = 0x9290;
	TAG_SUBSEC_TIME_ORIG       = 0x9291;
	TAG_SUBSEC_TIME_DIG        = 0x9292;

	TAG_WINXP_TITLE            = 0x9c9b; // Windows XP - not part of exif standard.
	TAG_WINXP_COMMENT          = 0x9c9c; // Windows XP - not part of exif standard.
	TAG_WINXP_AUTHOR           = 0x9c9d; // Windows XP - not part of exif standard.
	TAG_WINXP_KEYWORDS         = 0x9c9e; // Windows XP - not part of exif standard.
	TAG_WINXP_SUBJECT          = 0x9c9f; // Windows XP - not part of exif standard.

	TAG_FLASH_PIX_VERSION      = 0xA000;
	TAG_COLOR_SPACE            = 0xA001;
	TAG_PIXEL_X_DIMENSION      = 0xA002;
	TAG_PIXEL_Y_DIMENSION      = 0xA003;
	TAG_RELATED_AUDIO_FILE     = 0xA004;
	TAG_INTEROP_OFFSET         = 0xA005;
	TAG_FLASH_ENERGY           = 0xA20B;
	TAG_SPATIAL_FREQ_RESP      = 0xA20C;
	TAG_FOCAL_PLANE_XRES       = 0xA20E;
	TAG_FOCAL_PLANE_YRES       = 0xA20F;
	TAG_FOCAL_PLANE_UNITS      = 0xA210;
	TAG_SUBJECT_LOCATION       = 0xA214;
	TAG_EXPOSURE_INDEX         = 0xA215;
	TAG_SENSING_METHOD         = 0xA217;
	TAG_FILE_SOURCE            = 0xA300;
	TAG_SCENE_TYPE             = 0xA301;
	TAG_CFA_PATTERN            = 0xA302;
	TAG_CUSTOM_RENDERED        = 0xA401;
	TAG_EXPOSURE_MODE          = 0xA402;
	TAG_WHITEBALANCE           = 0xA403;
	TAG_DIGITALZOOMRATIO       = 0xA404;
	TAG_FOCALLENGTH_35MM       = 0xA405;
	TAG_SCENE_CAPTURE_TYPE     = 0xA406;
	TAG_GAIN_CONTROL           = 0xA407;
	TAG_CONTRAST               = 0xA408;
	TAG_SATURATION             = 0xA409;
	TAG_SHARPNESS              = 0xA40A;
	TAG_DISTANCE_RANGE         = 0xA40C;
	TAG_IMAGE_UNIQUE_ID        = 0xA420;

	TagTable_t tt_temp;

	tt_temp.Tag = TAG_INTEROP_INDEX; 			tt_temp.Desc = "InteropIndex"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_INTEROP_VERSION; 			tt_temp.Desc = "InteropVersion"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_IMAGE_WIDTH; 				tt_temp.Desc = "ImageWidth"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_IMAGE_LENGTH; 			tt_temp.Desc = "ImageLength"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_BITS_PER_SAMPLE; 			tt_temp.Desc = "BitsPerSample"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_COMPRESSION; 				tt_temp.Desc = "Compression"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_PHOTOMETRIC_INTERP; 		tt_temp.Desc = "PhotometricInterpretation"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FILL_ORDER; 				tt_temp.Desc = "FillOrder"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_DOCUMENT_NAME; 			tt_temp.Desc = "DocumentName"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_IMAGE_DESCRIPTION; 		tt_temp.Desc = "ImageDescription"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_MAKE; 					tt_temp.Desc = "Make"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_MODEL; 					tt_temp.Desc = "Model"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SRIP_OFFSET; 				tt_temp.Desc = "StripOffsets"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_ORIENTATION; 				tt_temp.Desc = "Orientation"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SAMPLES_PER_PIXEL; 		tt_temp.Desc = "SamplesPerPixel"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_ROWS_PER_STRIP; 			tt_temp.Desc = "RowsPerStrip"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_STRIP_BYTE_COUNTS; 		tt_temp.Desc = "StripByteCounts"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_X_RESOLUTION; 			tt_temp.Desc = "XResolution"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_Y_RESOLUTION; 			tt_temp.Desc = "YResolution"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_PLANAR_CONFIGURATION; 	tt_temp.Desc = "PlanarConfiguration"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_RESOLUTION_UNIT; 			tt_temp.Desc = "ResolutionUnit"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_TRANSFER_FUNCTION; 		tt_temp.Desc = "TransferFunction"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SOFTWARE; 				tt_temp.Desc = "Software"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_DATETIME; 				tt_temp.Desc = "DateTime"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_ARTIST; 					tt_temp.Desc = "Artist"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_WHITE_POINT; 				tt_temp.Desc = "WhitePoint"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_PRIMARY_CHROMATICITIES; 	tt_temp.Desc = "PrimaryChromaticities"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_TRANSFER_RANGE; 			tt_temp.Desc = "TransferRange"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_JPEG_PROC; 				tt_temp.Desc = "JPEGProc"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_THUMBNAIL_OFFSET; 		tt_temp.Desc = "ThumbnailOffset"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_THUMBNAIL_LENGTH; 		tt_temp.Desc = "ThumbnailLength"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_Y_CB_CR_COEFFICIENTS; 	tt_temp.Desc = "YCbCrCoefficients"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_Y_CB_CR_SUB_SAMPLING; 	tt_temp.Desc = "YCbCrSubSampling"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_Y_CB_CR_POSITIONING; 		tt_temp.Desc = "YCbCrPositioning"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_REFERENCE_BLACK_WHITE; 	tt_temp.Desc = "ReferenceBlackWhite"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_RELATED_IMAGE_WIDTH; 		tt_temp.Desc = "RelatedImageWidth"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_RELATED_IMAGE_LENGTH; 	tt_temp.Desc = "RelatedImageLength"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_CFA_REPEAT_PATTERN_DIM; 	tt_temp.Desc = "CFARepeatPatternDim"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_CFA_PATTERN1; 			tt_temp.Desc = "CFAPattern"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_BATTERY_LEVEL; 			tt_temp.Desc = "BatteryLevel"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_COPYRIGHT; 				tt_temp.Desc = "Copyright"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_EXPOSURETIME; 			tt_temp.Desc = "ExposureTime"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FNUMBER; 					tt_temp.Desc = "FNumber"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_IPTC_NAA; 				tt_temp.Desc = "IPTC/NAA"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_EXIF_OFFSET; 				tt_temp.Desc = "ExifOffset"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_INTER_COLOR_PROFILE; 		tt_temp.Desc = "InterColorProfile"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_EXPOSURE_PROGRAM; 		tt_temp.Desc = "ExposureProgram"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SPECTRAL_SENSITIVITY; 	tt_temp.Desc = "SpectralSensitivity"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_GPSINFO; 					tt_temp.Desc = "GPS Dir offset"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_ISO_EQUIVALENT; 			tt_temp.Desc = "ISOSpeedRatings"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_OECF; 					tt_temp.Desc = "OECF"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_EXIF_VERSION; 			tt_temp.Desc = "ExifVersion"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_DATETIME_ORIGINAL; 		tt_temp.Desc = "DateTimeOriginal"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_DATETIME_DIGITIZED; 		tt_temp.Desc = "DateTimeDigitized"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_COMPONENTS_CONFIG; 		tt_temp.Desc = "ComponentsConfiguration"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_CPRS_BITS_PER_PIXEL; 		tt_temp.Desc = "CompressedBitsPerPixel"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SHUTTERSPEED; 			tt_temp.Desc = "ShutterSpeedValue"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_APERTURE; 				tt_temp.Desc = "ApertureValue"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_BRIGHTNESS_VALUE; 		tt_temp.Desc = "BrightnessValue"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_EXPOSURE_BIAS; 			tt_temp.Desc = "ExposureBiasValue"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_MAXAPERTURE; 				tt_temp.Desc = "MaxApertureValue"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SUBJECT_DISTANCE; 		tt_temp.Desc = "SubjectDistance"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_METERING_MODE; 			tt_temp.Desc = "MeteringMode"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_LIGHT_SOURCE; 			tt_temp.Desc = "LightSource"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FLASH; 					tt_temp.Desc = "Flash"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FOCALLENGTH; 				tt_temp.Desc = "FocalLength"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_MAKER_NOTE; 				tt_temp.Desc = "MakerNote"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_USERCOMMENT; 				tt_temp.Desc = "UserComment"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SUBSEC_TIME; 				tt_temp.Desc = "SubSecTime"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SUBSEC_TIME_ORIG; 		tt_temp.Desc = "SubSecTimeOriginal"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SUBSEC_TIME_DIG; 			tt_temp.Desc = "SubSecTimeDigitized"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_WINXP_TITLE; 				tt_temp.Desc = "Windows-XP Title"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_WINXP_COMMENT; 			tt_temp.Desc = "Windows-XP comment"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_WINXP_AUTHOR; 			tt_temp.Desc = "Windows-XP author"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_WINXP_KEYWORDS; 			tt_temp.Desc = "Windows-XP keywords"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_WINXP_SUBJECT; 			tt_temp.Desc = "Windows-XP subject"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FLASH_PIX_VERSION; 		tt_temp.Desc = "FlashPixVersion"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_COLOR_SPACE; 				tt_temp.Desc = "ColorSpace"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_PIXEL_X_DIMENSION; 		tt_temp.Desc = "ExifImageWidth"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_PIXEL_Y_DIMENSION; 		tt_temp.Desc = "ExifImageLength"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_RELATED_AUDIO_FILE; 		tt_temp.Desc = "RelatedAudioFile"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_INTEROP_OFFSET; 			tt_temp.Desc = "InteroperabilityOffset"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FLASH_ENERGY; 			tt_temp.Desc = "FlashEnergy"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SPATIAL_FREQ_RESP; 		tt_temp.Desc = "SpatialFrequencyResponse"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FOCAL_PLANE_XRES; 		tt_temp.Desc = "FocalPlaneXResolution"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FOCAL_PLANE_YRES; 		tt_temp.Desc = "FocalPlaneYResolution"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FOCAL_PLANE_UNITS; 		tt_temp.Desc = "FocalPlaneResolutionUnit"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SUBJECT_LOCATION; 		tt_temp.Desc = "SubjectLocation"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_EXPOSURE_INDEX; 			tt_temp.Desc = "ExposureIndex"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SENSING_METHOD; 			tt_temp.Desc = "SensingMethod"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FILE_SOURCE; 				tt_temp.Desc = "FileSource"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SCENE_TYPE; 				tt_temp.Desc = "SceneType"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_CFA_PATTERN; 				tt_temp.Desc = "CFA Pattern"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_CUSTOM_RENDERED; 			tt_temp.Desc = "CustomRendered"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_EXPOSURE_MODE; 			tt_temp.Desc = "ExposureMode"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_WHITEBALANCE; 			tt_temp.Desc = "WhiteBalance"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_DIGITALZOOMRATIO; 		tt_temp.Desc = "DigitalZoomRatio"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_FOCALLENGTH_35MM; 		tt_temp.Desc = "FocalLengthIn35mmFilm"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SUBJECTAREA; 				tt_temp.Desc = "SubjectArea"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SCENE_CAPTURE_TYPE; 		tt_temp.Desc = "SceneCaptureType"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_GAIN_CONTROL; 			tt_temp.Desc = "GainControl"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_CONTRAST; 				tt_temp.Desc = "Contrast"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SATURATION; 				tt_temp.Desc = "Saturation"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_SHARPNESS; 				tt_temp.Desc = "Sharpness"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_DISTANCE_RANGE; 			tt_temp.Desc = "SubjectDistanceRange"; TagTable.push_back(tt_temp);
	tt_temp.Tag = TAG_IMAGE_UNIQUE_ID; 			tt_temp.Desc = "ImageUniqueId"; TagTable.push_back(tt_temp);

	TAG_TABLE_SIZE = TagTable.size();

	BytesPerFormat.push_back(0);
	BytesPerFormat.push_back(1);
	BytesPerFormat.push_back(1);
	BytesPerFormat.push_back(2);
	BytesPerFormat.push_back(4);
	BytesPerFormat.push_back(8);
	BytesPerFormat.push_back(1);
	BytesPerFormat.push_back(1);
	BytesPerFormat.push_back(2);
	BytesPerFormat.push_back(4);
	BytesPerFormat.push_back(8);
	BytesPerFormat.push_back(4);
	BytesPerFormat.push_back(8);

	TAG_GPS_LAT_REF    = 1;
	TAG_GPS_LAT        = 2;
	TAG_GPS_LONG_REF   = 3;
	TAG_GPS_LONG       = 4;
	TAG_GPS_ALT_REF    = 5;
	TAG_GPS_ALT        = 6;

	NUM_FORMATS = 12;

	FMT_BYTE      = 1;
	FMT_STRING    = 2;
	FMT_USHORT    = 3;
	FMT_ULONG     = 4;
	FMT_URATIONAL = 5;
	FMT_SBYTE     = 6;
	FMT_UNDEFINED = 7;
	FMT_SSHORT    = 8;
	FMT_SLONG     = 9;
	FMT_SRATIONAL =10;
	FMT_SINGLE    =11;
	FMT_DOUBLE    =12;

	GpsTags.push_back("VersionID       ");//0x00
	GpsTags.push_back("LatitudeRef     ");//0x01
	GpsTags.push_back("Latitude        ");//0x02
	GpsTags.push_back("LongitudeRef    ");//0x03
	GpsTags.push_back("Longitude       ");//0x04
	GpsTags.push_back("AltitudeRef     ");//0x05
	GpsTags.push_back("Altitude        ");//0x06
	GpsTags.push_back("TimeStamp       ");//0x07
	GpsTags.push_back("Satellites      ");//0x08
	GpsTags.push_back("Status          ");//0x09
	GpsTags.push_back("MeasureMode     ");//0x0A
	GpsTags.push_back("DOP             ");//0x0B
	GpsTags.push_back("SpeedRef        ");//0x0C
	GpsTags.push_back("Speed           ");//0x0D
	GpsTags.push_back("TrackRef        ");//0x0E
	GpsTags.push_back("Track           ");//0x0F
	GpsTags.push_back("ImgDirectionRef ");//0x10
	GpsTags.push_back("ImgDirection    ");//0x11
	GpsTags.push_back("MapDatum        ");//0x12
	GpsTags.push_back("DestLatitudeRef ");//0x13
	GpsTags.push_back("DestLatitude    ");//0x14
	GpsTags.push_back("DestLongitudeRef");//0x15
	GpsTags.push_back("DestLongitude   ");//0x16
	GpsTags.push_back("DestBearingRef  ");//0x17
	GpsTags.push_back("DestBearing     ");//0x18
	GpsTags.push_back("DestDistanceRef ");//0x19
	GpsTags.push_back("DestDistance    ");//0x1A
	GpsTags.push_back("ProcessingMethod");//0x1B
	GpsTags.push_back("AreaInformation ");//0x1C
	GpsTags.push_back("DateStamp       ");//0x1D
	GpsTags.push_back("Differential    ");//0x1E


	DumpExifMap = FALSE;
	ShowTags	= FALSE;
	SupressNonFatalErrors = FALSE;

	M_SOF0  = 0xC0;          // Start Of Frame N
	M_SOF1  = 0xC1;          // N indicates which compression process
	M_SOF2  = 0xC2;          // Only SOF0-SOF2 are now in common use
	M_SOF3  = 0xC3;
	M_SOF5  = 0xC5;         // NB: codes C4 and CC are NOT SOF markers
	M_SOF6  = 0xC6;
	M_SOF7  = 0xC7;
	M_SOF9  = 0xC9;
	M_SOF10 = 0xCA;
	M_SOF11 = 0xCB;
	M_SOF13 = 0xCD;
	M_SOF14 = 0xCE;
	M_SOF15 = 0xCF;
	M_SOI   = 0xD8;         // Start Of Image (beginning of datastream)
	M_EOI   = 0xD9;        // End Of Image (end of datastream)
	M_SOS   = 0xDA;       // Start Of Scan (begins compressed data)
	M_JFIF  = 0xE0;      // Jfif marker
	M_EXIF  = 0xE1;     // Exif marker.  Also used for XMP data!
	M_XMP   = 0x10E1;    // Not a real tag (same value in file as Exif!)
	M_COM   = 0xFE;   // COMment
	M_DQT   = 0xDB;
	M_DHT   = 0xC4;
	M_DRI   = 0xDD;
	M_IPTC  = 0xED;         // IPTC marker


}

SfmExifExtractor::~SfmExifExtractor(){
}

void SfmExifExtractor::Extract(SfmImage *img){
	// This function is based off of jhead: http://www.sentex.net/~mwandel/jhead/

	CopyHereSfmImageParams(img);
#ifdef EXIF_DEBUG
	printf("%s 1\n",EXIF_DEBUG);
#endif
	int Modified = FALSE;
	ReadMode_t ReadMode;

	if (strlen(imgName.data()) >= PATH_MAX-1){
		// Protect against buffer overruns in strcpy / strcat's on filename
		ErrFatal("filename too long");
	}

	ReadMode = READ_METADATA;
	CurrentFile = imgName.data();
	FilesMatched = 1;
#ifdef EXIF_DEBUG
	printf("%s 2\n",EXIF_DEBUG);
#endif

	ResetJpgfile();

#ifdef EXIF_DEBUG
	printf("%s 3\n",EXIF_DEBUG);
#endif

	if (!ReadJpegFile(imgName.data(), ReadMode)) return;

#ifdef EXIF_DEBUG
	printf("%s 4\n",EXIF_DEBUG);
#endif

	CopyThereSfmImageParams(img);

#ifdef EXIF_DEBUG
	printf("%s 5\n",EXIF_DEBUG);
#endif
}

void SfmExifExtractor::CopyHereSfmImageParams(SfmImage *img){

	imgName 				= img->imgName;
	FileDateTime		 	= img->FileDateTime;
	FileSize 				= img->FileSize;
	Height					= img->Height;
	Width					= img->Width;
	Orientation				= img->Orientation;
	IsColor					= img->IsColor;
	Process					= img->Process;
	FlashUsed				= img->FlashUsed;
	FocalLength				= img->FocalLength;
	FocalLength35mmEquiv 	= img->FocalLength35mmEquiv;
	ExposureTime			= img->ExposureTime;
	ApertureFNumber			= img->ApertureFNumber;
	Distance				= img->Distance;
	CCDWidth				= img->CCDWidth;
	ExposureBias			= img->ExposureBias;
	DigitalZoomRatio		= img->DigitalZoomRatio;
	Whitebalance			= img->Whitebalance;
	MeteringMode			= img->MeteringMode;
	ExposureProgram			= img->ExposureProgram;
	ExposureMode			= img->ExposureMode;
	ISOequivalent			= img->ISOequivalent;
	LightSource				= img->LightSource;
	DistanceRange			= img->DistanceRange;
	xResolution				= img->xResolution;
	yResolution				= img->yResolution;
	ResolutionUnit			= img->ResolutionUnit;
	CommentWidthchars		= img->CommentWidthchars;
	ThumbnailOffset			= img->ThumbnailOffset;
	ThumbnailSize			= img->ThumbnailSize;
	LargestExifOffset		= img->LargestExifOffset;
	numDateTimeTags			= img->numDateTimeTags;
	GpsInfoPresent			= img->GpsInfoPresent;


	memcpy(CameraMake, 			img->CameraMake, 		32);
	memcpy(CameraModel, 		img->CameraModel, 		40);
	memcpy(DateTime, 			img->DateTime, 			20);
	memcpy(Comments, 			img->Comments, 			MAX_COMMENT_SIZE);
	memcpy(DateTimeOffsets, 	img->DateTimeOffsets, 	sizeof(int)*MAX_DATE_COPIES);
	memcpy(GpsLat, 				img->GpsLat, 			31);
	memcpy(GpsLong, 			img->GpsLong, 			31);
	memcpy(GpsAlt,				img->GpsAlt, 			20);

}
void SfmExifExtractor::CopyThereSfmImageParams(SfmImage *img){

	img->imgName 				= imgName;
	img->FileDateTime 			= FileDateTime;
	img->FileSize 				= FileSize;
	img->Height					= Height;
	img->Width					= Width;
	img->Orientation			= Orientation;
	img->IsColor				= IsColor;
	img->Process				= Process;
	img->FlashUsed				= FlashUsed;
	img->FocalLength			= FocalLength;
	img->FocalLength35mmEquiv 	= FocalLength35mmEquiv;
	img->ExposureTime			= ExposureTime;
	img->ApertureFNumber		= ApertureFNumber;
	img->Distance				= Distance;
	img->CCDWidth				= CCDWidth;
	img->ExposureBias			= ExposureBias;
	img->DigitalZoomRatio		= DigitalZoomRatio;
	img->Whitebalance			= Whitebalance;
	img->MeteringMode			= MeteringMode;
	img->ExposureProgram		= ExposureProgram;
	img->ExposureMode			= ExposureMode;
	img->ISOequivalent			= ISOequivalent;
	img->LightSource			= LightSource;
	img->DistanceRange			= DistanceRange;
	img->xResolution			= xResolution;
	img->yResolution			= yResolution;
	img->ResolutionUnit			= ResolutionUnit;
	img->CommentWidthchars		= CommentWidthchars;
	img->ThumbnailOffset		= ThumbnailOffset;
	img->ThumbnailSize			= ThumbnailSize;
	img->LargestExifOffset		= LargestExifOffset;
	img->numDateTimeTags		= numDateTimeTags;
	img->GpsInfoPresent			= GpsInfoPresent;


	memcpy(img->CameraMake, 		CameraMake, 	32);
	memcpy(img->CameraModel,		CameraModel, 	40);
	memcpy(img->DateTime, 			DateTime, 		20);
	memcpy(img->Comments, 			Comments, 		MAX_COMMENT_SIZE);
	memcpy(img->DateTimeOffsets, 	DateTimeOffsets, sizeof(int)*MAX_DATE_COPIES);
	memcpy(img->GpsLat, 			GpsLat, 		31);
	memcpy(img->GpsLong, 			GpsLong, 		31);
	memcpy(img->GpsAlt, 			GpsAlt, 		20);

}

void SfmExifExtractor::ErrFatal(string msg){
    fprintf(stderr,"\nError : %s\n", msg.data());
    if (CurrentFile) fprintf(stderr,"in file '%s'\n",CurrentFile);
    exit(EXIT_FAILURE);
}

void SfmExifExtractor::ResetJpgfile()
{
    if (Sections == NULL){
        Sections = (Section_t *)malloc(sizeof(Section_t)*5);
        SectionsAllocated = 5;
    }

    SectionsRead = 0;
    HaveAll = 0;
}

//--------------------------------------------------------------------------
// Read image data.
//--------------------------------------------------------------------------
int SfmExifExtractor::ReadJpegFile(const char * FileName, ReadMode_t ReadMode)
{
    FILE * infile;
    int ret;
#ifdef EXIF_DEBUG
	printf("%s ReadJpegFile 1\n",EXIF_DEBUG);
#endif
    infile = fopen(FileName, "rb"); // Unix ignores 'b', windows needs it.
#ifdef EXIF_DEBUG
	printf("%s ReadJpegFile 2\n",EXIF_DEBUG);
#endif

    if (infile == NULL) {
        fprintf(stderr, "can't open '%s'\n", FileName);
        return FALSE;
    }
#ifdef EXIF_DEBUG
	printf("%s ReadJpegFile 3\n",EXIF_DEBUG);
#endif


    // Scan the JPEG headers.
    ret = ReadJpegSections(infile, ReadMode);
    if (!ret){
        fprintf(stderr,"Not JPEG: %s\n",FileName);
    }
#ifdef EXIF_DEBUG
	printf("%s ReadJpegFile 4\n",EXIF_DEBUG);
#endif

    fclose(infile);
#ifdef EXIF_DEBUG
	printf("%s ReadJpegFile 5\n",EXIF_DEBUG);
#endif

    if (ret == FALSE){
        DiscardData();
    }
    return ret;
}

//--------------------------------------------------------------------------
// Parse the marker stream until SOS or EOI is seen;
//--------------------------------------------------------------------------
int SfmExifExtractor::ReadJpegSections (FILE * infile, ReadMode_t ReadMode)
{
    int a;
    int HaveCom = FALSE;
#ifdef EXIF_DEBUG
	printf("%s ReadJpegSections 1\n",EXIF_DEBUG);
#endif
    a = fgetc(infile);

    if (a != 0xff || fgetc(infile) != M_SOI){
        return FALSE;
    }

    JfifHeader.XDensity = JfifHeader.YDensity = 300;
    JfifHeader.ResolutionUnits = 1;

    for(;;){
        int itemlen;
        int prev;
        int marker = 0;
        int ll,lh, got;
        uchar * Data;
#ifdef EXIF_DEBUG
	printf("%s ReadJpegSections 2\n",EXIF_DEBUG);
#endif

        CheckSectionsAllocated();

        prev = 0;
        for (a=0;;a++){
            marker = fgetc(infile);
            if (marker != 0xff && prev == 0xff) break;
            prev = marker;
        }
#ifdef EXIF_DEBUG
	printf("%s ReadJpegSections 3\n",EXIF_DEBUG);
#endif

        if (a > 10){
            ErrNonfatal("Extraneous %d padding bytes before section %02X",a-1,marker);
        }

        Sections[SectionsRead].Type = marker;

        // Read the length of the section.
        lh = fgetc(infile);
        ll = fgetc(infile);

        itemlen = (lh << 8) | ll;

        if (itemlen < 2){
            ErrFatal("invalid marker");
        }

        Sections[SectionsRead].Size = itemlen;

        Data = (uchar *)malloc(itemlen);
        if (Data == NULL){
            ErrFatal("Could not allocate memory");
        }
        Sections[SectionsRead].Data = Data;

        // Store first two pre-read bytes.
        Data[0] = (uchar)lh;
        Data[1] = (uchar)ll;

        got = fread(Data+2, 1, itemlen-2, infile); // Read the whole section.
        if (got != itemlen-2){
            ErrFatal("Premature end of file?");
        }
        SectionsRead += 1;
#ifdef EXIF_DEBUG
	printf("%s ReadJpegSections 4: %X \n",EXIF_DEBUG,marker);
#endif
		if(marker == M_SOS){   // stop before hitting compressed data

			// If reading entire image is requested, read the rest of the data.
			if (ReadMode & READ_IMAGE){
				int cp, ep, size;
				// Determine how much file is left.
				cp = ftell(infile);
				fseek(infile, 0, SEEK_END);
				ep = ftell(infile);
				fseek(infile, cp, SEEK_SET);

				size = ep-cp;
				Data = (uchar *)malloc(size);
				if (Data == NULL){
					ErrFatal("could not allocate data for entire image");
				}

				got = fread(Data, 1, size, infile);
				if (got != size){
					ErrFatal("could not read the rest of the image");
				}

				CheckSectionsAllocated();
				Sections[SectionsRead].Data = Data;
				Sections[SectionsRead].Size = size;
				Sections[SectionsRead].Type = PSEUDO_IMAGE_MARKER;
				SectionsRead ++;
				HaveAll = 1;
			}
			return TRUE;
		}
		else if(marker == M_EOI){   // in case it's a tables-only JPEG stream
			fprintf(stderr,"No image in jpeg!\n");
			return FALSE;
		}

		else if(marker == M_COM){ // Comment section
			if (HaveCom || ((ReadMode & READ_METADATA) == 0)){
				// Discard this section.
				free(Sections[--SectionsRead].Data);
			}else{
				process_COM(Data, itemlen);
				HaveCom = TRUE;
			}
		}

		else if(marker == M_JFIF){
			// Regular jpegs always have this tag, exif images have the exif
			// marker instead, althogh ACDsee will write images with both markers.
			// this program will re-create this marker on absence of exif marker.
			// hence no need to keep the copy from the file.
			if (memcmp(Data+2, "JFIF\0",5)){
				fprintf(stderr,"Header missing JFIF marker\n");
			}
			if (itemlen < 16){
				fprintf(stderr,"Jfif header too short\n");
				goto ignore;
			}

			JfifHeader.Present = TRUE;
			JfifHeader.ResolutionUnits = Data[9];
			JfifHeader.XDensity = (Data[10]<<8) | Data[11];
			JfifHeader.YDensity = (Data[12]<<8) | Data[13];
			if (ShowTags){
				printf("JFIF SOI marker: Units: %d ",JfifHeader.ResolutionUnits);
				switch(JfifHeader.ResolutionUnits){
					case 0: printf("(aspect ratio)"); break;
					case 1: printf("(dots per inch)"); break;
					case 2: printf("(dots per cm)"); break;
					default: printf("(unknown)"); break;
				}
				printf("  X-density=%d Y-density=%d\n",JfifHeader.XDensity, JfifHeader.YDensity);

				if (Data[14] || Data[15]){
					fprintf(stderr,"Ignoring jfif header thumbnail\n");
				}
			}

			ignore:

			free(Sections[--SectionsRead].Data);
		}

		else if(marker == M_EXIF){
#ifdef EXIF_DEBUG
	printf("%s ReadJpegSections M_EXIF 1\n",EXIF_DEBUG);
#endif
			// There can be different section using the same marker.
			if (ReadMode & READ_METADATA){
#ifdef EXIF_DEBUG
	printf("%s ReadJpegSections M_EXIF 2\n",EXIF_DEBUG);
#endif
				if (memcmp(Data+2, "Exif", 4) == 0){
#ifdef EXIF_DEBUG
	printf("%s ReadJpegSections M_EXIF 3\n",EXIF_DEBUG);
#endif
					process_EXIF(Data, itemlen);
					break;
				}else if (memcmp(Data+2, "http:", 5) == 0){
#ifdef EXIF_DEBUG
	printf("%s ReadJpegSections M_EXIF 4\n",EXIF_DEBUG);
#endif
					Sections[SectionsRead-1].Type = M_XMP; // Change tag for internal purposes.
					if (ShowTags){
						printf("Image cotains XMP section, %d bytes long\n", itemlen);
						if (ShowTags){
							ShowXmp(Sections[SectionsRead-1]);
						}
					}
					break;
				}
			}
#ifdef EXIF_DEBUG
	printf("%s ReadJpegSections M_EXIF end\n",EXIF_DEBUG);
#endif
			// Oterwise, discard this section.
			free(Sections[--SectionsRead].Data);
		}

		else if(marker == M_IPTC){
			if (ReadMode & READ_METADATA){
				if (ShowTags){
					printf("Image cotains IPTC section, %d bytes long\n", itemlen);
				}
				// Note: We just store the IPTC section.  Its relatively straightforward
				// and we don't act on any part of it, so just display it at parse time.
			}else{
				free(Sections[--SectionsRead].Data);
			}
		}

		else if((marker == M_SOF0) ||
				(marker == M_SOF1) ||
				(marker == M_SOF2) ||
				(marker == M_SOF3) ||
				(marker == M_SOF5) ||
				(marker == M_SOF6) ||
				(marker == M_SOF7) ||
				(marker == M_SOF9) ||
				(marker == M_SOF10) ||
				(marker == M_SOF11) ||
				(marker == M_SOF13) ||
				(marker == M_SOF14) ||
				(marker == M_SOF15)){

			process_SOFn(Data, marker);
		}
		else{
			// Skip any other sections.
			if (ShowTags){
				printf("Jpeg section marker 0x%02x size %d\n",marker, itemlen);
			}
		}
    }
#ifdef EXIF_DEBUG
	printf("%s ReadJpegSections 5\n",EXIF_DEBUG);
#endif
    return TRUE;
}

//--------------------------------------------------------------------------
// Check sections array to see if it needs to be increased in size.
//--------------------------------------------------------------------------
void SfmExifExtractor::CheckSectionsAllocated()
{
    if (SectionsRead > SectionsAllocated){
        ErrFatal("allocation screwup");
    }
    if (SectionsRead >= SectionsAllocated){
        SectionsAllocated += SectionsAllocated/2;
        Sections = (Section_t *)realloc(Sections, sizeof(Section_t)*SectionsAllocated);
        if (Sections == NULL){
            ErrFatal("could not allocate data for entire image");
        }
    }
}

//--------------------------------------------------------------------------
// Process a COM marker.
// We want to print out the marker contents as legible text;
// we must guard against random junk and varying newline representations.
//--------------------------------------------------------------------------
void SfmExifExtractor::process_COM (const uchar * Data, int length)
{
    int ch;
    char Comment[MAX_COMMENT_SIZE+1];
    int nch;
    int a;

    nch = 0;

    if (length > MAX_COMMENT_SIZE) length = MAX_COMMENT_SIZE; // Truncate if it won't fit in our structure.

    for (a=2;a<length;a++){
        ch = Data[a];

        if (ch == '\r' && Data[a+1] == '\n') continue; // Remove cr followed by lf.

        if (ch >= 32 || ch == '\n' || ch == '\t'){
            Comment[nch++] = (char)ch;
        }else{
            Comment[nch++] = '?';
        }
    }

    Comment[nch] = '\0'; // Null terminate

    if (ShowTags){
        printf("COM marker comment: %s\n",Comment);
    }

    strcpy(Comments,Comment);
    CommentWidthchars = 0;
}

//--------------------------------------------------------------------------
// Process a SOFn marker.  This is useful for the image dimensions
//--------------------------------------------------------------------------
void SfmExifExtractor::process_SOFn (const uchar * Data, int marker)
{
    int data_precision, num_components;

    data_precision = Data[2];
    Height = Get16m(Data+3);
    Width = Get16m(Data+5);
    num_components = Data[7];

    if (num_components == 3){
        IsColor = 1;
    }else{
        IsColor = 0;
    }

    Process = marker;

    if (ShowTags){
        printf("JPEG image is %uw * %uh, %d color components, %d bits per sample\n",
                   Width, Height, num_components, data_precision);
    }
}

//--------------------------------------------------------------------------
// Get 16 bits motorola order (always) for jpeg header stuff.
//--------------------------------------------------------------------------
int SfmExifExtractor::Get16m(const void * Short)
{
    return (((uchar *)Short)[0] << 8) | ((uchar *)Short)[1];
}

//--------------------------------------------------------------------------
// Convert a 16 bit unsigned value to file's native byte order
//--------------------------------------------------------------------------
void SfmExifExtractor::Put16u(void * Short, unsigned short PutValue)
{
    if (MotorolaOrder){
        ((uchar *)Short)[0] = (uchar)(PutValue>>8);
        ((uchar *)Short)[1] = (uchar)PutValue;
    }else{
        ((uchar *)Short)[0] = (uchar)PutValue;
        ((uchar *)Short)[1] = (uchar)(PutValue>>8);
    }
}

//--------------------------------------------------------------------------
// Convert a 16 bit unsigned value from file's native byte order
//--------------------------------------------------------------------------
int SfmExifExtractor::Get16u(void * Short)
{
    if (MotorolaOrder){
        return (((uchar *)Short)[0] << 8) | ((uchar *)Short)[1];
    }else{
        return (((uchar *)Short)[1] << 8) | ((uchar *)Short)[0];
    }
}

//--------------------------------------------------------------------------
// Convert a 32 bit signed value from file's native byte order
//--------------------------------------------------------------------------
int SfmExifExtractor::Get32s(void * Long)
{
    if (MotorolaOrder){
        return  ((( char *)Long)[0] << 24) | (((uchar *)Long)[1] << 16)
              | (((uchar *)Long)[2] << 8 ) | (((uchar *)Long)[3] << 0 );
    }else{
        return  ((( char *)Long)[3] << 24) | (((uchar *)Long)[2] << 16)
              | (((uchar *)Long)[1] << 8 ) | (((uchar *)Long)[0] << 0 );
    }
}

//--------------------------------------------------------------------------
// Convert a 32 bit unsigned value to file's native byte order
//--------------------------------------------------------------------------
void SfmExifExtractor::Put32u(void * Value, unsigned PutValue)
{
    if (MotorolaOrder){
        ((uchar *)Value)[0] = (uchar)(PutValue>>24);
        ((uchar *)Value)[1] = (uchar)(PutValue>>16);
        ((uchar *)Value)[2] = (uchar)(PutValue>>8);
        ((uchar *)Value)[3] = (uchar)PutValue;
    }else{
        ((uchar *)Value)[0] = (uchar)PutValue;
        ((uchar *)Value)[1] = (uchar)(PutValue>>8);
        ((uchar *)Value)[2] = (uchar)(PutValue>>16);
        ((uchar *)Value)[3] = (uchar)(PutValue>>24);
    }
}

//--------------------------------------------------------------------------
// Convert a 32 bit unsigned value from file's native byte order
//--------------------------------------------------------------------------
unsigned SfmExifExtractor::Get32u(void * Long)
{
    return (unsigned)Get32s(Long) & 0xffffffff;
}


//--------------------------------------------------------------------------
// Discard read data.
//--------------------------------------------------------------------------
void SfmExifExtractor::DiscardData()
{
    int a;

    for (a=0;a<SectionsRead;a++){
        free(Sections[a].Data);
    }

 //   memset(&ImageInfo, 0, sizeof(ImageInfo));
    SectionsRead = 0;
    HaveAll = 0;
}

//--------------------------------------------------------------------------
// Dump contents of XMP section
//--------------------------------------------------------------------------
void SfmExifExtractor::ShowXmp(Section_t XmpSection)
{
    unsigned char * Data;
    char OutLine[101];
    int OutLineChars;
    int NonBlank;
    unsigned a;
    NonBlank = 0;
    Data = XmpSection.Data;
    OutLineChars = 0;


    for (a=0;a<XmpSection.Size;a++){
        if (Data[a] >= 32 && Data[a] < 128){
            OutLine[OutLineChars++] = Data[a];
            if (Data[a] != ' ') NonBlank |= 1;
        }else{
            if (Data[a] != '\n'){
                OutLine[OutLineChars++] = '?';
            }
        }
        if (Data[a] == '\n' || OutLineChars >= 100){
            OutLine[OutLineChars] = 0;
            if (NonBlank){
                puts(OutLine);
            }
            NonBlank = (NonBlank & 1) << 1;
            OutLineChars = 0;
        }
    }
}

//--------------------------------------------------------------------------
// Process a EXIF marker
// Describes all the drivel that most digital cameras include...
//--------------------------------------------------------------------------
void SfmExifExtractor::process_EXIF (unsigned char * ExifSection, unsigned int length)
{
    unsigned int FirstOffset;

    FocalplaneXRes = 0;
    FocalplaneUnits = 0;
    ExifImageWidth = 0;
    NumOrientations = 0;

    if (ShowTags){
        printf("Exif header %d bytes long\n",length);
    }

    {   // Check the EXIF header component
        static uchar ExifHeader[] = "Exif\0\0";
        if (memcmp(ExifSection+2, ExifHeader,6)){
            ErrNonfatal("Incorrect Exif header",0,0);
            return;
        }
    }

    if (memcmp(ExifSection+8,"II",2) == 0){
        if (ShowTags) printf("Exif section in Intel order\n");
        MotorolaOrder = 0;
    }else{
        if (memcmp(ExifSection+8,"MM",2) == 0){
            if (ShowTags) printf("Exif section in Motorola order\n");
            MotorolaOrder = 1;
        }else{
            ErrNonfatal("Invalid Exif alignment marker.",0,0);
            return;
        }
    }
#ifdef EXIF_DEBUG
	printf("%s process_EXIF 1\n",EXIF_DEBUG);
#endif
    // Check the next value for correctness.
    if (Get16u(ExifSection+10) != 0x2a){
        ErrNonfatal("Invalid Exif start (1)",0,0);
        return;
    }
#ifdef EXIF_DEBUG
	printf("%s process_EXIF 2\n",EXIF_DEBUG);
#endif
    FirstOffset = Get32u(ExifSection+12);
    if (FirstOffset < 8 || FirstOffset > 16){
        if (FirstOffset < 16 || FirstOffset > length-16){
            ErrNonfatal("invalid offset for first Exif IFD value",0,0);
            return;
        }
        // Usually set to 8, but other values valid too.
        ErrNonfatal("Suspicious offset of first Exif IFD value",0,0);
    }
#ifdef EXIF_DEBUG
	printf("%s process_EXIF 3\n",EXIF_DEBUG);
#endif
    DirWithThumbnailPtrs = NULL;

#ifdef EXIF_DEBUG
	printf("%s process_EXIF 4\n",EXIF_DEBUG);
#endif
    // First directory starts 16 bytes in.  All offset are relative to 8 bytes in.
    ProcessExifDir(ExifSection+8+FirstOffset, ExifSection+8, length-8, 0);
#ifdef EXIF_DEBUG
	printf("%s process_EXIF 5\n",EXIF_DEBUG);
#endif
    ThumbnailAtEnd = ThumbnailOffset >= LargestExifOffset ? TRUE : FALSE;

    if (DumpExifMap){
        unsigned a,b;
        printf("Map: %05d- End of exif\n",length-8);
        for (a=0;a<length-8;a+= 10){
            printf("Map: %05d ",a);
            for (b=0;b<10;b++) printf(" %02x",*(ExifSection+8+a+b));
            printf("\n");
        }
    }
#ifdef EXIF_DEBUG
	printf("%s process_EXIF 6\n",EXIF_DEBUG);
#endif

    // Compute the CCD width, in millimeters.
    if (FocalplaneXRes != 0){
        // Note: With some cameras, its not possible to compute this correctly because
        // they don't adjust the indicated focal plane resolution units when using less
        // than maximum resolution, so the CCDWidth value comes out too small.  Nothing
        // that Jhad can do about it - its a camera problem.
        CCDWidth = (float)(ExifImageWidth * FocalplaneUnits / FocalplaneXRes);

        if (FocalLength && FocalLength35mmEquiv == 0){
            // Compute 35 mm equivalent focal length based on sensor geometry if we haven't
            // already got it explicitly from a tag.
            FocalLength35mmEquiv = (int)(FocalLength/CCDWidth*36 + 0.5);
        }
    }
}

//--------------------------------------------------------------------------
// Process one of the nested EXIF directories.
//--------------------------------------------------------------------------
void SfmExifExtractor::ProcessExifDir(unsigned char * DirStart, unsigned char * OffsetBase,
        unsigned ExifLength, int NestingLevel)
{
    int de;
    int a;
    int NumDirEntries;
    unsigned ThumbnailOffset = 0;
    unsigned ThumbnailSize = 0;
    char IndentString[25];

    if (NestingLevel > 4){
        ErrNonfatal("Maximum Exif directory nesting exceeded (corrupt Exif header)", 0,0);
        return;
    }

    memset(IndentString, ' ', 25);
    IndentString[NestingLevel * 4] = '\0';

#ifdef EXIF_DEBUG
	printf("%s ProcessExifDir 1\n",EXIF_DEBUG);
#endif
    NumDirEntries = Get16u(DirStart);
    #define DIR_ENTRY_ADDR(Start, Entry) (Start+2+12*(Entry))

    {
        unsigned char * DirEnd;
        DirEnd = DIR_ENTRY_ADDR(DirStart, NumDirEntries);
        if (DirEnd+4 > (OffsetBase+ExifLength)){
            if (DirEnd+2 == OffsetBase+ExifLength || DirEnd == OffsetBase+ExifLength){
                // Version 1.3 of jhead would truncate a bit too much.
                // This also caught later on as well.
            }else{
                ErrNonfatal("Illegally sized Exif subdirectory (%d entries)",NumDirEntries,0);
                return;
            }
        }
        if (DumpExifMap){
            printf("Map: %05d-%05d: Directory\n",(int)(DirStart-OffsetBase), (int)(DirEnd+4-OffsetBase));
        }


    }
#ifdef EXIF_DEBUG
	printf("%s ProcessExifDir 2: %d\n",EXIF_DEBUG,NumDirEntries);
#endif
    if (ShowTags){
        printf("(dir has %d entries)\n",NumDirEntries);
    }

    for (de=0;de<NumDirEntries;de++){
        int Tag, Format, Components;

        unsigned char * ValuePtr;
        int ByteCount;
        unsigned char * DirEntry;
        DirEntry = DIR_ENTRY_ADDR(DirStart, de);

        Tag = Get16u(DirEntry);
        Format = Get16u(DirEntry+2);
        Components = Get32u(DirEntry+4);
#ifdef EXIF_DEBUG
	printf("%s ProcessExifDir 3: %X\n",EXIF_DEBUG,Tag);
#endif
        if ((Format-1) >= NUM_FORMATS) {
            // (-1) catches illegal zero case as unsigned underflows to positive large.
            ErrNonfatal("Illegal number format %d for tag %04x in Exif", Format, Tag);
            continue;
        }

        if ((unsigned)Components > 0x10000){
            ErrNonfatal("Too many components %d for tag %04x in Exif", Components, Tag);
            continue;
        }

        ByteCount = Components * BytesPerFormat[Format];

        if (ByteCount > 4){
            unsigned OffsetVal;
            OffsetVal = Get32u(DirEntry+8);
            // If its bigger than 4 bytes, the dir entry contains an offset.
            if (OffsetVal+ByteCount > ExifLength){
                // Bogus pointer offset and / or bytecount value
                ErrNonfatal("Illegal value pointer for tag %04x in Exif", Tag,0);
                continue;
            }
            ValuePtr = OffsetBase+OffsetVal;

            if (OffsetVal > LargestExifOffset){
                LargestExifOffset = OffsetVal;
            }

            if (DumpExifMap){
                printf("Map: %05d-%05d:   Data for tag %04x\n",OffsetVal, OffsetVal+ByteCount, Tag);
            }
        }else{
            // 4 bytes or less and value is in the dir entry itself
            ValuePtr = DirEntry+8;
        }

        if (Tag == TAG_MAKER_NOTE){
            if (ShowTags){
                printf("%s    Maker note: ",IndentString);
            }
            ProcessMakerNote(ValuePtr, ByteCount, OffsetBase, ExifLength);
            continue;
        }

        if (ShowTags){
            // Show tag name
            for (a=0;;a++){
                if (a >= TAG_TABLE_SIZE){
                    printf(IndentString);
                    printf("    Unknown Tag %04x Value = ", Tag);
                    break;
                }
                if (TagTable[a].Tag == Tag){
                    printf(IndentString);
                    printf("    %s = ",TagTable[a].Desc.data());
                    break;
                }
            }

            // Show tag value.
			if(Format == FMT_BYTE){
				if(ByteCount>1){
					printf("%.*ls\n", ByteCount/2, (wchar_t *)ValuePtr);
				}else{
					PrintFormatNumber(ValuePtr, Format, ByteCount);
					printf("\n");
				}
			}

			else if((Format == FMT_UNDEFINED) || (Format == FMT_STRING)){
				// Undefined is typically an ascii string.
				// String arrays printed without function call (different from int arrays)
				int NoPrint = 0;
				printf("\"");
				for (a=0;a<ByteCount;a++){
					if (ValuePtr[a] >= 32){
						putchar(ValuePtr[a]);
						NoPrint = 0;
					}else{
						// Avoiding indicating too many unprintable characters of proprietary
						// bits of binary information this program may not know how to parse.
						if (!NoPrint && a != ByteCount-1){
							putchar('?');
							NoPrint = 1;
						}
					}
				}
				printf("\"\n");
			}

			else{
				// Handle arrays of numbers later (will there ever be?)
				PrintFormatNumber(ValuePtr, Format, ByteCount);
				printf("\n");
			}

        }

        // Extract useful components of tag
		if(Tag == TAG_MAKE){
			strncpy(CameraMake, (char *)ValuePtr, ByteCount < 31 ? ByteCount : 31);
		}

		else if(Tag == TAG_MODEL){
			strncpy(CameraModel, (char *)ValuePtr, ByteCount < 39 ? ByteCount : 39);
		}

		else if((Tag == TAG_DATETIME_ORIGINAL) || (Tag == TAG_DATETIME_DIGITIZED) || (Tag == TAG_DATETIME)){
#ifdef EXIF_DEBUG
	printf("%s ProcessExifDir 4: DateTime 1\n",EXIF_DEBUG);
#endif
			if(Tag == TAG_DATETIME_ORIGINAL){
				// If we get a DATETIME_ORIGINAL, we use that one.
				strncpy(DateTime, (char *)ValuePtr, 19);
				// Fallthru...
			}
#ifdef EXIF_DEBUG
	printf("%s ProcessExifDir 4: DateTime 2\n",EXIF_DEBUG);
#endif
			if (!isdigit(DateTime[0])){
				// If we don't already have a DATETIME_ORIGINAL, use whatever
				// time fields we may have.
				strncpy(DateTime, (char *)ValuePtr, 19);
			}
#ifdef EXIF_DEBUG
	printf("%s ProcessExifDir 4: DateTime 3\n",EXIF_DEBUG);
#endif
			if (numDateTimeTags >= MAX_DATE_COPIES){
				ErrNonfatal("More than %d date fields in Exif.  This is nuts", MAX_DATE_COPIES, 0);
				break;
			}
#ifdef EXIF_DEBUG
	printf("%s ProcessExifDir 4: DateTime 4: %d < %d\n",EXIF_DEBUG,numDateTimeTags,MAX_DATE_COPIES);
#endif
			DateTimeOffsets[numDateTimeTags++] =
				(char *)ValuePtr - (char *)OffsetBase;
#ifdef EXIF_DEBUG
	printf("%s ProcessExifDir 4: DateTime 5\n",EXIF_DEBUG);
#endif

		}

		else if(Tag == TAG_WINXP_COMMENT){
			if (Comments[0]){ // We already have a jpeg comment.
				// Already have a comment (probably windows comment), skip this one.
				if (ShowTags) printf("Windows XP commend and other comment in header\n");
				break; // Already have a windows comment, skip this one.
			}

			if (ByteCount > 1){
				if (ByteCount > MAX_COMMENT_SIZE) ByteCount = MAX_COMMENT_SIZE;
				memcpy(Comments, ValuePtr, ByteCount);
				CommentWidthchars = ByteCount/2;
			}
		}

		else if(Tag == TAG_USERCOMMENT){
			if (Comments[0]){ // We already have a jpeg comment.
				// Already have a comment (probably windows comment), skip this one.
				if (ShowTags) printf("Multiple comments in exif header\n");
				break; // Already have a windows comment, skip this one.
			}

			// Comment is often padded with trailing spaces.  Remove these first.
			for (a=ByteCount;;){
				a--;
				if ((ValuePtr)[a] == ' '){
					(ValuePtr)[a] = '\0';
				}else{
					break;
				}
				if (a == 0) break;
			}

			// Copy the comment
			if (memcmp(ValuePtr, "ASCII",5) == 0){
				for (a=5;a<10;a++){
					int c;
					c = (ValuePtr)[a];
					if (c != '\0' && c != ' '){
						strncpy(Comments, (char *)ValuePtr+a, 199);
						break;
					}
				}
			}else{
				strncpy(Comments, (char *)ValuePtr, MAX_COMMENT_SIZE-1);
			}
		}

		else if(Tag == TAG_FNUMBER){
			// Simplest way of expressing aperture, so I trust it the most.
			// (overwrite previously computd value if there is one)
			ApertureFNumber = (float)ConvertAnyFormat(ValuePtr, Format);
		}

		else if((Tag == TAG_APERTURE) || (Tag == TAG_MAXAPERTURE)){
			// More relevant info always comes earlier, so only use this field if we don't
			// have appropriate aperture information yet.
			if (ApertureFNumber == 0){
				ApertureFNumber
					= (float)exp(ConvertAnyFormat(ValuePtr, Format)*log(2)*0.5);
			}
		}

		else if(Tag == TAG_FOCALLENGTH){
			// Nice digital cameras actually save the focal length as a function
			// of how farthey are zoomed in.
			FocalLength = (float)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_SUBJECT_DISTANCE){
			// Inidcates the distacne the autofocus camera is focused to.
			// Tends to be less accurate as distance increases.
			Distance = (float)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_EXPOSURETIME){
			// Simplest way of expressing exposure time, so I trust it most.
			// (overwrite previously computd value if there is one)
			ExposureTime = (float)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_SHUTTERSPEED){
			// More complicated way of expressing exposure time, so only use
			// this value if we don't already have it from somewhere else.
			if (ExposureTime == 0){
				ExposureTime
					= (float)(1/exp(ConvertAnyFormat(ValuePtr, Format)*log(2)));
			}
		}


		else if(Tag == TAG_FLASH){
			FlashUsed=(int)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_ORIENTATION){
			if (NumOrientations >= 2){
				// Can have another orientation tag for the thumbnail, but if there's
				// a third one, things are stringae.
				ErrNonfatal("More than two orientation in Exif",0,0);
				break;
			}
			OrientationPtr[NumOrientations] = ValuePtr;
			OrientationNumFormat[NumOrientations] = Format;
			if (NumOrientations == 0){
				Orientation = (int)ConvertAnyFormat(ValuePtr, Format);
			}
			if (Orientation < 0 || Orientation > 8){
				ErrNonfatal("Undefined rotation value %d in Exif", Orientation, 0);
				Orientation = 0;
			}
			NumOrientations += 1;
		}

		else if((Tag == TAG_PIXEL_Y_DIMENSION) || (Tag == TAG_PIXEL_X_DIMENSION)){
			// Use largest of height and width to deal with images that have been
			// rotated to portrait format.
			a = (int)ConvertAnyFormat(ValuePtr, Format);
			if (ExifImageWidth < a) ExifImageWidth = a;
		}

		else if(Tag == TAG_FOCAL_PLANE_XRES){
			FocalplaneXRes = ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_FOCAL_PLANE_UNITS){
			switch((int)ConvertAnyFormat(ValuePtr, Format)){
				case 1: FocalplaneUnits = 25.4; break; // inch
				case 2:
					// According to the information I was using, 2 means meters.
					// But looking at the Cannon powershot's files, inches is the only
					// sensible value.
					FocalplaneUnits = 25.4;
					break;

				case 3: FocalplaneUnits = 10;   break;  // centimeter
				case 4: FocalplaneUnits = 1;    break;  // millimeter
				case 5: FocalplaneUnits = .001; break;  // micrometer
			}
		}

		else if(Tag == TAG_EXPOSURE_BIAS){
			ExposureBias = (float)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_WHITEBALANCE){
			Whitebalance = (int)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_LIGHT_SOURCE){
			LightSource = (int)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_METERING_MODE){
			MeteringMode = (int)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_EXPOSURE_PROGRAM){
			ExposureProgram = (int)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_EXPOSURE_INDEX){
			if (ISOequivalent == 0){
				// Exposure index and ISO equivalent are often used interchangeably,
				// so we will do the same in jhead.
				// http://photography.about.com/library/glossary/bldef_ei.htm
				ISOequivalent = (int)ConvertAnyFormat(ValuePtr, Format);
			}
		}

		else if(Tag == TAG_EXPOSURE_MODE){
			ExposureMode = (int)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_ISO_EQUIVALENT){
			ISOequivalent = (int)ConvertAnyFormat(ValuePtr, Format);
			if ( ISOequivalent < 50 ){
				// Fixes strange encoding on some older digicams.
				ISOequivalent *= 200;
			}
		}

		else if(Tag == TAG_DIGITALZOOMRATIO){
			DigitalZoomRatio = (float)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_THUMBNAIL_OFFSET){
			ThumbnailOffset = (unsigned)ConvertAnyFormat(ValuePtr, Format);
			DirWithThumbnailPtrs = DirStart;
		}

		else if(Tag == TAG_THUMBNAIL_LENGTH){
			ThumbnailSize = (unsigned)ConvertAnyFormat(ValuePtr, Format);
			ThumbnailSizeOffset = ValuePtr-OffsetBase;
		}

		else if((Tag == TAG_EXIF_OFFSET) || (Tag == TAG_INTEROP_OFFSET)){
			if((Tag == TAG_EXIF_OFFSET) && ShowTags) printf("%s    Exif Dir:",IndentString);
			if (Tag == TAG_INTEROP_OFFSET && ShowTags) printf("%s    Interop Dir:",IndentString);
			{
				unsigned char * SubdirStart;
				SubdirStart = OffsetBase + Get32u(ValuePtr);
				if (SubdirStart < OffsetBase || SubdirStart > OffsetBase+ExifLength){
					ErrNonfatal("Illegal Exif or interop ofset directory link",0,0);
				}else{
					ProcessExifDir(SubdirStart, OffsetBase, ExifLength, NestingLevel+1);
				}
				continue;
			}
		}

		else if(Tag == TAG_GPSINFO){
			if (ShowTags) printf("%s    GPS info dir:",IndentString);
			{
				unsigned char * SubdirStart;
				SubdirStart = OffsetBase + Get32u(ValuePtr);
				if (SubdirStart < OffsetBase || SubdirStart > OffsetBase+ExifLength){
					ErrNonfatal("Illegal GPS directory link in Exif",0,0);
				}else{
					ProcessGpsInfo(SubdirStart, ByteCount, OffsetBase, ExifLength);
				}
				continue;
			}
		}

		else if(Tag == TAG_FOCALLENGTH_35MM){
			// The focal length equivalent 35 mm is a 2.2 tag (defined as of April 2002)
			// if its present, use it to compute equivalent focal length instead of
			// computing it from sensor geometry and actual focal length.
			FocalLength35mmEquiv = (unsigned)ConvertAnyFormat(ValuePtr, Format);
		}

		else if(Tag == TAG_DISTANCE_RANGE){
			// Three possible standard values:
			//   1 = macro, 2 = close, 3 = distant
			DistanceRange = (int)ConvertAnyFormat(ValuePtr, Format);
		}



		else if(Tag == TAG_X_RESOLUTION){
			if (NestingLevel==0) {// Only use the values from the top level directory
				xResolution = (float)ConvertAnyFormat(ValuePtr,Format);
				// if yResolution has not been set, use the value of xResolution
				if (yResolution == 0.0) yResolution = xResolution;
			}
		}

		else if(Tag == TAG_Y_RESOLUTION){
			if (NestingLevel==0) {// Only use the values from the top level directory
				yResolution = (float)ConvertAnyFormat(ValuePtr,Format);
				// if xResolution has not been set, use the value of yResolution
				if (xResolution == 0.0) xResolution = yResolution;
			}
		}

		else if(Tag == TAG_RESOLUTION_UNIT){
			if (NestingLevel==0) {// Only use the values from the top level directory
				ResolutionUnit = (int) ConvertAnyFormat(ValuePtr,Format);
			}
		}

    }


    {
        // In addition to linking to subdirectories via exif tags,
        // there's also a potential link to another directory at the end of each
        // directory.  this has got to be the result of a committee!
        unsigned char * SubdirStart;
        unsigned Offset;

        if (DIR_ENTRY_ADDR(DirStart, NumDirEntries) + 4 <= OffsetBase+ExifLength){
            Offset = Get32u(DirStart+2+12*NumDirEntries);
            if (Offset){
                SubdirStart = OffsetBase + Offset;
                if (SubdirStart > OffsetBase+ExifLength || SubdirStart < OffsetBase){
                    if (SubdirStart > OffsetBase && SubdirStart < OffsetBase+ExifLength+20){
                        // Jhead 1.3 or earlier would crop the whole directory!
                        // As Jhead produces this form of format incorrectness,
                        // I'll just let it pass silently
                        if (ShowTags) printf("Thumbnail removed with Jhead 1.3 or earlier\n");
                    }else{
                        ErrNonfatal("Illegal subdirectory link in Exif header",0,0);
                    }
                }else{
                    if (SubdirStart <= OffsetBase+ExifLength){
                        if (ShowTags) printf("%s    Continued directory ",IndentString);
                        ProcessExifDir(SubdirStart, OffsetBase, ExifLength, NestingLevel+1);
                    }
                }
                if (Offset > LargestExifOffset){
                    LargestExifOffset = Offset;
                }
            }
        }else{
            // The exif header ends before the last next directory pointer.
        }
    }

    if (ThumbnailOffset){
        ThumbnailAtEnd = FALSE;

        if (DumpExifMap){
            printf("Map: %05d-%05d: Thumbnail\n",ThumbnailOffset, ThumbnailOffset+ThumbnailSize);
        }

        if (ThumbnailOffset <= ExifLength){
            if (ThumbnailSize > ExifLength-ThumbnailOffset){
                // If thumbnail extends past exif header, only save the part that
                // actually exists.  Canon's EOS viewer utility will do this - the
                // thumbnail extracts ok with this hack.
                ThumbnailSize = ExifLength-ThumbnailOffset;
                if (ShowTags) printf("Thumbnail incorrectly placed in header\n");

            }
            // The thumbnail pointer appears to be valid.  Store it.
            ThumbnailOffset = ThumbnailOffset;
            ThumbnailSize = ThumbnailSize;

            if (ShowTags){
                printf("Thumbnail size: %d bytes\n",ThumbnailSize);
            }
        }
    }
}

//--------------------------------------------------------------------------
// Process GPS info directory
//--------------------------------------------------------------------------
void SfmExifExtractor::ProcessGpsInfo(unsigned char * DirStart, int ByteCountUnused, unsigned char * OffsetBase, unsigned ExifLength)
{
    int de;
    unsigned a;
    int NumDirEntries;

    NumDirEntries = Get16u(DirStart);
    #define DIR_ENTRY_ADDR(Start, Entry) (Start+2+12*(Entry))

    if (ShowTags){
        printf("(dir has %d entries)\n",NumDirEntries);
    }

    GpsInfoPresent = TRUE;
    strcpy(GpsLat, "? ?");
    strcpy(GpsLong, "? ?");
    GpsAlt[0] = 0;

    for (de=0;de<NumDirEntries;de++){
        unsigned Tag, Format, Components;
        unsigned char * ValuePtr;
        int ComponentSize;
        unsigned ByteCount;
        unsigned char * DirEntry;
        DirEntry = DIR_ENTRY_ADDR(DirStart, de);

        if (DirEntry+12 > OffsetBase+ExifLength){
            ErrNonfatal("GPS info directory goes past end of exif",0,0);
            return;
        }

        Tag = Get16u(DirEntry);
        Format = Get16u(DirEntry+2);
        Components = Get32u(DirEntry+4);

        if ((Format-1) >= NUM_FORMATS) {
            // (-1) catches illegal zero case as unsigned underflows to positive large.
            ErrNonfatal("Illegal number format %d for Exif gps tag %04x", Format, Tag);
            continue;
        }

        ComponentSize = BytesPerFormat[Format];
        ByteCount = Components * ComponentSize;

        if (ByteCount > 4){
            unsigned OffsetVal;
            OffsetVal = Get32u(DirEntry+8);
            // If its bigger than 4 bytes, the dir entry contains an offset.
            if (OffsetVal+ByteCount > ExifLength){
                // Bogus pointer offset and / or bytecount value
                ErrNonfatal("Illegal value pointer for Exif gps tag %04x", Tag,0);
                continue;
            }
            ValuePtr = OffsetBase+OffsetVal;
        }else{
            // 4 bytes or less and value is in the dir entry itself
            ValuePtr = DirEntry+8;
        }

		char FmtString[21];
		char TempString[50];
		double Values[3];

		if(Tag == TAG_GPS_LAT_REF){
			GpsLat[0] = ValuePtr[0];
		}

		else if(Tag == TAG_GPS_LONG_REF){
			GpsLong[0] = ValuePtr[0];
		}

		else if((Tag == TAG_GPS_LAT) || (Tag == TAG_GPS_LONG)){
			if (Format != FMT_URATIONAL){
				ErrNonfatal("Inappropriate format (%d) for Exif GPS coordinates!", Format, 0);
			}
			strcpy(FmtString, "%0.0fd %0.0fm %0.0fs");
			for (a=0;a<3;a++){
				int den, digits;

				den = Get32s(ValuePtr+4+a*ComponentSize);
				digits = 0;
				while (den > 1 && digits <= 6){
					den = den / 10;
					digits += 1;
				}
				if (digits > 6) digits = 6;
				FmtString[1+a*7] = (char)('2'+digits+(digits ? 1 : 0));
				FmtString[3+a*7] = (char)('0'+digits);

				Values[a] = ConvertAnyFormat(ValuePtr+a*ComponentSize, Format);
			}

			sprintf(TempString, FmtString, Values[0], Values[1], Values[2]);

			if (Tag == TAG_GPS_LAT){
				strncpy(GpsLat+2, TempString, 29);
			}else{
				strncpy(GpsLong+2, TempString, 29);
			}
		}

		else if(Tag == TAG_GPS_ALT_REF){
			GpsAlt[0] = (char)(ValuePtr[0] ? '-' : ' ');
		}

		else if(Tag == TAG_GPS_ALT){
			sprintf(GpsAlt + 1, "%.2fm",
				ConvertAnyFormat(ValuePtr, Format));
		}

        if (ShowTags){
            // Show tag value.
            if (Tag < MAX_GPS_TAG){
                printf("        GPS%s =", GpsTags[Tag].data());
            }else{
                // Show unknown tag
                printf("        Illegal GPS tag %04x=", Tag);
            }

            if((Format == FMT_UNDEFINED) || (Format == FMT_UNDEFINED)){
				// Undefined is typically an ascii string.
				// String arrays printed without function call (different from int arrays)
				printf("\"");
				for (a=0;a<ByteCount;a++){
					int ZeroSkipped = 0;
					if (ValuePtr[a] >= 32){
						if (ZeroSkipped){
							printf("?");
							ZeroSkipped = 0;
						}
						putchar(ValuePtr[a]);
					}else{
						if (ValuePtr[a] == 0){
							ZeroSkipped = 1;
						}
					}
				}
				printf("\"\n");
			}
            else{
				// Handle arrays of numbers later (will there ever be?)
				for (a=0;;){
					PrintFormatNumber(ValuePtr+a*ComponentSize, Format, ByteCount);
					if (++a >= Components) break;
					printf(", ");
				}
				printf("\n");
            }
        }
    }
}

//--------------------------------------------------------------------------
// Display a number as one of its many formats
//--------------------------------------------------------------------------
void SfmExifExtractor::PrintFormatNumber(void * ValuePtr, int Format, int ByteCount)
{
    int s,n;

    for(n=0;n<16;n++){
        if((Format == FMT_SBYTE) || (Format == FMT_BYTE)) {
        	printf("%02x",*(uchar *)ValuePtr);
        	s=1;
        }
        else if(Format == FMT_USHORT) {
        	printf("%d",Get16u(ValuePtr));
        	s=2;
        }
        else if((Format == FMT_ULONG) || (Format == FMT_SLONG)){
        	printf("%d",Get32s(ValuePtr));
        	s=4;
        }
        else if(Format == FMT_SSHORT) {
        	printf("%hd",(signed short)Get16u(ValuePtr));
        	s=2;
        }
        else if((Format == FMT_URATIONAL) || (Format == FMT_SRATIONAL)){
        	printf("%d/%d",Get32s(ValuePtr), Get32s(4+(char *)ValuePtr));
        	s = 8;
        }
        else if(Format == FMT_SINGLE) {
        	printf("%f",(double)*(float *)ValuePtr);
        	s=8;
        }
        else if(Format == FMT_DOUBLE) {
        	printf("%f",*(double *)ValuePtr);
        	s=8;
        }
        else{
			printf("Unknown format %d:", Format);
			return;
        }
        ByteCount -= s;
        if (ByteCount <= 0) break;
        printf(", ");
        ValuePtr = (void *)((char *)ValuePtr + s);

    }
    if (n >= 16) printf("...");
}

//--------------------------------------------------------------------------
// Process maker note - to the limited extent that its supported.
//--------------------------------------------------------------------------
void SfmExifExtractor::ProcessMakerNote(unsigned char * ValuePtr, int ByteCount,
        unsigned char * OffsetBase, unsigned ExifLength)
{
    if (strstr(CameraMake, "Canon")){
        ProcessCanonMakerNoteDir(ValuePtr, OffsetBase, ExifLength);
    }else{
        if (ShowTags){
            ShowMakerNoteGeneric(ValuePtr, ByteCount);
        }
    }
}

//--------------------------------------------------------------------------
// Process exif format directory, as used by Cannon maker note
//--------------------------------------------------------------------------
void SfmExifExtractor::ProcessCanonMakerNoteDir(unsigned char * DirStart, unsigned char * OffsetBase,
        unsigned ExifLength)
{
    int de;
    int a;
    int NumDirEntries;

    NumDirEntries = Get16u(DirStart);
    #define DIR_ENTRY_ADDR(Start, Entry) (Start+2+12*(Entry))

    {
        unsigned char * DirEnd;
        DirEnd = DIR_ENTRY_ADDR(DirStart, NumDirEntries);
        if (DirEnd > (OffsetBase+ExifLength)){
            ErrNonfatal("Illegally sized Exif makernote subdir (%d entries)",NumDirEntries,0);
            return;
        }

        if (DumpExifMap){
            printf("Map: %05d-%05d: Directory (makernote)\n",(int)(DirStart-OffsetBase), (int)(DirEnd-OffsetBase));
        }
    }

    if (ShowTags){
        printf("(dir has %d entries)\n",NumDirEntries);
    }

    for (de=0;de<NumDirEntries;de++){
        int Tag, Format, Components;
        unsigned char * ValuePtr;
        int ByteCount;
        unsigned char * DirEntry;
        DirEntry = DIR_ENTRY_ADDR(DirStart, de);

        Tag = Get16u(DirEntry);
        Format = Get16u(DirEntry+2);
        Components = Get32u(DirEntry+4);

        if ((Format-1) >= NUM_FORMATS) {
            // (-1) catches illegal zero case as unsigned underflows to positive large.
            ErrNonfatal("Illegal Exif number format %d for maker tag %04x", Format, Tag);
            continue;
        }

        if ((unsigned)Components > 0x10000){
            ErrNonfatal("Too many components (%d) for Exif maker tag %04x", Components, Tag);
            continue;
        }

        ByteCount = Components * BytesPerFormat[Format];

        if (ByteCount > 4){
            unsigned OffsetVal;
            OffsetVal = Get32u(DirEntry+8);
            // If its bigger than 4 bytes, the dir entry contains an offset.
            if (OffsetVal+ByteCount > ExifLength){
                // Bogus pointer offset and / or bytecount value
                ErrNonfatal("Illegal value pointer for Exif maker tag %04x", Tag,0);
                continue;
            }
            ValuePtr = OffsetBase+OffsetVal;

            if (DumpExifMap){
                printf("Map: %05d-%05d:   Data for makernote tag %04x\n",OffsetVal, OffsetVal+ByteCount, Tag);
            }
        }else{
            // 4 bytes or less and value is in the dir entry itself
            ValuePtr = DirEntry+8;
        }

        if (ShowTags){
            // Show tag name
            printf("            Canon maker tag %04x Value = ", Tag);
        }

        // Show tag value.
        if((Format == FMT_UNDEFINED) || (Format == FMT_STRING)) {
			// Undefined is typically an ascii string.
            // String arrays printed without function call (different from int arrays)
			if (ShowTags){
				printf("\"");
				for (a=0;a<ByteCount;a++){
					int ZeroSkipped = 0;
					if (ValuePtr[a] >= 32){
						if (ZeroSkipped){
							printf("?");
							ZeroSkipped = 0;
						}
						putchar(ValuePtr[a]);
					}else{
						if (ValuePtr[a] == 0){
							ZeroSkipped = 1;
						}
					}
				}
				printf("\"\n");
			}
        }
        else{
			if (ShowTags){
				PrintFormatNumber(ValuePtr, Format, ByteCount);
				printf("\n");
			}
        }
        if (Tag == 1 && Components > 16){
            int IsoCode = Get16u(ValuePtr + 16*sizeof(unsigned short));
            if (IsoCode >= 16 && IsoCode <= 24){
                ISOequivalent = 50 << (IsoCode-16);
            }
        }

        if (Tag == 4 && Format == FMT_USHORT){
            if (Components > 7){
                int WhiteBalance = Get16u(ValuePtr + 7*sizeof(unsigned short));
                switch(WhiteBalance){
                    // 0=Auto, 6=Custom
                    case 1: LightSource = 1; break; // Sunny
                    case 2: LightSource = 1; break; // Cloudy
                    case 3: LightSource = 3; break; // Thungsten
                    case 4: LightSource = 2; break; // Fourescent
                    case 5: LightSource = 4; break; // Flash
                }
            }
            if (Components > 19 && Distance <= 0) {
                // Indicates the distance the autofocus camera is focused to.
                // Tends to be less accurate as distance increases.
                int temp_dist = Get16u(ValuePtr + 19*sizeof(unsigned short));
                if (temp_dist != 65535){
                    Distance = (float)temp_dist/100;
                }else{
                    Distance = -1 /* infinity */;
                }
            }
        }
    }
}

//--------------------------------------------------------------------------
// Show generic maker note - just hex bytes.
//--------------------------------------------------------------------------
void SfmExifExtractor::ShowMakerNoteGeneric(unsigned char * ValuePtr, int ByteCount)
{
    int a;
    for (a=0;a<ByteCount;a++){
        if (a > 10){
            printf("...");
            break;
        }
        printf(" %02x",ValuePtr[a]);
    }
    printf(" (%d bytes)", ByteCount);
    printf("\n");

}

//--------------------------------------------------------------------------
// Report non fatal errors.  Now that microsoft.net modifies exif headers,
// there's corrupted ones, and there could be more in the future.
//--------------------------------------------------------------------------
void SfmExifExtractor::ErrNonfatal(string msg, int a1, int a2)
{
    if (SupressNonFatalErrors) return;

    fprintf(stderr,"\nNonfatal Error : ");
    if (CurrentFile) fprintf(stderr,"'%s' ",CurrentFile);
    fprintf(stderr, msg.data(), a1, a2);
    fprintf(stderr, "\n");
}

//--------------------------------------------------------------------------
// Evaluate number, be it int, rational, or float from directory.
//--------------------------------------------------------------------------
double SfmExifExtractor::ConvertAnyFormat(void * ValuePtr, int Format)
{
    double Value;
    Value = 0;

	if(Format == FMT_SBYTE)     return *(signed char *)ValuePtr;
	if(Format == FMT_BYTE)      return *(uchar *)ValuePtr;
	if(Format == FMT_USHORT)    return Get16u(ValuePtr);
	if(Format == FMT_ULONG)     return Get32u(ValuePtr);
	if((Format == FMT_URATIONAL) || (Format == FMT_SRATIONAL)){
		int Num,Den;
		Num = Get32s(ValuePtr);
		Den = Get32s(4+(char *)ValuePtr);
		if (Den == 0){
			return 0;
		}else{
			return (double)Num/Den;
		}
	}

	if(Format == FMT_SSHORT)    return (signed short)Get16u(ValuePtr);
	if(Format == FMT_SLONG)     return Get32s(ValuePtr);

	// Not sure if this is correct (never seen float used in Exif format)
	if(Format == FMT_SINGLE)    return (double)*(float *)ValuePtr;
	if(Format == FMT_DOUBLE)    return *(double *)ValuePtr;

	else
		ErrNonfatal("Illegal format code %d in Exif header",Format,0);

	return Value;
}

