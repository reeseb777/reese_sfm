#include "sfmFeatureDetector.h"

SfmFeatureDetector::SfmFeatureDetector(){

}

SfmFeatureDetector::~SfmFeatureDetector(){

}

void SfmFeatureDetector::ImportImageData(SfmImage *im){
	// convert imgData to grayscale so that we can perform the feature detection
	if(strcmp(im->imgData->colorModel, "RGB")==0){ // image is RGB
		if(strcmp(im->imgData->channelSeq, "RGB")==0){ // color sequence is RGB
			img = cvCreateImage(cvSize( im->imgData->width, im->imgData->height), IPL_DEPTH_8U, 1);
			cvCvtColor(im->imgData, img, CV_RGB2GRAY);
		}
		else if(strcmp(im->imgData->channelSeq, "BGR")==0){ // color sequence is BGR
			img = cvCreateImage(cvSize( im->imgData->width, im->imgData->height), IPL_DEPTH_8U, 1);
			cvCvtColor(im->imgData, img, CV_BGR2GRAY);
		}
		else{ // color sequence is neither RGB nor BGR, so something must be wrong...
			printf("ERROR in sfmFeatureDetector::ImportImageData( ... )\n");
			printf("Color Model of Image ID %d is RGB (OK), Channel Sequence %s, which is not supported\n", im->imID, im->imgData->channelSeq);
			exit(1);
		}
	}
	else if(strcmp(im->imgData->colorModel, "GRAY")==0){ // image is already in grayscale
		img = cvCloneImage(im->imgData);
	}
	else{ // we can add other color models as needed, but for now just say there's an error
		printf("ERROR in sfmFeatureDetector::ImportImageData( ... )\n");
		printf("Color Model of Image ID %d is %s, which is not supported\n", im->imID, im->imgData->channelSeq);
		exit(1);
	}
	imID = im->imID;
}

void SfmFeatureDetector::Detect(SfmImage *im, SfmImageFeatures *feats){
	ImportImageData(im);
	Detect(feats);
}

void SfmFeatureDetector::Detect(SfmImageFeatures *feats){
	feats->params = cvSURFParams(500,1);
	feats->storage = cvCreateMemStorage(0);
	cvExtractSURF(img, 0, &(feats->features), &(feats->descriptors), feats->storage, feats->params);
}
