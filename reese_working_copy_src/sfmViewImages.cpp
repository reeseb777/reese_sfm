#include "sfmViewImages.h"
//#include "cv.h"
#include "highgui.h"
#include "sfmimage.h"
#include <sstream>
#include <math.h>
#include <algorithm>

using namespace std;

//using namespace cv;

SfmViewImages::SfmViewImages(Params *sim){
	show = sim->show & 0x01;
	showTime = sim->showImageTime;
}
SfmViewImages::~SfmViewImages(){

}
void SfmViewImages::ImportImage(SfmImage *img){
	if(show > 0){
		imgData 	= img->imgData;
		imID 		= img->imID;
		imgName 	= img->imgName;
	}
}
void SfmViewImages::ViewImage(){
	if(show > 0){
		cvNamedWindow(imgName.data(), CV_WINDOW_AUTOSIZE);
		cvShowImage(imgName.data(),imgData);
		cvWaitKey(showTime);
		cvDestroyWindow(imgName.data());
	}
}


SfmFeatureViewer::SfmFeatureViewer(Params *sim){
	show = sim->show & 0x02;
	showTime = sim->showGrayTime;
	viewMaxFeatures = sim->viewMaxFeatures;
	viewFeatureBox = sim->viewFeatureBox;
}

SfmFeatureViewer::~SfmFeatureViewer(){

}
/* ImportData
 * Imports the data needed to view the grayscale image and detected features
 *
 * Inputs:
 * - SfmFeatureDetector *fdetect: feature detector object (contains image information
 * - SfmImageFeatures *feats: contains image feature information
 *
 * Outputs: none
 */
void SfmFeatureViewer::ImportData(SfmFeatureDetector *fdetect, SfmImageFeatures *feats){
	if(show > 0){
		// clone image so that we can draw boxes around features
		imgData		= cvCloneImage(fdetect->img);
		// get image ID
		imID		= fdetect->imID;
		// convert image ID to string so we can display in the title
		stringstream ss;
		ss << imID;
		imgName 	= ss.str();

		// get pointer to features
		features 	= feats->features;
	}
}
void SfmFeatureViewer::ImportData(IplImage*img, unsigned imageID, vector<KeyPoint>&keypts){
	if(show > 0){
		// clone image so that we can draw boxes around features
		imgData		= cvCloneImage(img);
		// get image ID
		imID		= imageID;
		// convert image ID to string so we can display in the title
		stringstream ss;
		ss << imageID;
		imgName 	= ss.str();

		// get pointer to features
		keypoints=keypts;
	}
}
void SfmFeatureViewer::ImportKeyPoints(vector<KeyPoint>&keypts){
	keypoints=keypts;
}
/* ViewFeatures
 *
 * Displays the grayscale image used for feature detection.
 * Displays oriented boxes around those features if desired.
 *
 * Inputs: none
 * Outputs: none
 */
void SfmFeatureViewer::ViewFeatures(){
	if(show > 0){
		// create window to show image in
		cvNamedWindow(imgName.data(), CV_WINDOW_AUTOSIZE);

		// create reader to go through the features
		CvSeqReader freader;
		cvStartReadSeq( features, &freader, 0);
		// set the number of features to display
		unsigned num_features_displayed;
		if(viewMaxFeatures < 0)
			//num_features_displayed = (unsigned)features->total;
			num_features_displayed = keypoints.size();
		else
			//num_features_displayed = (unsigned)((features->total > viewMaxFeatures) ? viewMaxFeatures:features->total);
			num_features_displayed = (unsigned)((keypoints.size() > unsigned(viewMaxFeatures)) ? viewMaxFeatures:keypoints.size());
		for(unsigned i=0; i<num_features_displayed; i++){
			// get the SURF point (begins with the one with the largest Hessian)
			//CvSURFPoint* p = (CvSURFPoint *)freader.ptr;
			// point to next one
			//CV_NEXT_SEQ_ELEM(freader.seq->elem_size, freader);

			KeyPoint p=keypoints[i];
			CvPoint2D32f pt=p.pt;
			switch(viewFeatureBox){
				case 0:
					// draw circle at pt
					//cvCircle(imgData, cvPointFrom32f(p->pt), 1, cvScalar(255,255,255));
					cvCircle(imgData, cvPointFrom32f(pt), 1, cvScalar(255,255,255));
					break;
				case 1:
					// draw oriented rectangle at pt
					CvPoint pts_t[4];
					for(unsigned j=0; j<4; j++){
						//pts_t[j].x = (int)( p->pt.x+float(p->size)*cos((p->dir+45.+float(j)*90.)*DEG2RAD));
						//pts_t[j].y = (int)( p->pt.y+float(p->size)*sin((p->dir+45.+float(j)*90.)*DEG2RAD));
						pts_t[j].x = (int)( pt.x+float(p.size)*cos((p.angle+45.+float(j)*90.)*DEG2RAD));
						pts_t[j].y = (int)( pt.y+float(p.size)*sin((p.angle+45.+float(j)*90.)*DEG2RAD));
					}
					CvPoint* pts[1] = {pts_t};
					int nPts[1] = {4};
					cvPolyLine(imgData,pts,nPts,1,1,cvScalar(255,255,255));
					break;
			}

			/**/

			/**
			// draw rectangle with lower left at p1
			p2 = p1 = cvPointFrom32f(p->pt);
			p2.x += p->size; p2.y += p->size;
			cvRectangle(imgData, p1, p2,cvScalar(255,255,255));
			/**/

			/**/

			/**/
		}

		cvShowImage(imgName.data(),imgData);


		cvWaitKey(showTime);
		cvDestroyWindow(imgName.data());
	}
}

/* SURFCompareHessian
 * Compares the Hessian from two SURF features
 *
 * Inputs:
 * - const void* a: pointer to a SURF feature
 * - const void* b: pointer to another SURF feature
 * - void* userdata: not used
 *
 * Output:
 * -1 if the hessian from a is larger than that of b.
 * 1 otherwise
 */
int SURFCompareHessian(const void* a, const void* b, void* userdata){
	CvSURFPoint* p1 = (CvSURFPoint *)a;
	CvSURFPoint* p2 = (CvSURFPoint *)b;
	if(p1->hessian > p2->hessian)
		return -1;
	else
		return 1;
}


SfmFeatureMatchViewer::SfmFeatureMatchViewer(Params *sim){
//	show = sim->show & 0x04; //overriden by viewMatches
	showTime = sim->showMatchTime;
	viewMaxMatches = (sim->viewMaxMatches==-1) ? (1<<30):sim->viewMaxMatches;
	viewMatchBox = sim->viewMatchBox;
	show = sim->viewMatches;

	img_id_count = 0;
	feature_id_count = 0;
	keypoint_id_count = 0;

	node_r = 10; // radius of the image nodes in the connectivity graph
	selectInd=0;
}

SfmFeatureMatchViewer::~SfmFeatureMatchViewer(){

}

void SfmFeatureMatchViewer::ImportImage(SfmImage *im){
	if(show == 0){
		return;
	}

	if(im->imID >= img_ind.size()){
		img_ind.resize(im->imID+1);
	}
	img_ind[im->imID] = img_id_count++;
	imgNames.push_back(im->imgName);
	img.push_back(cvCloneImage(im->imgData));
}
void SfmFeatureMatchViewer::ImportFeatures(SfmImageFeatures *feats){
	if(show == 0){
		return;
	}
	if(feats->imID >= feature_ind.size()){
		feature_ind.resize(feats->imID+1);
	}
	feature_ind[feats->imID] = feature_id_count++;
	feature_storage.push_back(cvCreateMemStorage());
	features.push_back(cvCloneSeq(feats->features, feature_storage.back()));
}

void SfmFeatureMatchViewer::ImportKeyPoints(unsigned ID, vector<KeyPoint>&keypts){
	if(show == 0){
		return;
	}
	if(ID >= keypoint_ind.size()){
		keypoint_ind.resize(ID+1);
	}
	keypoint_ind[ID] = keypoint_id_count++;
	printf("keypoint_ind[%u] = %d\n", ID, keypoint_id_count);
	keypoints.push_back(keypts);
}

void SfmFeatureMatchViewer::ImportFeatureMatches(SfmFeatureMatch *match){
	if(show == 0){
		return;
	}
	NI = match->match_id_index_list.size();
	if(NI != img_ind.size()){
		printf("Missing images\n");
		exit(1);
	}

	matches.assign(match->matches.begin(), match->matches.end());
	match_id_index_list.resize(NI);
	for(unsigned i=0; i<match_id_index_list.size(); i++)
		match_id_index_list[i].assign(match->match_id_index_list[i].begin(), match->match_id_index_list[i].end());

	connect.resize(NI); // matrix specifying the number of "connections" (feature matches) between image pairs
	for(unsigned i=0; i<NI; i++)
			connect[i].assign(NI,0);
	// populate connect (for each matching feature, increment the corresponding components of connect)
	for(unsigned i=0; i<matches.size(); i++){
		connect[matches[i].id1][matches[i].id2]++;
		connect[matches[i].id2][matches[i].id1]++;
	}
}
void SfmFeatureMatchViewer::CreateImagePair(unsigned id1, unsigned id2){
	unsigned j = match_id_index_list[id1][id2];
	SfmMatch temp = matches[j];
	if(temp.id1 == id2){
		if(temp.id2 == id1){
			// flip id1 and id2
			unsigned tid = id1;
			id1 = id2;
			id2 = tid;
		}
	}

	// create new image that has one image on either side
	curImage = cvCreateImage(
			cvSize( max( img[img_ind[id1]]->width, img[img_ind[id2]]->width), img[img_ind[id1]]->height + img[img_ind[id2]]->height ),
			img[img_ind[id1]]->depth,
			img[img_ind[id1]]->nChannels);
	cvSet(curImage,cvScalar(200,200,200));
	cvSetImageROI(curImage, cvRect(0,0,img[img_ind[id1]]->width,img[img_ind[id1]]->height));
	cvCopy(img[img_ind[id1]], curImage);
	cvSetImageROI(curImage, cvRect(0,img[img_ind[id1]]->height,img[img_ind[id2]]->width,img[img_ind[id2]]->height));
	cvCopy(img[img_ind[id2]], curImage);
	cvResetImageROI(curImage);
	// print names
/**
	CvFont *font;
	cvInitFont(font, CV_FONT_HERSHEY_SIMPLEX, 1.0, 1.0);
	cvPutText(curImage,
			imgNames[img_ind[id1]].data(),
			cvPoint(0,0),
			font,
			cvScalar(0,0,255));
	cvPutText(curImage,
			imgNames[img_ind[id2]].data(),
			cvPoint(0,img[img_ind[id1]]->height),
			font,
			cvScalar(0,0,255));
			/**/

	// put circles at features and connect using lines


	for(int i=0; (i<viewMaxMatches) & (j<matches.size()); i++,j++){
		temp = matches[j];
		if(temp.id1 != id1)
			break;
		if(temp.id2 != id2)
			break;
		// get the SURF point for feature from image 1
			//CvSURFPoint* p1 = (CvSURFPoint *)cvGetSeqElem(features[feature_ind[id1]],temp.f1);
			//CvPoint2D32f pt1(p1->pt);
		CvPoint2D32f pt1=keypoints[keypoint_ind[id1]][temp.f1].pt;
		// get the SURF point for feature from image 2
		//	CvSURFPoint* p2 = (CvSURFPoint *)cvGetSeqElem(features[feature_ind[id2]],temp.f2);
		//	CvPoint2D32f pt2(p2->pt);
		CvPoint2D32f pt2=keypoints[keypoint_ind[id2]][temp.f2].pt;

		//CvPoint2D32f pt1=( (features[feature_ind[id1]])[temp.f1] ).pt;
		pt2.y += (float)(img[img_ind[id1]]->height); // shift point over so it falls within image 2

		if(~(viewMatchBox & 0x01)){
			// draw circle at pt
			cvCircle(curImage, cvPointFrom32f(pt1), 1, cvScalar(255,255,255));
			cvCircle(curImage, cvPointFrom32f(pt2), 1, cvScalar(255,255,255));
		}
		else{

		}
		if(viewMatchBox & 0x02){
			// draw line between points
			cvLine(curImage, cvPointFrom32f(pt1), cvPointFrom32f(pt2), cvScalar(255,255,255));
		}

	}
}
int SfmFeatureMatchViewer::ViewMatches(unsigned id1, unsigned id2){
	if(show == 0){
		return -1;
	}
#ifdef FEATURE_MATCHING_SANITY_CHECK
	if(id1!=id2)
#else
	if(id1==id2) // make sure that id's are different
#endif
		return -1;
	if((connect[id1][id2]==0) & (show < 3)){ // no matches, get out and move on
		return 0;
	}
	im1_id = id1; // save ids
	im2_id = id2;

	char title[300];
	sprintf(title,"Images %d and %d",id1, id2);
	cvNamedWindow(title,CV_WINDOW_AUTOSIZE);

	CreateImagePair(id1, id2);

	baseImage = cvCloneImage(curImage); // save this as the base image

	cvSetMouseCallback(title,HighlightFeatureCallback, (void *) this);

	mouseevent = -1;
	cvShowImage(title,curImage);
	while(1){
		if(mouseevent==CV_EVENT_LBUTTONDOWN){ // down-click: reset image
			cvCopy(baseImage, curImage);
			mouseevent = -1;
		}
		// on up-click, will highlight features on out image
		else if(mouseevent==CV_EVENT_LBUTTONUP){
			cvShowImage(title,curImage);
			mouseevent = -1;
		}
		if(cvWaitKey(15) == 27) break; // exit when user presses ESC key
	}

//	cvWaitKey(showTime);
	cvDestroyWindow(title);

	return 0;
}
void SfmFeatureMatchViewer::ViewMatchesAll(){
	for(unsigned id1=0; id1<img_ind.size(); id1++){
#ifdef FEATURE_MATCHING_SANITY_CHECK
		unsigned id2=id1; // make sure that features from a single image match themselves
#else
		for(unsigned id2=0; id2<id1; id2++)
#endif
			ViewMatches(id1,id2);
	}
}
// ViewConnectivity()
// Summary:
//   This viewer will create a window showing a single
//	 image and matched features from all images overlaid on top,
//	 connecting matched features with lines.  Furthermore,
//	 the features from the currently displayed image are highlighted
//   and the user can scroll through the images sequentially.
void SfmFeatureMatchViewer::ViewFeatureTrajectory(){

	int ID1=0, swap=0;
	if(show > 0){
		cvNamedWindow(imgName.data(), CV_WINDOW_AUTOSIZE);
		while(1){
				// create window to show image in
				// Draw the background image (should have imported data first)
				unsigned id1=img_ind[ID1], id2=0, id0=0;
				curImage = cvCreateImage(
							cvSize( img[img_ind[id1]]->width, img[img_ind[id1]]->height ),
							img[img_ind[id1]]->depth,
							img[img_ind[id1]]->nChannels);
				cvCopy(img[img_ind[id1]], curImage);
				// for each image in collection, draw a dot in 2D space for each keypoint
				unsigned num_keypoints_displayed;
				for(unsigned k=0; k<img_ind.size(); k++){
					//		DrawFeatures(id1);
					id1=img_ind[k];
					id2=img_ind[(k+1)%img_ind.size()];
					id0=img_ind[((int)(k)-1)%img_ind.size()];
					num_keypoints_displayed = (unsigned)(keypoints[keypoint_ind[id1]]).size();
					unsigned j = match_id_index_list[id1][id2];
					//printf("%u / %u\n\n",j,matches.size());
							swap=0;
							SfmMatch temp = matches[j];
							if(temp.id1 == id2){
								if(temp.id2 == id1){
									// flip id1 and id2
									unsigned tid = id1;
									id1 = id2;
									id2 = tid;
									id0=img_ind[(k+2)%img_ind.size()];
									swap=1;
								}
							}
					for(int i=0; (i<viewMaxMatches) & (j<matches.size()); i++,j++){
																					//not sure if this bit is necessary
							temp = matches[j];										//
							if(temp.id1 == id2){									//
								if(temp.id2 == id1){								//
									// flip id1 and id2								//
									unsigned tid = id1;								//
									id1 = id2;										//
									id2 = tid;										//
									swap=1-swap;									//
								}													//
							}														//
							if(temp.id1 != id1)
								break;
							if(temp.id2 != id2)
								break;
							// get the SURF point for keypoint from image 1
							//CvSURFPoint* p1 = (CvSURFPoint *)cvGetSeqElem(keypoints[feature_ind[id1]],temp.f1);
							CvPoint2D32f pt1=keypoints[keypoint_ind[id1]][temp.f1].pt;//(p1->pt);
							// get the SURF point for feature from image 2
							//CvSURFPoint* p2 = (CvSURFPoint *)cvGetSeqElem(features[feature_ind[id2]],temp.f2);
							CvPoint2D32f pt2=keypoints[keypoint_ind[id2]][temp.f2].pt;//(p2->pt);
							//pt2.y += (float)(img[img_ind[id1]]->height); // shift point over so it falls within image 2

							//if(~(viewMatchBox & 0x01)){
								// draw circle at pt
							//	cvCircle(curImage, cvPointFrom32f(pt1), 1, cvScalar(255,255,255));
							//	cvCircle(curImage, cvPointFrom32f(pt2), 1, cvScalar(255,255,255));
							//}
							//else{

							//}
							//if(viewMatchBox & 0x02){
								// draw line between points
								CvPoint2D32f pt_to_use = pt1, other=pt2;
								if(swap) pt_to_use=pt2, other=pt1;
								if(k==(unsigned)ID1){
									cvCircle(curImage, cvPointFrom32f(pt_to_use), 4, cvScalar(255,30,30), -1);
									//cvCircle(curImage, cvPointFrom32f(other), 4, cvScalar(30,30,255), -1);
								}
								cvLine(curImage, cvPointFrom32f(pt1), cvPointFrom32f(pt2), cvScalar(255,255,255));
							//}

						}

					id1=img_ind[k];
					id2=img_ind[(k+1)%img_ind.size()];
					id0=img_ind[((int)(k)-1)%img_ind.size()];
					j = match_id_index_list[id1][id0];
					if(j >= matches.size()) j=0;
												swap=0;
												temp = matches[j];
												if(temp.id1 == id0){
													if(temp.id2 == id1){
														// flip id1 and id0
														unsigned tid = id1;
														id1 = id0;
														id0 = tid;
														swap=1;
													}
												}
										for(int i=0; (i<viewMaxMatches) & (j<matches.size()); i++,j++){
																										//not sure if this bit is necessary
												temp = matches[j];										//
												if(temp.id1 == id0){									//
													if(temp.id2 == id1){								//
														// flip id1 and id0								//
														unsigned tid = id1;								//
														id1 = id0;										//
														id0 = tid;										//
														swap=1-swap;									//
													}													//
												}														//
												if(temp.id1 != id1)
													break;
												if(temp.id2 != id0)
													break;
												// get the SURF point for keypoint from image 1
												//CvSURFPoint* p1 = (CvSURFPoint *)cvGetSeqElem(features[feature_ind[id1]],temp.f1);
												CvPoint2D32f pt1=keypoints[keypoint_ind[id1]][temp.f1].pt;//(p1->pt);
												// get the SURF point for feature from image 2
												//CvSURFPoint* p2 = (CvSURFPoint *)cvGetSeqElem(features[feature_ind[id2]],temp.f2);
												CvPoint2D32f pt2=keypoints[keypoint_ind[id2]][temp.f2].pt;//(p2->pt);
												//pt2.y += (float)(img[img_ind[id1]]->height); // shift point over so it falls within image 2

												//if(~(viewMatchBox & 0x01)){
													// draw circle at pt
												//	cvCircle(curImage, cvPointFrom32f(pt1), 1, cvScalar(255,255,255));
												//	cvCircle(curImage, cvPointFrom32f(pt2), 1, cvScalar(255,255,255));
												//}
												//else{

												//}
												//if(viewMatchBox & 0x02){
													// draw line between points
													CvPoint2D32f pt_to_use = pt1, other=pt2;
													if(swap) pt_to_use=pt2, other=pt1;
													if(k==(unsigned)ID1){
														cvCircle(curImage, cvPointFrom32f(pt_to_use), 4, cvScalar(255,30,30), -1);
														//cvCircle(curImage, cvPointFrom32f(other), 4, cvScalar(30,255,255), -1);
													}
													//cvLine(curImage, cvPointFrom32f(pt1), cvPointFrom32f(pt2), cvScalar(255,255,255));
												//}

											}

				}
				char title[300];
				sprintf(title,"Image Features");
				cvShowImage(title,curImage);
				unsigned key_pressed= cvWaitKey(-1);

				if(key_pressed == 27) break;
				else if (key_pressed==63234) --ID1; // left arrow pressed
				else if (key_pressed==63235) ++ID1; // right arrow pressed
				if (ID1<0) ID1+=img_ind.size();
				if ((unsigned)ID1>=img_ind.size()) ID1-=img_ind.size();

		}
		cvDestroyWindow(imgName.data());
	}
}
// ViewConnectivity()
// Summary:
//   This viewer will create a window which has the connectivity
// 	 graph on the left and the images on the right.  The images will
//   be stacked vertically.
//   The user can select which images to show by selecting the
//   corresponding node in the graph
void SfmFeatureMatchViewer::ViewConnectivity(){
	if(show!=1){
		return;
	}
	int winHeight = 1000; // minimum window height
	int winWidth = 500; // minimum window width
	graphWidth = 500; // width of the graph portion
	for(unsigned i=0; i<NI; i++){
		winHeight = max(winHeight,2*img[img_ind[i]]->height);
		winWidth = max(winWidth,graphWidth+img[img_ind[i]]->width);
	}

	idcenter.resize(NI); // coordinates of ID centers

	// Place nodes along a circle
	/**
	int r = 200; // radius of circle that the graph exists on
	for(unsigned i=0; i<NI; i++){

		idcenter[img_ind[i]].x = (int)(float(r)*cos(-float(i)*360.0/float(NI)*DEG2RAD))+graphWidth/2;
		idcenter[img_ind[i]].y = (int)(float(r)*sin(-float(i)*360.0/float(NI)*DEG2RAD))+winHeight/2;
	}
	/**/
	// Group nodes by network - i.e. connected sets
	umat network(1);
	unsigned net_id = 0;
	network[net_id].push_back(img_ind[0]);
	uvec notvisited;
	notvisited.assign(img_ind.begin()+1,img_ind.end());
	unsigned net_node=0;
	uvec::iterator new_id;
	while(notvisited.size() > 0){
		for(unsigned i=0; i<connect.size(); i++){
			if(connect[network[net_id][net_node]][i] > 0){ // if there's a connection to this network node
				//check that the node hasn't been visited yet
				if((new_id = find(notvisited.begin(),notvisited.end(),i))!=notvisited.end()){
					network[net_id].push_back(*new_id);
					notvisited.erase(new_id);
				}
			}
		}
		net_node++;
		if(net_node >= network[net_id].size()){// we were at the last node in the network and no new ones were added
			if(notvisited.size() > 0){// and there's still nodes in the graph
				// create new network and initialize it with one of the nodes left in the graph
				network.push_back(uvec(1,notvisited[0]));
				notvisited.erase(notvisited.begin());
				net_node = 0;
				net_id++;
			}
		}
	}

	unsigned y_step = 60;
	unsigned cur_y = y_step;
	unsigned side_buffer = 30;
	for(unsigned i=0; i<network.size(); i++){
		if(network[i].size()==1){ // trivial case
			idcenter[network[i][0]].x = side_buffer;
			idcenter[network[i][0]].y = cur_y+side_buffer;
			cur_y += y_step;
		}
		else{
			int n_side = (int)ceil(sqrt(float(network[i].size())));
			n_side = min(n_side,6);

			for(unsigned k=0; k<network[i].size(); cur_y+=y_step){
				for(int j1=0; (j1<n_side) & (k<network[i].size()); j1++,k++){
					idcenter[network[i][k]].x = side_buffer + j1*(graphWidth - 2*side_buffer)/n_side;
					idcenter[network[i][k]].y = cur_y;
				}
			}
		}
	}



	maxconnect = 0;
	unsigned minconnect = 1<<30;
	for(unsigned i=0; i<NI; i++){
		for(unsigned j=0; j<i; j++){
			if(connect[i][j] > maxconnect)
				maxconnect = connect[i][j];
			if((connect[i][j] > 0) & (connect[i][j] < minconnect))
				minconnect = connect[i][j];
		}
	}

	graph = cvCreateImage(
			cvSize( winWidth, winHeight ),
			img[0]->depth,
			img[0]->nChannels);
	cvSet(graph,cvScalar(200,200,200));

	for(unsigned i=0; i<connect.size(); i++){
		cvCircle(graph,idcenter[img_ind[i]],node_r,cvScalar(255,0,0),4);
		for(unsigned j=0; j<i; j++){
			if(connect[img_ind[i]][img_ind[j]]>0){
			//	cvLine(graph,idcenter[img_ind[i]],idcenter[img_ind[j]],cvScalar(0,255*connect[img_ind[i]][img_ind[j]]/maxconnect,0),4);
				CvPoint center; CvSize axes; float rot_angle, start_angle, end_angle;
				center.x = (idcenter[img_ind[i]].x + idcenter[img_ind[j]].x)/2;
				center.y = (idcenter[img_ind[i]].y + idcenter[img_ind[j]].y)/2;
				axes.width = (int)sqrt(float((idcenter[img_ind[i]].x - idcenter[img_ind[j]].x)*(idcenter[img_ind[i]].x - idcenter[img_ind[j]].x) +
						(idcenter[img_ind[i]].y - idcenter[img_ind[j]].y)*(idcenter[img_ind[i]].y - idcenter[img_ind[j]].y)))/2.0;
				axes.height = 10;
				rot_angle = atan2(float(idcenter[img_ind[i]].y - idcenter[img_ind[j]].y),float(idcenter[img_ind[i]].x - idcenter[img_ind[j]].x))*RAD2DEG;
				start_angle = atan2(float(center.y - idcenter[img_ind[i]].y),float(center.x - idcenter[img_ind[i]].x))*RAD2DEG;
				end_angle = atan2(float(center.y - idcenter[img_ind[j]].y),float(center.x - idcenter[img_ind[j]].x))*RAD2DEG;
				cvEllipse(graph,center,axes,rot_angle,start_angle,end_angle,cvScalar(0,255*connect[img_ind[i]][img_ind[j]]/maxconnect,0),4);
				printf("connect[%d][%d] = %d\n",img_ind[i],img_ind[j],connect[img_ind[i]][img_ind[j]]);
			}
		}
	}
	im1_id = 1; im2_id = 0;
	// highlight nodes for these images
	cvCircle(graph,idcenter[img_ind[im1_id]],node_r,cvScalar(0,0,255),4);
	cvCircle(graph,idcenter[img_ind[im2_id]],node_r,cvScalar(0,0,255),4);

	CreateImagePair(im1_id, im2_id); // create image pair for images 0 and 1
	baseImage = cvCloneImage(curImage); // save this as the base image

	// place image pair into right side of graph
	cvSetImageROI(graph, cvRect(graphWidth,0,curImage->width,curImage->height));
	cvCopy(curImage, graph);
	cvResetImageROI(graph);

	char title[300] = "Image Connectivity";
	cvNamedWindow(title,CV_WINDOW_AUTOSIZE);

	cvSetMouseCallback(title,ConnectivityGraphCallback, (void *) this);
	cvShowImage(title,graph);
	mouseevent = -1;
	while(1){
		switch(mouseevent){
			case CV_EVENT_LBUTTONDOWN:  // down-click (only set this way if it occurs when mouse is in image region): reset image
				cvCopy(baseImage, curImage);
				// place image pair into right side of graph
				cvSetImageROI(graph, cvRect(graphWidth,0,curImage->width,curImage->height));
				cvCopy(curImage, graph);
				cvResetImageROI(graph);
				mouseevent = -1;
				break;
			case CV_EVENT_LBUTTONUP:
			case CV_EVENT_RBUTTONDOWN:
			case CV_EVENT_RBUTTONUP:
				cvShowImage(title,graph);
				mouseevent = -1;
				break;
			default:
				break;
		}
		if(cvWaitKey(15) == 27) break; // exit when user presses ESC key
	}
	cvDestroyWindow(title);
}
void SfmFeatureMatchViewer::View(){
	switch(show){
	case 0:
		break;
	case 1:
		ViewConnectivity();
		break;
	case 2:
	case 3:
		ViewMatchesAll();
	case 4:
		ViewFeatureTrajectory();
		break;
	default:
		printf("ERROR: Unknown Show Option for Feature Match Viewer\n");
		exit(1);
	}
}

void SfmFeatureMatchViewer::HighlightFeature(int x, int y){
	int m_id;
	if(y <= img[img_ind[im1_id]]->height){// mouse in first image
		m_id = 0;
	}
	else{
		m_id = 1;
		y -= img[img_ind[im1_id]]->height;
	}
	// check to see which matched feature of image m_id mouse is closest to
	unsigned j = match_id_index_list[im1_id][im2_id];
	int mindist = 1<<30;
	//CvSURFPoint  *cf, *mf; //closest feature and matching feature
	KeyPoint  cf, mf; //closest feature and matching feature
	for(int i=0; (i<viewMaxMatches) & (j<matches.size()); i++,j++){
		SfmMatch temp = matches[j];
		if(temp.id1 != im1_id){
			break;
		}
		if(temp.id2 != im2_id){
			break;
		}
		// get the SURF point for feature from image 1
		KeyPoint sp;
		if(m_id){
			sp=keypoints[keypoint_ind[im2_id]][temp.f2];
		}
		else{
			//sp = (CvSURFPoint *)cvGetSeqElem(features[feature_ind[im1_id]],temp.f1);
			sp=keypoints[keypoint_ind[im1_id]][temp.f1];
		}
		int pdist = ((int)sp.pt.x - x)*((int)sp.pt.x - x) + ((int)sp.pt.y - y)*((int)sp.pt.y - y);

		if(pdist < mindist){
			cf = sp;
			mindist = pdist;
			if(m_id){
				//mf = (CvSURFPoint *)cvGetSeqElem(features[feature_ind[im1_id]],temp.f1);
				mf=keypoints[keypoint_ind[im1_id]][temp.f1];
			}
			else{
				//mf = (CvSURFPoint *)cvGetSeqElem(features[feature_ind[im2_id]],temp.f2);
				mf=keypoints[keypoint_ind[im2_id]][temp.f2];
			}
		}
	}
	if(mindist == 1<<30) // never found a feature (or never entered for loop)
		return;
	if(mindist > cf.size*cf.size){ // if mouse isn't within feaure
		printf("not close enough\n");
		return;
	}
	CvPoint pt1, pt2;
	if(m_id){
		pt1 = cvPointFrom32f(mf.pt);
		pt2 = cvPointFrom32f(cf.pt);
	}
	else{
		pt2 = cvPointFrom32f(mf.pt);
		pt1 = cvPointFrom32f(cf.pt);
	}
	printf("ClickedFeature x,y: %f, %f\n", cf.pt.x, cf.pt.y);
	printf("MatchedFeature x,y: %f, %f\n\n", mf.pt.x, mf.pt.y);

	pt2.y += img[img_ind[im1_id]]->height;
	// draw small circle at pt's (center of features)
	cvCircle(curImage, pt1, 1, cvScalar(0,0,255));
	cvCircle(curImage, pt2, 1, cvScalar(0,0,255));


	// draw oriented box around features
	CvPoint pts_t[4];
	for(unsigned j=0; j<4; j++){
		pts_t[j].x = (int)( mf.pt.x+float(mf.size)*cos((mf.angle+45.+float(j)*90.)*DEG2RAD));
		pts_t[j].y = (int)( mf.pt.y+float(mf.size)*sin((mf.angle+45.+float(j)*90.)*DEG2RAD));
		if(m_id==0)
			pts_t[j].y += img[img_ind[im1_id]]->height;
	}
	CvPoint* pts[1] = {pts_t};
	int nPts[1] = {4};

	cvPolyLine(curImage,pts,nPts,1,1,cvScalar(0,0,255));

	for(unsigned j=0; j<4; j++){
		pts_t[j].x = (int)( cf.pt.x+float(cf.size)*cos((cf.angle+45.+float(j)*90.)*DEG2RAD));
		pts_t[j].y = (int)( cf.pt.y+float(cf.size)*sin((cf.angle+45.+float(j)*90.)*DEG2RAD));
		if(m_id==1)
			pts_t[j].y += img[img_ind[im1_id]]->height;
	}
	cvPolyLine(curImage,pts,nPts,1,1,cvScalar(0,0,255));

	// draw lines to indictate orientation of the features
	CvPoint orpt; //orientation
	orpt.x = (int)( cf.pt.x+float(cf.size)*cos(cf.angle*DEG2RAD)/sqrt(2.0));
	orpt.y = (int)( cf.pt.y+float(cf.size)*sin(cf.angle*DEG2RAD)/sqrt(2.0));
	if(m_id==1){
		orpt.y += img[img_ind[im1_id]]->height;
		cvLine(curImage, pt2, orpt, cvScalar(0,0,255));
	}
	else{
		cvLine(curImage, pt1, orpt, cvScalar(0,0,255));
	}
	orpt.x = (int)( mf.pt.x+float(mf.size)*cos(mf.angle*DEG2RAD)/sqrt(2.0));
	orpt.y = (int)( mf.pt.y+float(mf.size)*sin(mf.angle*DEG2RAD)/sqrt(2.0));
	if(m_id==0){
		orpt.y += img[img_ind[im1_id]]->height;
		cvLine(curImage, pt2, orpt, cvScalar(0,0,255));
	}
	else{
		cvLine(curImage, pt1, orpt, cvScalar(0,0,255));
	}

	// draw line connecting features
	cvLine(curImage, pt1, pt2, cvScalar(0,0,255));
}

void HighlightFeatureCallback(int event, int x, int y, int flags, void *param){
	SfmFeatureMatchViewer* view = (SfmFeatureMatchViewer *) param;

	// only continue if the left mouse has been hit
	switch(event){
		case CV_EVENT_LBUTTONDOWN:
			view->mouseevent = CV_EVENT_LBUTTONDOWN;
			return;
		case CV_EVENT_LBUTTONUP:
			view->mouseevent = CV_EVENT_LBUTTONUP;
			break;
		default:
			view->mouseevent = -1;
			return;
	}
	view->HighlightFeature(x,y);


}

void ConnectivityGraphCallback(int event, int x, int y, int flags, void *param){
	SfmFeatureMatchViewer* view = (SfmFeatureMatchViewer *) param;
	// on Macs, need to check if CTRL was held down (equivalent to right click)
	if(flags & CV_EVENT_FLAG_CTRLKEY){
		switch(event){
			case CV_EVENT_LBUTTONDOWN:
				event = CV_EVENT_RBUTTONDOWN;
				break;
			case CV_EVENT_LBUTTONUP:
				event = CV_EVENT_RBUTTONUP;
				break;
			default:
				break;
		}
	}

	// only continue if the left mouse has been hit
	switch(event){
		case CV_EVENT_LBUTTONDOWN:
			if( x >= view->graphWidth)
				view->mouseevent = CV_EVENT_LBUTTONDOWN;
			else
				view->mouseevent = -1;
			return;
		case CV_EVENT_LBUTTONUP: // select node to view
			view->mouseevent = CV_EVENT_LBUTTONUP;

			break;
		case CV_EVENT_RBUTTONDOWN: // select node to move node within graph
			if( x < view->graphWidth){
				view->mouseevent = CV_EVENT_RBUTTONDOWN;
				break;
			}
			else{
				view->mouseevent = -1;
				return;
			}
		case CV_EVENT_RBUTTONUP: // position node within graph
			if(view->moveEvent){
				view->mouseevent = CV_EVENT_RBUTTONUP;
				break;
			}
			else{
				view->mouseevent = -1;
				return;
			}
		default:
			view->mouseevent = -1;
			return;
	}
	// check to see if mouse is in graph or on images
	if( x < view->graphWidth ){// in graph
		if( (view->moveEvent) & (view->mouseevent == CV_EVENT_RBUTTONUP)){
			view->idcenter[view->moveNode].x = x;
			view->idcenter[view->moveNode].y = y;
			view->moveEvent = 0;
			// redraw graph
			cvSetImageROI(view->graph, cvRect(0,0,view->graphWidth,view->graph->height));
			cvSet(view->graph,cvScalar(200,200,200));

			for(unsigned i=0; i<view->connect.size(); i++){
				cvCircle(view->graph,view->idcenter[view->img_ind[i]],view->node_r,cvScalar(255,0,0),4);
				for(unsigned j=0; j<i; j++){
					if(view->connect[view->img_ind[i]][view->img_ind[j]]>0){
						CvPoint center; CvSize axes; float rot_angle, start_angle, end_angle;
						center.x = (view->idcenter[view->img_ind[i]].x + view->idcenter[view->img_ind[j]].x)/2;
						center.y = (view->idcenter[view->img_ind[i]].y + view->idcenter[view->img_ind[j]].y)/2;
						axes.width = (int)sqrt(float((view->idcenter[view->img_ind[i]].x - view->idcenter[view->img_ind[j]].x)*(view->idcenter[view->img_ind[i]].x - view->idcenter[view->img_ind[j]].x) +
								(view->idcenter[view->img_ind[i]].y - view->idcenter[view->img_ind[j]].y)*(view->idcenter[view->img_ind[i]].y - view->idcenter[view->img_ind[j]].y)))/2.0;
						axes.height = 10;
						rot_angle = atan2(float(view->idcenter[view->img_ind[i]].y - view->idcenter[view->img_ind[j]].y),float(view->idcenter[view->img_ind[i]].x - view->idcenter[view->img_ind[j]].x))*RAD2DEG;
						start_angle = atan2(float(center.y - view->idcenter[view->img_ind[i]].y),float(center.x - view->idcenter[view->img_ind[i]].x))*RAD2DEG;
						end_angle = atan2(float(center.y - view->idcenter[view->img_ind[j]].y),float(center.x - view->idcenter[view->img_ind[j]].x))*RAD2DEG;
						cvEllipse(view->graph,center,axes,rot_angle,start_angle,end_angle,cvScalar(0,255*view->connect[view->img_ind[i]][view->img_ind[j]]/view->maxconnect,0),4);

					//	cvLine(view->graph,view->idcenter[view->img_ind[i]],view->idcenter[view->img_ind[j]],
						//		cvScalar(0,255*view->connect[view->img_ind[i]][view->img_ind[j]]/view->maxconnect,0),4);
					}
				}
			}
			// highlight nodes for these images
			cvCircle(view->graph,view->idcenter[view->img_ind[view->im1_id]],view->node_r,cvScalar(0,0,255),4);
			cvCircle(view->graph,view->idcenter[view->img_ind[view->im2_id]],view->node_r,cvScalar(0,0,255),4);

			cvResetImageROI(view->graph);
		}

		// check to see which idcenter the mouse is at (if any)
		int id = -1;
		for(unsigned i=0; i<view->NI; i++){
			if((view->idcenter[i].x - x)*(view->idcenter[i].x - x) + (view->idcenter[i].y - y)*(view->idcenter[i].y - y) < view->node_r*view->node_r){
				id = i;
				break;
			}
		}
		if(id==-1){ // no node clicked on
			return;
		}
		if(view->mouseevent == CV_EVENT_RBUTTONDOWN){ // initialize the move event
			view->moveNode = id;
			view->moveEvent = 1;
			cvCircle(view->graph,view->idcenter[id],view->node_r,cvScalar(0,255,255),4);
		}

		view->selectedPair[view->selectInd++] = id;
		// highlight the selected nodes so that user knows they were selected
		// first, reset all the nodes to the regular color
		for(unsigned i=0; i<view->NI; i++)
			cvCircle(view->graph,view->idcenter[i],view->node_r,cvScalar(255,00,0),4);
		for(unsigned i=0; i<view->selectInd; i++)
			cvCircle(view->graph,view->idcenter[view->selectedPair[i]],view->node_r,cvScalar(0,0,255),4);

		if(view->selectInd==2){// this is the second image selected, now we can show a match
			// first, ensure that two different nodes were selected
			if(view->selectedPair[0]==view->selectedPair[1]){
				view->selectInd=1;
				return;
			}
			view->im1_id = view->selectedPair[0];
			view->im2_id = view->selectedPair[1];
			unsigned j = view->match_id_index_list[view->im1_id][view->im2_id];
			SfmMatch temp = view->matches[j];
			if(temp.id1 == view->im2_id){
				if(temp.id2 == view->im1_id){
					// flip id1 and id2
					unsigned tid = view->im1_id;
					view->im1_id = view->im2_id;
					view->im2_id = tid;
				}
			}
			// successful selection, view matches
			view->selectInd = 0; // reset so another pair can be selected
			view->CreateImagePair(view->im1_id, view->im2_id);
			view->baseImage = cvCloneImage(view->curImage); // save this as the base image
			// update the display
			// place image pair into right side of graph
			cvSetImageROI(view->graph, cvRect(view->graphWidth,0,view->curImage->width,view->curImage->height));
			cvCopy(view->curImage, view->graph);
			cvResetImageROI(view->graph);
		}
	}
	else{ // on images
		// highlight the features
		view->HighlightFeature(x-view->graphWidth,y);
		// update the display
		// place image pair into right side of graph
		cvSetImageROI(view->graph, cvRect(view->graphWidth,0,view->curImage->width,view->curImage->height));
		cvCopy(view->curImage, view->graph);
		cvResetImageROI(view->graph);
	}

}
