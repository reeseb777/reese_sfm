/********************************
 * SfM - Structure from Motion
 * Nick Richard
 * The Aerospace Corp, 2011
 *
 * This is the main script which works through a single
 * threaded implementation of the SfM software.
 *
 * Input Arguments:
 * - script file name
 *
 * Outputs:
 * - TBD
 *
 *********************************/




#include "cv.h"
#include "highgui.h"
#include "params.h"
#include "sfmDirectory.h"
#include "sfmimage.h"
#include "sfmExifExtractor.h"
#include "sfmViewImages.h"
#include "sfmFeatureDetector.h"
#include "sfmImageFeatures.h"
#include "sfmFeatureMatch.h"
#include "3D/pcloud_viewer.h"
#include "camera_init/camera_init.h"
#include "points_init/points_init.h"
#include "typedef.h"

// all the new API is put into "cv" namespace. Export its content
using namespace cv;

#ifdef MAIN_SFM
int main( int argc, char** argv ) {

	string script_fname;

	script_fname = argv[1]; // linux: use filename is the command line argument

	Params sim( script_fname ); //copies configuration data into structure
	printf("Reading script file...\n");
	bool ExitFlag = false;
	while ( !ExitFlag )
	{
		ExitFlag = sim.RunScriptFile(); //runs each line of the configuration data

		if ( !ExitFlag )
		{
			break;
		}
	}

	SfmDirectory imgdir;
	printf("Reading directory of images...\n");
	imgdir.ReadDirectory(sim.imgDir); // read in the directory of images
	int img_width, img_height;

	 // feature matching object
		SfmFeatureMatch myFeatMatch(&sim);
		FlannBasedMatcher matcher; // feature matching object
		vector< vector<KeyPoint> > allKeypoints;
		vector<Mat> allDescriptors;
		uvec ids;
		SfmFeatureMatchViewer myFeatMatchView(&sim); // viewing object for the feature matches


		// construct a matched-image graph, represented as a vector of indices
		vector<vector<int> > image_tree;

		printf("Starting feature detection...\n");
		unsigned image_num=12;
		unsigned skip=1; // take every nth frame
		if(image_num*skip > imgdir.files.size()) image_num=imgdir.files.size()/skip;
		//for(unsigned i=0; i<imgdir.files.size(); i++){
		for(unsigned i=0; i<image_num; i++){
			printf("\tImporting image %d of %d: %s\n",i,(int)imgdir.files.size(),imgdir.files[i].data());
			// import image from file
			SfmImage img(imgdir.files[i*skip]);
			img.imID = i; //assign ID number

			img_width=img.Width;
			img_height=img.Height;


			// view original image
			SfmViewImages imgViewer(&sim);
			imgViewer.ImportImage(&img);
			imgViewer.ViewImage();
			// copy original image into feature  Match Viewer
			myFeatMatchView.ImportImage(&img);
			// copy original image into Match Connectivity Viewer

			printf("\tExtracting EXIF tag info\n");
			// extractor EXIF tag info
			SfmExifExtractor exif;
			exif.Extract(&img); // extract EXIF tag information
			printf("Img width: %d\n",img_width);

			// detect features
			printf("\tDetecting features\n");

			SfmImageFeatures myFeats(img.imID);
			FeatureDetector *fdetector=(FeatureDetector*)( sim.getFeatureDetector() );
			DescriptorExtractor *dextractor=(DescriptorExtractor*)(sim.getDescriptorExtractor());
			Mat mask;
			Mat descriptors;
			vector<KeyPoint> keypoints;
			(*fdetector).detect(Mat(img.imgData,true),keypoints,mask);
			(*dextractor).compute(Mat(img.imgData,true),keypoints,descriptors);
			allKeypoints.push_back(keypoints);
			allDescriptors.push_back(descriptors);

				CvMemStorage* featureStorage = cvCreateMemStorage(0);
				CvSeq * featureSeq = cvCreateSeq(CV_SEQ_ELTYPE_POINT, sizeof(CvSeq),
				sizeof(CvPoint), featureStorage);
				cvSeqPushMulti( featureSeq, &keypoints, keypoints.size() );
				CvMemStorage* descriptorStorage = cvCreateMemStorage(0);
				CvSeq * descriptorSeq = cvCreateSeq(CV_SEQ_ELTYPE_POINT, sizeof(CvSeq),
				sizeof(CvPoint), descriptorStorage);
				cvSeqPush( descriptorSeq, &descriptors );
				myFeats.descriptors=descriptorSeq;
				myFeats.features=featureSeq;

			ids.push_back(img.imID);
			printf("%d keypoints found!\n", (int)keypoints.size());

			// view image used for feature detector and features overlaid
			SfmFeatureViewer imgFeatureViewer(&sim);
			imgFeatureViewer.ImportData(img.imgData, img.imID, keypoints);
			imgFeatureViewer.ViewFeatures();

			// copy features into feature Match Viewer
				myFeatMatchView.ImportKeyPoints(img.imID, keypoints);

			// add features to matching object
				myFeatMatch.AddImageFeatures(&myFeats);


		}

		printf("Completed feature detection\n");
		printf("Starting feature matching\n");
		// Match features


				/*for(unsigned k=1;k<allDescriptors.size();++k){//
					vector< vector<DMatch> > matches;
					matcher.knnMatch(allDescriptors[3], allDescriptors[k], matches, sim.ANNk);
					//for(unsigned j=0;j<matches.size();++j)
					//	printf("Match1 %d: %f\n", j, matches[j][0].distance);
					//printf("\n\n");
					printf(" -- Images %d and %d --\n",3,k);
					unsigned count=0;
					for(unsigned fnum=0;fnum<matches.size();++fnum){
						if(matches[fnum][0].distance < sim.MatchRatio * matches[fnum][1].distance && k!=3){
							printf("  - Feature %d -\n",fnum);
							for(unsigned j=0;j<matches[fnum].size();++j){
								printf("Match %d: %f\n", j, matches[fnum][j].distance);
							}
							++count;
						}
					}
					printf("%d matches\n\n",count);
				}*/
		myFeatMatch.consecutive=2;
		myFeatMatch.MatchAll_(matcher, allDescriptors);
		printf("Completed feature matching\n");

		// copy results into feature match viewer
			myFeatMatchView.ImportFeatureMatches(&myFeatMatch);

		// view matches
			myFeatMatchView.View();





	// add bundle adjust

		float focal_length=1.2*720;
		unsigned id1, id2, id0, n_points=4000;
		float meanx1, meany1, meanx2, meany2;
		vector<CvMat*> P;
		CvMat *P_initial;
		P_initial=cvCreateMat(3,4,CV_32F);
		cvSetIdentity(P_initial);
		P.push_back(P_initial);
		for(unsigned id=1; id<image_num;++id){
			id2=id;
			id1=id2-1;
			id0=id1-1;
			vector<Point2f> points1;	// matches to find stereo correspondance
			vector<Point2f> points2;

			vector<Point2f> common0;	// three-way matches needed to resolve scaling
			vector<Point2f> common1;
			vector<Point2f> common2;

			// initialize the points here ... */
			for( unsigned i = 0; i < myFeatMatch.matches.size(); i++ )
			{
				if( myFeatMatch.matches[i].id1==id1 && myFeatMatch.matches[i].id2==id2 ){
					points1.push_back(allKeypoints[id1][myFeatMatch.matches[i].f1].pt);
					points2.push_back(allKeypoints[id2][myFeatMatch.matches[i].f2].pt);
				}
				else if( myFeatMatch.matches[i].id1==id2 && myFeatMatch.matches[i].id2==id1 ){
					points1.push_back(allKeypoints[id1][myFeatMatch.matches[i].f2].pt);
					points2.push_back(allKeypoints[id2][myFeatMatch.matches[i].f1].pt);
				}
				else continue;
				meanx1+=points1[points1.size()-1].x;
				meany1+=points1[points1.size()-1].y;
				meanx2+=points2[points2.size()-1].x;
				meany2+=points2[points2.size()-1].y;
				if(points1.size()==n_points) break;
			}
			meanx1/=points1.size();
			meany1/=points1.size();
			meanx2/=points1.size();
			meany2/=points1.size();
			CvMat *P2, *status;
			if(P.size()==1)
				projectionMatFromCor(points1, points2, focal_length, focal_length, 0, 0, P2, status, CV_FM_RANSAC, 1./2, 0.99);
			else
				projectionMatFromCor(points1, points2, focal_length, focal_length, 0, 0, P[P.size()-1], P2, status, CV_FM_RANSAC, 1.0/2, 0.99);
			printf("P2:\n");
			printCvMat(P2, 3, 4);
			P.push_back(P2);
		}

			// Visualize the solved camera poses
						PCloudViewer *pcviewer=new PCloudViewer();
						pcviewer->setInstance(pcviewer);
						pcviewer->STEREO=0;
						pcviewer->flashing=GL_FALSE;
						pcviewer->init_openGL(argc, argv);
						//vector<vector<GLfloat> > pcloud;
						string fname="/Users/bmr27715/Documents/workspace/SFM/images/dino_data/point_data.txt";
						//pcviewer->readPointCloudTextFile(pcloud, fname, 4);
						//pcviewer->drawPointCloud(pcloud, PI/100/6, -1);
						string cam_fname="/Users/bmr27715/Documents/workspace/SFM2/images/dino_data/camera_data.txt";
						//pcviewer->readCameraTextFile(cam_fname);

						// initialize point locations
									CvMat *rotMat, *camCenter;
									rotMat=cvCreateMat(3,3,CV_32FC1);
									camCenter=cvCreateMat(3,1,CV_32FC1);
									vector<CvMat*> cam_centers;
									vector<CvMat*> cam_rots;
									vector<vector<GLfloat> > pcloud;
									vector<string> cam_fnames;
									vector<CvMat*> cam_fovs;
									vector<CvMat*> cam_dists;
									readCamTextFile(cam_fnames, cam_centers, cam_rots, cam_fovs, cam_dists);

						pcviewer->addCamera(focal_length/20,img_width/20,img_height/20);
						int camera_n=P.size();
						float temp1[camera_n][4][4], loc1[camera_n][3];
						CvMat *rot1;
						rot1=cvCreateMat(3,3,CV_32FC1);
						cvSetIdentity(rot1);
						for(int r=0;r<3;++r)
							for(int c=0;c<3;++c){
								temp1[0][r][c]=cvmGet(rot1,r,c);
								for(int i=1; i<camera_n;++i)
								temp1[i][r][c]=cvmGet(P[i],r,c);



							}

//						for(int i=0; i<camera_n;++i){
//							for(int r=0;r<3;++r)
//								for(int c=0;c<3;++c){
//									cvmSet(rotMat,r,c,cvmGet(P[i],r,c));
//								}
//							cam_rots.push_back(cvCloneMat(rotMat));
//							for(int k=0;k<3;++k){
//								cvmSet(camCenter,k,0,cvmGet(P[i],k,3));
//							}
//							cam_centers.push_back(cvCloneMat(camCenter));
//						}

						for(int k=0;k<3;++k){
							for(int i=0; i<camera_n; ++i){
								temp1[i][k][3]=temp1[i][3][k]=0.0;
								loc1[i][k]=0;
							}
						}
						for(int i=0; i<camera_n; ++i)
							temp1[i][3][3]=1.0;
						for(int i=1; i<camera_n;++i){
							loc1[i][0]=cvmGet(P[i],0,3);
							loc1[i][1]=cvmGet(P[i],1,3);
							loc1[i][2]=cvmGet(P[i],2,3);
						}
						for(int i=0; i<camera_n; ++i)
							pcviewer->drawCameraVis(loc1[i], temp1[i], i);
						pcviewer->camera_num=camera_n;
						for(int i=0; i<camera_n; ++i){
							vector<GLfloat> l1(loc1[i], loc1[i]+3);
							vector<GLfloat> rotMat1;
							for( int m=0;m<4;++m){
								for(int n=0;n<4;++n){
									rotMat1.push_back(temp1[i][m][n]);
								}
							}

							pcviewer->cameraRotations.push_back(rotMat1);
							pcviewer->cameraLocations.push_back(l1);
							if(i==0) (pcviewer->cameraFlashFrequencies).push_back(0.000002);
							else (pcviewer->cameraFlashFrequencies).push_back(0.0);
						}

						char pfname[100];
						sprintf(pfname, "images/dino_data/point_data.txt");
						char avgfname[100];
						sprintf(avgfname, "images/dino_data/point_data_averaged.txt");
						pcloud=average_points(myFeatMatch,
								solve_points(myFeatMatch, myFeatMatchView , cam_centers, cam_rots, 50, img_width, img_height,
														allKeypoints, pfname), avgfname );
						pcviewer->pointRenderOption=pcviewer->RENDER_CUBES;
						pcviewer->drawPointCloud(pcloud, PI/100/4, pcloud.size());
						  pcviewer->start_openGL();



			/*Mat pts1(n_pts,2, CV_32FC1);
			Mat pts2(n_pts,2, CV_32FC1);

			int i=0;
			for( unsigned j = 0; j < n_pts+3; j++ )
			{
				//if(j==2 || j==6 || j==7) continue;
				pts1.at<float>(i,0)=points1[j].x - (float)(img_width/2);
				pts1.at<float>(i,1)=points1[j].y - (float)(img_height/2);
				pts2.at<float>(i,0)=points2[j].x - (float)(img_width/2);
				pts2.at<float>(i,1)=points2[j].y - (float)(img_height/2);
				// sanity check: printf("%f\n",pts1.at<float>(i,1)-points1[i].y-img_width/2);
				 printf("%f, %f; %f, %f\n",points2[j].x, points2[j].y, pts2.at<float>(i,0), pts2.at<float>(i,1));
				 //outliers are 2, 6,7
				 i++;
			}
			Mat F =
			 findFundamentalMat(pts1, pts2, FM_8POINT, 1, 0.99);

			for( unsigned i = 0; i < n_pts; i++ )
			{

				Mat x1(3,1, CV_64FC1, 1), x2(3,1, CV_64FC1, 1);
				x1.at<double>(0,0)=pts1.at<float>(i,0);
				x1.at<double>(1,0)=pts1.at<float>(i,1);
				x2.at<double>(0,0)=pts2.at<float>(i,0);
				x2.at<double>(1,0)=pts2.at<float>(i,1);
				Mat Fx(3,1, CV_64FC1);
				Mat xFx(1,1, CV_64FC1);
				Fx = F*x1;
				xFx = x2.t()*Fx;
				printf("%f\tx1=(%f,%f), x2=(%f,%f)\n", xFx.at<float>(0,0),
						x1.at<double>(0,0),
						x1.at<double>(1,0),
						x2.at<double>(0,0),
						x2.at<double>(1,0));
			}

			printf("Fundamental Matrix \n");
							for(int row=0;row<3;++row){
								for(int col=0;col<3;++col){
									printf("%f\t", F.at<float>(row,col));
								}
								printf("\n");
							}
*/

			// initialize point locations
			/*vector<string> cam_fnames;
			vector<CvMat*> cam_centers;
			vector<CvMat*> cam_rots;
			vector<CvMat*> cam_fovs;
			vector<CvMat*> cam_dists;
			readCamTextFile(cam_fnames, cam_centers, cam_rots, cam_fovs, cam_dists);
			vector<vector<GLfloat> > pcloud;
			pcloud=average_points(myFeatMatch, solve_points(myFeatMatch, myFeatMatchView , cam_centers, cam_rots, 50, img_width, img_height,
					    					allKeypoints) );*/
			//init_openGL(argc, argv);
		    //drawPointCloud(pcloud, PI/100/4, pcloud.size());
		    //readCameraTextFile();
		    //start_openGL();
	return 0;
}
#endif
