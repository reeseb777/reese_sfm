#include "sfmimage.h"
//#include "cv.h"
#include "highgui.h"
//using namespace cv;

SfmImage::SfmImage(){
	numDateTimeTags = 0;
}

SfmImage::SfmImage(string name){
	imgName = name; // set image name
	ReadImage();
}

SfmImage::~SfmImage(){

}

void SfmImage::ReadImage(){
	ReadImage(imgName);
}
void SfmImage::ReadImage(string name){
	if(name != imgName){
		// reset the image information
	}
	imgName = name;
	imgData = cvLoadImage(imgName.data());
}
