#ifndef _SFM_IMAGE_FEATURES_H_
#define _SFM_IMAGE_FEATURES_H_

#include "typedef.h"
#include "cv.h"

class SfmImageFeatures{

public:
	SfmImageFeatures();
	SfmImageFeatures(unsigned ID);
	~SfmImageFeatures();
	CvSeq *features;
	CvSeq *descriptors;
	CvMemStorage *storage;
	CvSURFParams params;
	unsigned imID;

};

#endif
