#ifndef _PARAMS_H_
#define _PARAMS_H_

#include "typedef.h"
#include "cv.h"
#include <fstream>
#include <sstream>
#include <iostream>

// all the new API is put into "cv" namespace. Export its content
using namespace cv;

class Params{

public:
	Params(const string &);
	~Params();

	void SetIndependentParamsToDefault();
	void ProcessScriptLine(int);
	bool RunScriptFile( );
	void UpdateParams();
	FeatureDetector *getFeatureDetector();
	DescriptorExtractor *getDescriptorExtractor();

	// variables needed only by Params
	unsigned		scriptLineNum;
	string			scriptFname;
	svec			scriptLines;
	string			outputFnameRoot;


	// project parameters

	//********************
	// original image parameters
	string 			imgDir; 	// directory where the source images are located

	//********************
	// Feature Detection Parameters


	//********************
	// Feature Matching Parameters
	// ANN Parameters
	int 			ANNk; 		// number of nearest neighbors to search for
	float			MatchRatio; // ratio of distances of 2nd nearest and nearest neighbors that must be exceeded for match to be considered valid
	unsigned		MinMatchesPerPair; // number of matches between two images needed for that pair to be used by bundler

	//********************
	// viewer parameters
	unsigned		show;		// dictates what is shown on screen
					/**
					 * (show  ^ 0x01) == TRUE means raw images are shown
					 * (show  ^ 0x02) == TRUE means grayscale images with features are shown
					 * (show  ^ 0x04) == TRUE means feature matches are shown
					 */
	// original images
	int 			showImageTime; // time (in ms) to show an original image before destroying the window (can also enter keystroke to kill window). If 0 or negative, will wait indefinitely.

	// grayscale images used for feature detection
	int 			showGrayTime; // time (in ms) to show the pre-feature detection grayscale image before destroying the window (can also enter keystroke to kill window). If 0 or negative, will wait indefinitely.
	int				viewMaxFeatures; // maximum number of features to show on grayscale image. If equals -1, all features are shown
	int				viewFeatureBox; // determines how features are displayed.
									// 0 == points
									// 1 == boxes
	// images used for displaying the matches
	int				showMatchTime; // time (in ms) to show the two images with matched features before destroying the window (can also enter keystroke to kill window). If 0 or negative, will wait indefinitely.
	int 			viewMaxMatches; // maximum number of features to show on matched images.  If equal -1, all features are shown
	int				viewMatchBox; // determines how features of matches are displayed.
									// 0x0 == points without lines
									// 0x1 == boxes without lines
									// 0x2 == points with lines
									// 0x3 == boxes with lines
	int				viewMatches; 	// overrides show parameter
									// if ==0, don't show any match information.
									// if ==1, show interactive image connectivity graph
									// if ==2, show all image pairs that have matches (no connectivity graph)
									// if ==3, show all image pairs (no connectivity graph)



protected:
	FeatureDetector *featureDetector;
	DescriptorExtractor *descriptorExtractor;

private:
	svec			imgNames; 				// list of all the image names
	unsigned		nxtImg_ImgSpecs;		// index to the next image that will be processed by ImageSpecs
	unsigned 		nxtImg_FeatureExtraction; //index to the next image that will be processed by FeatureExtraction

	string GetNextImg_ImgSpecs(){return imgNames[nxtImg_ImgSpecs++];} //returns the name of the next image, and increments the index
	string GetNextImg_FeatureExtraction(){return imgNames[nxtImg_FeatureExtraction++];} //returns the name of the next image, and increments the index
};

#endif
