#ifndef _SFM_EXIF_EXTRACTOR_H_
#define _SFM_EXIF_EXTRACTOR_H_

#include "typedef.h"
//#include "jhead.h"
#include <math.h>
#include "sfmimage.h"

class SfmExifExtractor : public SfmImage{

public:
	SfmExifExtractor();
	~SfmExifExtractor();

	void Extract(SfmImage *img);

private:

	void CopyHereSfmImageParams(SfmImage *img);
	void CopyThereSfmImageParams(SfmImage *img);
	struct {
		// Info in the jfif header.
		// This info is not used much - jhead used to just replace it with default
		// values, and over 10 years, only two people pointed this out.
		char  Present;
		char  ResolutionUnits;
		short XDensity;
		short YDensity;
	}JfifHeader;


	void 	ErrFatal(string msg);
	void 	ResetJpgfile();
	int 	ReadJpegFile(const char * FileName, ReadMode_t ReadMode);
	int 	ReadJpegSections (FILE * infile, ReadMode_t ReadMode);
	void 	CheckSectionsAllocated();
	void 	process_COM (const uchar * Data, int length);
	void 	process_SOFn (const uchar * Data, int marker);
	int 	Get16m(const void * Short);
	void	Put16u(void * Short, unsigned short PutValue);
	int 	Get16u(void * Short);
	int 	Get32s(void * Long);
	void 	Put32u(void * Value, unsigned PutValue);
	unsigned	Get32u(void * Long);
	void 	DiscardData();
	void 	ShowXmp(Section_t XmpSection);
	void	process_EXIF (unsigned char * ExifSection, unsigned int length);
	void	ProcessExifDir(unsigned char * DirStart, unsigned char * OffsetBase,
	        	unsigned ExifLength, int NestingLevel);
	void 	ProcessGpsInfo(unsigned char * DirStart, int ByteCountUnused, unsigned char * OffsetBase, unsigned ExifLength);
	void 	PrintFormatNumber(void * ValuePtr, int Format, int ByteCount);
	void	ProcessMakerNote(unsigned char * ValuePtr, int ByteCount,
	        	unsigned char * OffsetBase, unsigned ExifLength);
	void	ProcessCanonMakerNoteDir(unsigned char * DirStart, unsigned char * OffsetBase,
	        	unsigned ExifLength);
	void 	ShowMakerNoteGeneric(unsigned char * ValuePtr, int ByteCount);

	void 	ErrNonfatal(string msg, int a1, int a2);

	double ConvertAnyFormat(void * ValuePtr, int Format);



	int 	FilesMatched;
	const char * CurrentFile;

	Section_t * Sections;
	int SectionsAllocated;
	int SectionsRead;
	int HaveAll;

	double FocalplaneXRes;
	double FocalplaneUnits;
	int ExifImageWidth;
	int MotorolaOrder;
	unsigned char * DirWithThumbnailPtrs;

	void * OrientationPtr[2];
	int    OrientationNumFormat[2];
	int NumOrientations;

	ivec BytesPerFormat;// = {0,1,1,2,4,8,1,1,2,4,8,4,8};

	int DumpExifMap;
	int ShowTags;
	int SupressNonFatalErrors; // Wether or not to pint warnings on recoverable errors


	//--------------------------------------------------------------------------
	// Describes tag values

	int TAG_INTEROP_INDEX          ;//0001
	int TAG_INTEROP_VERSION        ;//0002
	int TAG_IMAGE_WIDTH            ;//0100
	int TAG_IMAGE_LENGTH           ;//0101
	int TAG_BITS_PER_SAMPLE        ;//0102
	int TAG_COMPRESSION            ;//0103
	int TAG_PHOTOMETRIC_INTERP     ;//0106
	int TAG_FILL_ORDER             ;//010A
	int TAG_DOCUMENT_NAME          ;//010D
	int TAG_IMAGE_DESCRIPTION      ;//010E
	int TAG_MAKE                   ;//010F
	int TAG_MODEL                  ;//0110
	int TAG_SRIP_OFFSET            ;//0111
	int TAG_ORIENTATION            ;//0112
	int TAG_SAMPLES_PER_PIXEL      ;//0115
	int TAG_ROWS_PER_STRIP         ;//0116
	int TAG_STRIP_BYTE_COUNTS      ;//0117
	int TAG_X_RESOLUTION           ;//011A
	int TAG_Y_RESOLUTION           ;//011B
	int TAG_PLANAR_CONFIGURATION   ;//011C
	int TAG_RESOLUTION_UNIT        ;//0128
	int TAG_TRANSFER_FUNCTION      ;//012D
	int TAG_SOFTWARE               ;//0131
	int TAG_DATETIME               ;//0132
	int TAG_ARTIST                 ;//013B
	int TAG_WHITE_POINT            ;//013E
	int TAG_PRIMARY_CHROMATICITIES ;//013F
	int TAG_TRANSFER_RANGE         ;//0156
	int TAG_JPEG_PROC              ;//0200
	int TAG_THUMBNAIL_OFFSET       ;//0201
	int TAG_THUMBNAIL_LENGTH       ;//0202
	int TAG_Y_CB_CR_COEFFICIENTS   ;//0211
	int TAG_Y_CB_CR_SUB_SAMPLING   ;//0212
	int TAG_Y_CB_CR_POSITIONING    ;//0213
	int TAG_REFERENCE_BLACK_WHITE  ;//0214
	int TAG_RELATED_IMAGE_WIDTH    ;//1001
	int TAG_RELATED_IMAGE_LENGTH   ;//1002
	int TAG_CFA_REPEAT_PATTERN_DIM ;//828D
	int TAG_CFA_PATTERN1           ;//828E
	int TAG_BATTERY_LEVEL          ;//828F
	int TAG_COPYRIGHT              ;//8298
	int TAG_EXPOSURETIME           ;//829A
	int TAG_FNUMBER                ;//829D
	int TAG_IPTC_NAA               ;//83BB
	int TAG_EXIF_OFFSET            ;//8769
	int TAG_INTER_COLOR_PROFILE    ;//8773
	int TAG_EXPOSURE_PROGRAM       ;//8822
	int TAG_SPECTRAL_SENSITIVITY   ;//8824
	int TAG_GPSINFO                ;//8825
	int TAG_ISO_EQUIVALENT         ;//8827
	int TAG_OECF                   ;//8828
	int TAG_EXIF_VERSION           ;//9000
	int TAG_DATETIME_ORIGINAL      ;//9003
	int TAG_DATETIME_DIGITIZED     ;//9004
	int TAG_COMPONENTS_CONFIG      ;//9101
	int TAG_CPRS_BITS_PER_PIXEL    ;//9102
	int TAG_SHUTTERSPEED           ;//9201
	int TAG_APERTURE               ;//9202
	int TAG_BRIGHTNESS_VALUE       ;//9203
	int TAG_EXPOSURE_BIAS          ;//9204
	int TAG_MAXAPERTURE            ;//9205
	int TAG_SUBJECT_DISTANCE       ;//9206
	int TAG_METERING_MODE          ;//9207
	int TAG_LIGHT_SOURCE           ;//9208
	int TAG_FLASH                  ;//9209
	int TAG_FOCALLENGTH            ;//920A
	int TAG_SUBJECTAREA            ;//9214
	int TAG_MAKER_NOTE             ;//927C
	int TAG_USERCOMMENT            ;//9286
	int TAG_SUBSEC_TIME            ;//9290
	int TAG_SUBSEC_TIME_ORIG       ;//9291
	int TAG_SUBSEC_TIME_DIG        ;//9292

	int TAG_WINXP_TITLE            ;//9c9b // Windows XP - not part of exif standard.
	int TAG_WINXP_COMMENT          ;//9c9c // Windows XP - not part of exif standard.
	int TAG_WINXP_AUTHOR           ;//9c9d // Windows XP - not part of exif standard.
	int TAG_WINXP_KEYWORDS         ;//9c9e // Windows XP - not part of exif standard.
	int TAG_WINXP_SUBJECT          ;//9c9f // Windows XP - not part of exif standard.

	int TAG_FLASH_PIX_VERSION      ;//A000
	int TAG_COLOR_SPACE            ;//A001
	int TAG_PIXEL_X_DIMENSION      ;//A002
	int TAG_PIXEL_Y_DIMENSION      ;//A003
	int TAG_RELATED_AUDIO_FILE     ;//A004
	int TAG_INTEROP_OFFSET         ;//A005
	int TAG_FLASH_ENERGY           ;//A20B
	int TAG_SPATIAL_FREQ_RESP      ;//A20C
	int TAG_FOCAL_PLANE_XRES       ;//A20E
	int TAG_FOCAL_PLANE_YRES       ;//A20F
	int TAG_FOCAL_PLANE_UNITS      ;//A210
	int TAG_SUBJECT_LOCATION       ;//A214
	int TAG_EXPOSURE_INDEX         ;//A215
	int TAG_SENSING_METHOD         ;//A217
	int TAG_FILE_SOURCE            ;//A300
	int TAG_SCENE_TYPE             ;//A301
	int TAG_CFA_PATTERN            ;//A302
	int TAG_CUSTOM_RENDERED        ;//A401
	int TAG_EXPOSURE_MODE          ;//A402
	int TAG_WHITEBALANCE           ;//A403
	int TAG_DIGITALZOOMRATIO       ;//A404
	int TAG_FOCALLENGTH_35MM       ;//A405
	int TAG_SCENE_CAPTURE_TYPE     ;//A406
	int TAG_GAIN_CONTROL           ;//A407
	int TAG_CONTRAST               ;//A408
	int TAG_SATURATION             ;//A409
	int TAG_SHARPNESS              ;//A40A
	int TAG_DISTANCE_RANGE         ;//A40C
	int TAG_IMAGE_UNIQUE_ID        ;//A420

	int TAG_TABLE_SIZE; //(sizeof(TagTable) / sizeof(TagTable_t))
	vector<TagTable_t> TagTable;


	#define MAX_GPS_TAG 0x1e

	int NUM_FORMATS;

	int FMT_BYTE       ;//1
	int FMT_STRING     ;//2
	int FMT_USHORT     ;//3
	int FMT_ULONG      ;//4
	int FMT_URATIONAL  ;//5
	int FMT_SBYTE      ;//6
	int FMT_UNDEFINED  ;//7
	int FMT_SSHORT     ;//8
	int FMT_SLONG      ;//9
	int FMT_SRATIONAL  ;//10
	int FMT_SINGLE     ;//11
	int FMT_DOUBLE     ;//12

	unsigned TAG_GPS_LAT_REF    ;//1
	unsigned TAG_GPS_LAT        ;//2
	unsigned TAG_GPS_LONG_REF   ;//3
	unsigned TAG_GPS_LONG       ;//4
	unsigned TAG_GPS_ALT_REF    ;//5
	unsigned TAG_GPS_ALT        ;//6
	svec GpsTags;

	int M_SOF0  ;//0xC0          // Start Of Frame N
	int M_SOF1  ;//0xC1          // N indicates which compression process
	int M_SOF2  ;//0xC2          // Only SOF0-SOF2 are now in common use
    int M_SOF3  ;//0xC3
    int M_SOF5  ;//0xC5          // NB: codes C4 and CC are NOT SOF markers
    int M_SOF6  ;//0xC6
    int M_SOF7  ;//0xC7
    int M_SOF9  ;//0xC9
    int M_SOF10 ;//0xCA
    int M_SOF11 ;//0xCB
    int M_SOF13 ;//0xCD
    int M_SOF14 ;//0xCE
    int M_SOF15 ;//0xCF
    int M_SOI   ;//0xD8          // Start Of Image (beginning of datastream)
    int M_EOI   ;//0xD9          // End Of Image (end of datastream)
    int M_SOS   ;//0xDA          // Start Of Scan (begins compressed data)
    int M_JFIF  ;//0xE0          // Jfif marker
    int M_EXIF  ;//0xE1          // Exif marker.  Also used for XMP data!
    int M_XMP   ;//0x10E1        // Not a real tag (same value in file as Exif!)
    int M_COM   ;//0xFE          // COMment
    int M_DQT   ;//0xDB
    int M_DHT   ;//0xC4
    int M_DRI   ;//0xDD
    int M_IPTC  ;//0xED          // IPTC marker


	unsigned PSEUDO_IMAGE_MARKER; // Extra value.

};
#endif
