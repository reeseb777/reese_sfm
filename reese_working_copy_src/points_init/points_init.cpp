/*
 * points_init.cpp
 *
 *  Created on: Jun 24, 2011
 *      Author: bmr27715
 */
#include "points_init.h"

vector<vector<float> > solve_points(
		SfmFeatureMatch fm,
		SfmFeatureMatchViewer fmv,
		vector<CvMat*> cam_centers, // sorted so that index = img.ID
		vector<CvMat*> cam_rots,    // sorted so that index = img.ID
		float cam_focal_length,
		float cam_width_pixels,
		float cam_height_pixels,
		vector<vector<KeyPoint> > kpts,
		string fname
		)
		/*
		 * Return value should be a vector of points, one for each
		 * match that was passed in.  Some points are in more than
		 * two images which means more than one xyz location will
		 * be returned for these points.  These discrepancies should
		 * be resolved (i.e. averaged) in a later step.
		 */
{
	vector<vector<float> > result;
	vector<float> reprojection_error;
	char filename[100];
	strcpy(filename, fname.c_str());
	unsigned num_matches=fm.matches.size();
	for(unsigned i=0; i<num_matches; ++i){
		unsigned id1=fm.matches[i].id1;
		unsigned id2=fm.matches[i].id2;

		float cam_view_ang_x=40*DEG2RAD; // info not available for dino_data -- guessing
		float cam_view_ang_y=35*DEG2RAD;

		cam_focal_length=50;
		float x1 = kpts[id1][fm.matches[i].f1].pt.x;
		float y1 = kpts[id1][fm.matches[i].f1].pt.y;
		float x2 = kpts[id2][fm.matches[i].f2].pt.x;
		float y2 = kpts[id2][fm.matches[i].f2].pt.y;
		x1=x1/cam_width_pixels-0.5; // normalize pixels to % of ccd
		x2=x2/cam_width_pixels-0.5;
		y1=y1/cam_height_pixels-0.5;
		y2=y2/cam_height_pixels-0.5;
		CvMat *Vec1 = cvCreateMat(3,1,CV_32FC1);  // in camera coordinate frame
		cvmSet(Vec1,1,0, -1*tan(cam_view_ang_y)*y1);
		cvmSet(Vec1,0,0, 1*tan(cam_view_ang_x)*x1);
		cvmSet(Vec1,2,0, 1);
		CvMat *Vec2 = cvCreateMat(3,1,CV_32FC1);  // in camera coordinate frame
		cvmSet(Vec2,1,0, -1*tan(cam_view_ang_y)*y2);
		cvmSet(Vec2,0,0, 1*tan(cam_view_ang_x)*x2);
		cvmSet(Vec2,2,0, 1);

			printf("\nMatrix Vec1\n");
			for(int row=0;row<3;++row)
				printf("%f\n", cvmGet(Vec1,row,0));
			printf("\nMatrix Vec2\n");
			for(int row=0;row<3;++row)
				printf("%f\n", cvmGet(Vec2,row,0));

//
//		CvMat *ROTtr = cvCreateMat(3,3,CV_32FC1);
//		CvMat *TEMPtr = cvCreateMat(1,3,CV_32FC1);
//
//		cvTranspose(cam_rots[id1], ROTtr);
//		cvTranspose(Vec1, TEMPtr);
//		cvMatMul(TEMPtr, ROTtr, TEMPtr);
//		cvTranspose(TEMPtr, Vec1);
//
//		cvTranspose(cam_rots[id2], ROTtr);
//		cvTranspose(Vec2, TEMPtr);
//		cvMatMul(TEMPtr, ROTtr, TEMPtr);
//		cvTranspose(TEMPtr, Vec2);

		cvMatMul(cam_rots[id1],Vec1,Vec1); // rotate to global frame
		cvMatMul(cam_rots[id2],Vec2,Vec2);


		///debug
		/*for(int j=0; j<300; ++j){
			vector<float> xyzrgb;
						xyzrgb.push_back( cvmGet(Vec1,0,0)*j/300. );
						xyzrgb.push_back( cvmGet(Vec1,1,0)*j/300. );
						xyzrgb.push_back( cvmGet(Vec1,2,0)*j/300. );
						xyzrgb.push_back( 1.0);//(0.5*(s1.val[2]+s2.val[2]))/255);//-12)/12 );  // color = average of f1 and f2 colors
						xyzrgb.push_back( 1.0);//(0.5*(s1.val[1]+s2.val[1]))/255);//-12)/12 );
						xyzrgb.push_back( 1.0);//(0.5*(s1.val[0]+s2.val[0]))/255);//-12)/12 );
						xyzrgb.push_back( 0.2);// reprojection error
						result.push_back(xyzrgb);
		}
		for(int j=0; j<300; ++j){
					vector<float> xyzrgb;
								xyzrgb.push_back( cvmGet(Vec2,0,0)*j/300. );
								xyzrgb.push_back( cvmGet(Vec2,1,0)*j/300. );
								xyzrgb.push_back( cvmGet(Vec2,2,0)*j/300. );
								xyzrgb.push_back( 0.2);//(0.5*(s1.val[2]+s2.val[2]))/255);//-12)/12 );  // color = average of f1 and f2 colors
								xyzrgb.push_back( 0.2);//(0.5*(s1.val[1]+s2.val[1]))/255);//-12)/12 );
								xyzrgb.push_back( 1.0);//(0.5*(s1.val[0]+s2.val[0]))/255);//-12)/12 );
								xyzrgb.push_back( 0.2);// reprojection error
								result.push_back(xyzrgb);
				}
		break;*/
		///

				printf("\nRotation Matrix 2\n");
				for(int row=0;row<3;++row)
					printf("%f\t%f\t%f\n", cvmGet(cam_rots[id2],row,0), cvmGet(cam_rots[id2],row,1), cvmGet(cam_rots[id2],row,2));
		printf("\nRotated Vec1\n");
					for(int row=0;row<3;++row)
						printf("%f\n", cvmGet(Vec1,row,0));
					printf("\nRotated Vec2\n");
					for(int row=0;row<3;++row)
						printf("%f\n", cvmGet(Vec2,row,0));


		CvMat *A32 = cvCreateMat(3,2,CV_32FC1);
		cvmSet(A32,0,0, cvmGet(Vec1,0,0));
		cvmSet(A32,1,0, cvmGet(Vec1,1,0));
		cvmSet(A32,2,0, cvmGet(Vec1,2,0));
		cvmSet(A32,0,1, cvmGet(Vec2,0,0));
		cvmSet(A32,1,1, cvmGet(Vec2,1,0));
		cvmSet(A32,2,1, cvmGet(Vec2,2,0));
		CvMat *A23 = cvCreateMat(2,3,CV_32FC1);
		CvMat *A22 = cvCreateMat(2,2,CV_32FC1);
		CvMat *A22inv = cvCreateMat(2,2,CV_32FC1);
		CvMat *b31 = cvCreateMat(3,1,CV_32FC1);
		cvmSet(b31,0,0, cvmGet(cam_centers[id2],0,0) - cvmGet(cam_centers[id1],0,0));
		cvmSet(b31,1,0, cvmGet(cam_centers[id2],1,0) - cvmGet(cam_centers[id1],1,0));
		cvmSet(b31,2,0, cvmGet(cam_centers[id2],2,0) - cvmGet(cam_centers[id1],2,0));
		CvMat *b21 = cvCreateMat(2,1,CV_32FC1);
		CvMat *U_V = cvCreateMat(2,1,CV_32FC1);
		CvMat *X = cvCreateMat(3,1,CV_32FC1);     // storage for final xyz point
		cvTranspose(A32, A23);      // transpose(A32)
		cvMatMul(A23,A32,A22);		// (A^T)*A
		cvMatMul(A23,b31,b21);
		cvInv(A22,A22inv);
		cvMatMul(A22inv,b21,U_V);  // solving least-squares linear regression
		cvmSet(X,0,0, (cvmGet(cam_centers[id1],0,0)+cvmGet(U_V,0,0)*cvmGet(Vec1,0,0) )/2 + (cvmGet(cam_centers[id2],0,0)-cvmGet(U_V,1,0)*cvmGet(Vec2,0,0) )/2 );
		cvmSet(X,1,0, (cvmGet(cam_centers[id1],1,0)+cvmGet(U_V,0,0)*cvmGet(Vec1,1,0) )/2 + (cvmGet(cam_centers[id2],1,0)-cvmGet(U_V,1,0)*cvmGet(Vec2,1,0) )/2 );
		cvmSet(X,2,0, (cvmGet(cam_centers[id1],2,0)+cvmGet(U_V,0,0)*cvmGet(Vec1,2,0) )/2 + (cvmGet(cam_centers[id2],2,0)-cvmGet(U_V,1,0)*cvmGet(Vec2,2,0) )/2 );


		//Compare with vectors from camera center to triangulated point and compute reprojection error
		CvMat *Vec1X = cvCreateMat(3,1,CV_32FC1);  // direction to feature in camera coordinate frame
		cvmSet(Vec1X,0,0, cvmGet(X,0,0) - cvmGet(cam_centers[id1],0,0) );
		cvmSet(Vec1X,1,0, cvmGet(X,1,0) - cvmGet(cam_centers[id1],1,0) );
		cvmSet(Vec1X,2,0, cvmGet(X,2,0) - cvmGet(cam_centers[id1],2,0) );
		CvMat *Vec2X = cvCreateMat(3,1,CV_32FC1);  // direction to feature in camera coordinate frame
		cvmSet(Vec2X,0,0, cvmGet(X,0,0) - cvmGet(cam_centers[id2],0,0) );
		cvmSet(Vec2X,1,0, cvmGet(X,1,0) - cvmGet(cam_centers[id2],1,0) );
		cvmSet(Vec2X,2,0, cvmGet(X,2,0) - cvmGet(cam_centers[id2],2,0) );
		double length=sqrt(cvDotProduct(Vec1X,Vec1X)*cvDotProduct(Vec1,Vec1));
		double e1=sin(acos(cvDotProduct(Vec1,Vec1X)/length));
		length=sqrt(cvDotProduct(Vec2X,Vec2X)*cvDotProduct(Vec2,Vec2));
		double e2=sin(acos(cvDotProduct(Vec2,Vec2X)/length));
		double err=sqrt(e1*e1+e2*e2)*cam_focal_length;


			CvScalar s1, s2;         //Color of img1, img2 at feature coordinates
			//printf("%f, %f, %d, %d, %d\n", kpts[id1][fm.matches[i].f1].pt.x,
			//		kpts[id1][fm.matches[i].f1].pt.y, (int)(kpts[id1][fm.matches[i].f1].pt.x),
			//		(int)(kpts[id1][fm.matches[i].f1].pt.y), i);
			s1=cvGet2D((fmv.img)[id1],
					(int)(kpts[id1][fm.matches[i].f1].pt.y),
					(int)(kpts[id1][fm.matches[i].f1].pt.x)); // get the (i,j) pixel value
			s2=cvGet2D((fmv.img)[id2],
					(int)(kpts[id2][fm.matches[i].f2].pt.y),
					(int)(kpts[id2][fm.matches[i].f2].pt.x)); // get the (i,j) pixel value
			vector<float> xyzrgb;
			xyzrgb.push_back( cvmGet(X,0,0) );
			xyzrgb.push_back( cvmGet(X,1,0) );
			xyzrgb.push_back( cvmGet(X,2,0) );
			xyzrgb.push_back( (0.5*(s1.val[2]+s2.val[2]))/255);//-12)/12 );  // color = average of f1 and f2 colors
			xyzrgb.push_back( (0.5*(s1.val[1]+s2.val[1]))/255);//-12)/12 );
			xyzrgb.push_back( (0.5*(s1.val[0]+s2.val[0]))/255);//-12)/12 );
			xyzrgb.push_back( err);// reprojection error
			result.push_back(xyzrgb);
			reprojection_error.push_back(err);
	}
	printf("Reprojection Error MSD: %f\n", avg_dev_med(reprojection_error));
	float med=median(reprojection_error);
	float msd=avg_dev_med(reprojection_error);
	for(unsigned i=0; i<num_matches; i++){
		if(abs(reprojection_error[i]-med) > 2*msd){
			result[i][6]=1;
		}
		else result[i][6]=0;
	}
	FILE *fp = fopen(filename,"w");
		for(unsigned i=0; i<num_matches; i++){
			for(unsigned j=0; j<7; j++)
				fprintf(fp,"%f ",result[i][j]);
			fprintf(fp,"\n");
		}
	return result;
}
float pair_triangulation_error(
		CvMat* center1,	//input
		CvMat* center2, //input
		CvMat* &rot1,	//input
		CvMat* &rot2,	//input
		const vector<Point2f>&points1,	//input correspondances
		const vector<Point2f>&points2,	//input correspondances
		float cam_focal_length,
		float cam_width_pixels,
		float cam_height_pixels
		)
		/*
		 * Triangluation error computation for a pair of camera poses with given correspondances.
		 *
		 * The hope is to iterate to find a good relative pose estimate, calling this function many times
		 * 	with slightly different camera 2 poses, and picking the one with the lowest error.
		 *
		 * Return value should be a vector of reprojection errors, one for each
		 * match that was passed in.
		 */
{
	vector<float> reprojection_error;
	unsigned n_points=points1.size();

	CvMat *Vec1 = cvCreateMat(3,1,CV_32FC1);  // in camera coordinate frame
	CvMat *Vec2 = cvCreateMat(3,1,CV_32FC1);  // in camera coordinate frame
	CvMat *A32 = cvCreateMat(3,2,CV_32FC1);
	CvMat *A23 = cvCreateMat(2,3,CV_32FC1);
	CvMat *A22 = cvCreateMat(2,2,CV_32FC1);
	CvMat *A22inv = cvCreateMat(2,2,CV_32FC1);
	CvMat *b31 = cvCreateMat(3,1,CV_32FC1);
	CvMat *b21 = cvCreateMat(2,1,CV_32FC1);
	CvMat *U_V = cvCreateMat(2,1,CV_32FC1);
	CvMat *X = cvCreateMat(3,1,CV_32FC1);     // storage for final xyz point
	CvMat *Vec1X = cvCreateMat(3,1,CV_32FC1);  // direction to feature in camera coordinate frame
	CvMat *Vec2X = cvCreateMat(3,1,CV_32FC1);  // direction to feature in camera coordinate frame

	for(unsigned i=0; i<n_points; ++i){

		float cam_view_ang_x=40*DEG2RAD; // info not available for dino_data -- guessing
		float cam_view_ang_y=35*DEG2RAD;

		float x1 = points1[i].x;
		float y1 = points1[i].y;
		float x2 = points2[i].x;
		float y2 = points2[i].y;
		x1=x1/cam_width_pixels-0.5; // normalize pixels to % of ccd
		x2=x2/cam_width_pixels-0.5;
		y1=y1/cam_height_pixels-0.5;
		y2=y2/cam_height_pixels-0.5;

		cvmSet(Vec1,0,0, -1*tan(cam_view_ang_y)*y1);
		cvmSet(Vec1,1,0, 1*tan(cam_view_ang_x)*x1);
		cvmSet(Vec1,2,0, 1);

		cvmSet(Vec2,0,0, -1*tan(cam_view_ang_y)*y2);
		cvmSet(Vec2,1,0, 1*tan(cam_view_ang_x)*x2);
		cvmSet(Vec2,2,0, 1);

			/*printf("\nMatrix Vec1\n");
			for(int row=0;row<3;++row)
				printf("%f\n", cvmGet(Vec1,row,0));
			printf("\nMatrix Vec2\n");
			for(int row=0;row<3;++row)
				printf("%f\n", cvmGet(Vec2,row,0));*/


		cvMatMul(rot1,Vec1,Vec1); // rotate to global frame
		cvMatMul(rot2,Vec2,Vec2);


		cvmSet(A32,0,0, cvmGet(Vec1,0,0));
		cvmSet(A32,1,0, cvmGet(Vec1,1,0));
		cvmSet(A32,2,0, cvmGet(Vec1,2,0));
		cvmSet(A32,0,1, cvmGet(Vec2,0,0));
		cvmSet(A32,1,1, cvmGet(Vec2,1,0));
		cvmSet(A32,2,1, cvmGet(Vec2,2,0));
		cvmSet(b31,0,0, cvmGet(center2,0,0) - cvmGet(center1,0,0));
		cvmSet(b31,1,0, cvmGet(center2,1,0) - cvmGet(center1,1,0));
		cvmSet(b31,2,0, cvmGet(center2,2,0) - cvmGet(center1,2,0));
		cvTranspose(A32, A23);      // transpose(A32)
		cvMatMul(A23,A32,A22);		// (A^T)*A
		cvMatMul(A23,b31,b21);
		cvInv(A22,A22inv);
		cvMatMul(A22inv,b21,U_V);  // solving least-squares linear regression
		cvmSet(X,0,0, (cvmGet(center1,0,0)+cvmGet(U_V,0,0)*cvmGet(Vec1,0,0) )/2 + (cvmGet(center2,0,0)-cvmGet(U_V,1,0)*cvmGet(Vec2,0,0) )/2 );
		cvmSet(X,1,0, (cvmGet(center1,1,0)+cvmGet(U_V,0,0)*cvmGet(Vec1,1,0) )/2 + (cvmGet(center2,1,0)-cvmGet(U_V,1,0)*cvmGet(Vec2,1,0) )/2 );
		cvmSet(X,2,0, (cvmGet(center1,2,0)+cvmGet(U_V,0,0)*cvmGet(Vec1,2,0) )/2 + (cvmGet(center2,2,0)-cvmGet(U_V,1,0)*cvmGet(Vec2,2,0) )/2 );


		//Compare with vectors from camera center to triangulated point and compute reprojection error
		cvmSet(Vec1X,0,0, cvmGet(X,0,0) - cvmGet(center1,0,0) );
		cvmSet(Vec1X,1,0, cvmGet(X,1,0) - cvmGet(center1,1,0) );
		cvmSet(Vec1X,2,0, cvmGet(X,2,0) - cvmGet(center1,2,0) );
		cvmSet(Vec2X,0,0, cvmGet(X,0,0) - cvmGet(center2,0,0) );
		cvmSet(Vec2X,1,0, cvmGet(X,1,0) - cvmGet(center2,1,0) );
		cvmSet(Vec2X,2,0, cvmGet(X,2,0) - cvmGet(center2,2,0) );
		double length=sqrt(cvDotProduct(Vec1X,Vec1X)*cvDotProduct(Vec1,Vec1));
		double e1=sin(acos(cvDotProduct(Vec1,Vec1X)/length));
		length=sqrt(cvDotProduct(Vec2X,Vec2X)*cvDotProduct(Vec2,Vec2));
		double e2=sin(acos(cvDotProduct(Vec2,Vec2X)/length));
		double err=sqrt(e1*e1+e2*e2)*cam_focal_length;

		//Take care of the cases where the solved point is behind the camera
		//mean_u=mean_u*(i)/(i+1.0) + cvmGet(U_V,0,0)*1.0/(i+1.0);
		//mean_v=mean_v*(i)/(i+1.0) + cvmGet(U_V,1,0)*1.0/(i+1.0);
		//printf("Mean U: %f, Mean V: %f\n", mean_u, mean_v);
		//if(cvmGet(U_V,0,0)<0 || cvmGet(U_V,1,0)>0)
		//	err+=1*cam_focal_length;

			reprojection_error.push_back(err);
	}
	float tot=median(reprojection_error);

	return tot;
}
void sort(vector<float>&v){
	unsigned sorted=0; // length of sorted list
	int index=0;
	float temp;
	while(sorted<v.size()){
		index=sorted;
		while(index>0){
			if(v[index]<v[index-1]){
				//swap
				temp=v[index-1];
				v[index-1]=v[index];
				v[index]=temp;
				--index;
			}
			else break;
		}
		//one more element has been merged into sorted list
		++sorted;
	}
}
float median(const vector<float>&v){
	vector<float> vcpy;
	for(unsigned i=0;i<v.size();++i) vcpy.push_back(v[i]);
	sort(vcpy);
	return ( vcpy[v.size()/2] + vcpy[(v.size()-1)/2] )/2;
}
float total(const vector<float>&v){
	float val=0;
	for(unsigned i=0;i<v.size();++i) val+=v[i];
	return val;
}
float total(const vector<float>&v, int n){
	float val=0;
	for(unsigned i=0;i<n;++i) val+=v[i];
	return val;
}
float mean(const vector<float>&v){
	return total(v)/v.size();
}
//vector<float> minus(const vector<float>&v1, const vector<float>&v2){
//	for(int i=0;i<v.size();++i) v1[i]=v1[i]-v2[i];
//	return v1;
//}
vector<float> subtr(const vector<float>&v1, float f){
	vector<float> result;
	for(unsigned i=0;i<v1.size();++i) result.push_back(v1[i]-f);
	return result;
}
vector<float> abs(const vector<float>&v1){
	vector<float> result;
	for(unsigned i=0;i<v1.size();++i) result.push_back(abs(v1[i]));
	return result;
}
float avg_dev_med(const vector<float>&v){
	return mean(abs(subtr(v,median(v))));
}
/*vector<vector<float> > find_outlier_indices(
		SfmFeatureMatch fm,
		vector<vector<float> > points,
		vector<CvMat*> cam_centers, // sorted so that index = img.ID
		vector<CvMat*> cam_rots,    // sorted so that index = img.ID
		float cam_focal_length,
		float cam_width_pixels,
		float cam_height_pixels,
		vector<vector<KeyPoint> > kpts)
{
	vector<vector<float> > result;
		unsigned num_matches=fm.matches.size();
		for(unsigned i=0; i<num_matches; ++i){
			unsigned id1=fm.matches[i].id1;
			unsigned id2=fm.matches[i].id2;

			float cam_view_ang_x=40*DEG2RAD; // info not available for dino_data -- guessing
			float cam_view_ang_y=35*DEG2RAD;

			cam_focal_length=50;
			float x1 = kpts[id1][fm.matches[i].f1].pt.x;
			float y1 = kpts[id1][fm.matches[i].f1].pt.y;
			float x2 = kpts[id2][fm.matches[i].f2].pt.x;
			float y2 = kpts[id2][fm.matches[i].f2].pt.y;
			x1=x1/cam_width_pixels-0.5; // normalize pixels to % of ccd
			x2=x2/cam_width_pixels-0.5;
			y1=y1/cam_height_pixels-0.5;
			y2=y2/cam_height_pixels-0.5;
			CvMat *Vec1 = cvCreateMat(3,1,CV_32FC1);  // direction to feature in camera coordinate frame
			cvmSet(Vec1,0,0, -1*tan(cam_view_ang_y)*y1);
			cvmSet(Vec1,1,0, 1*tan(cam_view_ang_x)*x1);
			cvmSet(Vec1,2,0, 1);
			CvMat *Vec2 = cvCreateMat(3,1,CV_32FC1);  // direction to feature in camera coordinate frame
			cvmSet(Vec2,0,0, -1*tan(cam_view_ang_y)*y2);
			cvmSet(Vec2,1,0, 1*tan(cam_view_ang_x)*x2);
			cvmSet(Vec2,2,0, 1);

			cvMatMul(cam_rots[id1],Vec1,Vec1); // rotate to global frame
			cvMatMul(cam_rots[id2],Vec2,Vec2);

			//Compare with vectors from camera center to triangulated point
			CvMat *Vec1X = cvCreateMat(3,1,CV_32FC1);  // direction to feature in camera coordinate frame
			cvmSet(Vec1X,0,0, points[i][0] - cvmGet(cam_centers[id1],0,0) );
			cvmSet(Vec1X,1,0, points[i][1] - cvmGet(cam_centers[id1],1,0) );
			cvmSet(Vec1X,2,0, points[i][2] - cvmGet(cam_centers[id1],2,0) );
			CvMat *Vec2X = cvCreateMat(3,1,CV_32FC1);  // direction to feature in camera coordinate frame
			cvmSet(Vec2X,0,0, points[i][0] - cvmGet(cam_centers[id2],0,0) );
			cvmSet(Vec2X,1,0, points[i][1] - cvmGet(cam_centers[id2],1,0) );
			cvmSet(Vec2X,2,0, points[i][2] - cvmGet(cam_centers[id2],2,0) );
			double length=sqrt(cvDotProduct(Vec1X,Vec1X)*cvDotProduct(Vec1,Vec1));
			double e1=sin(acos(cvDotProduct(Vec1,Vec1X)/length));
			length=sqrt(cvDotProduct(Vec2X,Vec2X)*cvDotProduct(Vec2,Vec2));
			double e2=sin(acos(cvDotProduct(Vec2,Vec2X)/length));
			double err=sqrt(e1*e1+e2*e2)*cam_focal_length;
		}
}*/


vector<vector<float> > average_points(
		SfmFeatureMatch fm,
		vector<vector<float> > points,
		string fname){
	/*
	 * This function takes the point cloud produced by solve_points() and averages points that correspond in the SfmFeatureMatch structure
	 */

	vector<long > index_map; // every (possibly redundant) point, points[i] will be averaged in to result[index_map[i]]

	vector<vector<float> > result; // vector to be returned (redundancies removed by averaging)
	vector<int> num_averaged; // number of points being averaged for each element of result
	long current_index=0;
	int n_cols=7;

	char filename[100];
	strcpy(filename, fname.c_str());
	unsigned num_matches=fm.matches.size();


	num_matches=fm.matches.size();
	for(unsigned i=0; i<num_matches; ++i){
		index_map.push_back(-1);
	}
	for(unsigned i=0; i<num_matches; ++i){
		unsigned id1=fm.matches[i].id1;
		unsigned id2=fm.matches[i].id2;
		int f1=fm.matches[i].f1;
		int f2=fm.matches[i].f2;

		for(unsigned j=0; j<i; ++j){
			//search for an already mapped match
			if(
					(fm.matches[j].id1==id1 && fm.matches[j].f1==f1) ||
					(fm.matches[j].id2==id1 && fm.matches[j].f2==f1) ||
					(fm.matches[j].id1==id2 && fm.matches[j].f1==f2) ||
					(fm.matches[j].id2==id2 && fm.matches[j].f2==f2)
				){
				//map to the already mapped index
				index_map[i]=index_map[j];
				num_averaged[index_map[j]]=num_averaged[index_map[j]]+1;
				break; // exit the for loop search
			}
		}
		if(index_map[i]==-1){
			index_map[i]=current_index++; // map to a new index
			num_averaged.push_back(1); // a new point will be added to result
		}
	}
	for(unsigned i=0; i<current_index; ++i){
		vector<float> xyzrgb;
		for(int j=0; j<n_cols; ++j)
			xyzrgb.push_back(0);
		result.push_back(xyzrgb);
	}
	for(unsigned i=0; i<num_matches; ++i){
		for(int j=0; j<n_cols; ++j)
			result[index_map[i]][j]=result[ index_map[i] ][j]+points[i][j];
	}
	for(unsigned i=0; i<current_index; ++i){
		for(int j=0; j<n_cols; ++j)
			result[i][j]=result[i][j]/num_averaged[i];
	}

	FILE *fp = fopen(filename,"w");
		for(unsigned i=0; i<current_index; i++){
			for(unsigned j=0; j<n_cols; j++)
				fprintf(fp,"%f ",result[i][j]);
			fprintf(fp,"\n");
		}
	return result;
}
