/*
 * points_init.h
 *
 *  Created on: Jun 24, 2011
 *      Author: bmr27715
 */

#ifndef POINTS_INIT_H_
#define POINTS_INIT_H_

#include "cv.h"
#include "highgui.h"
#include "../params.h"
#include "../sfmDirectory.h"
#include "../sfmimage.h"
#include "../sfmExifExtractor.h"
#include "../sfmViewImages.h"
#include "../sfmFeatureDetector.h"
#include "../sfmImageFeatures.h"
#include "../sfmFeatureMatch.h"
#include "math.h"
#include "../typedef.h"

void readPCloudTextFile(vector<vector<float> >&xyzrgb);
vector<vector<float> > solve_points(
		SfmFeatureMatch fm,
		SfmFeatureMatchViewer fmv,
		vector<CvMat*> cam_centers, // sorted so that index = img.ID
		vector<CvMat*> cam_rots,    // sorted so that index = img.ID
		float cam_focal_length,
		float cam_width_pixels,
		float cam_height_pixels,
		vector<vector<KeyPoint> > kpts,
		string fname
		);
		/*
		 * Return value should be a vector of points, one for each
		 * match that was passed in.  Some points are in more than
		 * two images which means more than one xyz location will
		 * be returned for these points.  These discrepancies should
		 * be resolved (i.e. averaged) in a later step.
		 */

float pair_triangulation_error(
		CvMat* center1,	//input
		CvMat* center2, //input
		CvMat* &rot1,	//input
		CvMat* &rot2,	//input
		const vector<Point2f>&points1,	//input correspondances
		const vector<Point2f>&points2,	//input correspondances
		float cam_focal_length,
		float cam_width_pixels,
		float cam_height_pixels
		);

/*vector<vector<float> > find_outlier_indices(
		SfmFeatureMatch fm,
		vector<vector<float> > points,
		vector<CvMat*> cam_centers, // sorted so that index = img.ID
		vector<CvMat*> cam_rots,    // sorted so that index = img.ID
		float cam_focal_length,
		float cam_width_pixels,
		float cam_height_pixels,
		vector<vector<KeyPoint> > kpts);*/

vector<vector<float> > average_points(
		SfmFeatureMatch fm,
		vector<vector<float> > points,
		string fname);

void sort(vector<float>&v);
float median(const vector<float>&v);
float total(const vector<float>&v);
float mean(const vector<float>&v);
//vector<float> minus(const vector<float>&v1, const vector<float>&v2);
vector<float> subtr(const vector<float>&v1, float f);
float avg_dev_med(const vector<float>&v);
vector<float> abs(const vector<float>&v1);

#endif /* POINTS_INIT_H_ */
