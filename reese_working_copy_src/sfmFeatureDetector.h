#ifndef _SFM_FEATURE_DETECTOR_H_
#define _SFM_FEATURE_DETECTOR_H_

#include "typedef.h"
//#include "jhead.h"
#include <math.h>
#include "sfmimage.h"
#include "sfmImageFeatures.h"

class SfmFeatureDetector{

public:
	SfmFeatureDetector();
	~SfmFeatureDetector();

	void ImportImageData(SfmImage *im);
	void Detect(SfmImageFeatures *feats);
	void Detect(SfmImage *im, SfmImageFeatures *feats);

	unsigned imID;
	IplImage *img;

private:
	CvSeq *features;
	CvSeq *descriptors;

};
#endif
