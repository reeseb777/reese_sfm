#ifndef _SFM_FEATURE_MATCH_H_
#define _SFM_FEATURE_MATCH_H_

#include "typedef.h"
#include "sfmImageFeatures.h"
#include "params.h"
using namespace cv;

class SfmFeatureMatch{

public:
	SfmFeatureMatch(Params *sim);
	SfmFeatureMatch(Params *sim, uvec all_ids, vector<cv::Mat> allDescriptors );
	~SfmFeatureMatch();

	void AddImageFeatures(SfmImageFeatures *feats);
	int	 Match(unsigned i1, unsigned i2, cv::Mat &m_indices, cv::Mat &m_dists);
	void MatchAll();
	void MatchAll_(const DescriptorMatcher&matcher, const vector<Mat>&allDescriptors);

	vector<SfmMatch> matches; // list of all matches between image features
	imat match_id_index_list; // provides an index to where matches for image pairs begins in matches
	vector<vector<int> > weighted_match_list; // each element is a vector with format <id1, id2, num_matches>
	unsigned consecutive; // set to positive number to only match an image with the previous n frames


private:
	vector<cv::Mat> m_descriptor; // contains a matrix of feature descriptors for each image
	uvec	IDs; // provides the image IDs corresponding to the members of m_descriptors
	int knn; // number of nearest neighbors to include in the ANN search
	float thresh; // denotes how much closer the nearest neighbor must be than the 2nd nearest neighbor to be considered a true match
	unsigned min_matches; //number of matches required for a pair of images to be used by bundler
};


#endif
