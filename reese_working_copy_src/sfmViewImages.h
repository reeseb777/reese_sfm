#ifndef _SFM_VIEW_IMAGES_H_
#define _SFM_VIEW_IMAGES_H_

#include "typedef.h"
#include "sfmimage.h"
#include "sfmFeatureDetector.h"
#include "sfmFeatureMatch.h"
#include "params.h"
#include <time.h>
using namespace cv;

class SfmViewImages : public SfmImage{

public:
	SfmViewImages(Params *sim);
	~SfmViewImages();

	void ImportImage(SfmImage *img);
	void ViewImage();

protected:
	unsigned 	show;		// show original image
	int 		showTime;	// how long to show image before destroying window

};

class SfmFeatureViewer : public SfmImage{
public:
	SfmFeatureViewer(Params *sim);
	~SfmFeatureViewer();

	void ImportData(SfmFeatureDetector *fdetect, SfmImageFeatures *feats);
	void ImportData(IplImage*img, unsigned imageID, vector<KeyPoint>&keypts);
	void ImportKeyPoints(vector<KeyPoint>&keypoints);
	void ViewFeatures();

protected:
	unsigned 	show;			// show feature image
	int 		showTime;		// how long to show image before destroying window
	int 		viewMaxFeatures;// maximum number of features to show
	int 		viewFeatureBox;// how to display features (point or box)
	CvSeq 		*features;		// pointer to features
	vector<KeyPoint> keypoints;


};

class SfmFeatureMatchViewer : public SfmImage{
public:
	SfmFeatureMatchViewer(Params *sim);
	~SfmFeatureMatchViewer();

	void ImportImage(SfmImage *im);
	void ImportFeatures(SfmImageFeatures *feats);
	void ImportKeyPoints(unsigned ID, vector<KeyPoint>&keypts);
	void ImportFeatureMatches(SfmFeatureMatch *match);
	void CreateImagePair(unsigned, unsigned);
	int ViewMatches(unsigned, unsigned);
	void ViewMatchesAll();
	void ViewConnectivity();
	void ViewFeatureTrajectory();
	void View();
	void HighlightFeature(int x, int y);



	friend void HighlightFeatureCallback(int event, int x, int y, int flags, void *param);
	friend void ConnectivityGraphCallback(int event, int x, int y, int flags, void *param);

	vector<IplImage *>img;
private:
	unsigned 	show;
	// if ==0, don't show any match information.
	// if ==1, show interactive image connectivity graph
	// if ==2, show all image pairs that have matches (no connectivity graph)
	// if ==3, show all image pairs (no connectivity graph)
	int 		showTime;
	int 		viewMatchBox;// how to display matches (point or box)
//	int			viewOnlyMatches; //if ==0, shows all image pairs. if ==1, shows only image pairs which have a sufficient number of matching features

	int	viewMaxMatches;

	svec imgNames; // imgNames[i] provides the filename corresponding to img[i]
	vector<CvSeq *>features;
	vector< vector<KeyPoint> > keypoints;
	vector<SfmMatch> matches;
	imat match_id_index_list;

	uvec img_ind; // img_ind[ID] provides the index of img that contains the image data for image ID

	uvec feature_ind, keypoint_ind;

	IplImage *baseImage; // image containing a pair of images and objects around their matching features
	IplImage *curImage; // image currently being viewed
	IplImage *graph; // image of the connectivity graph
	unsigned im1_id; //id's of images currently being viewed
	unsigned im2_id;
	int mouseevent; // mouse event
	int graphWidth; // width of the graph portion of the display

	vector<CvMemStorage *> feature_storage;
	int 		img_id_count;
	int 		feature_id_count, keypoint_id_count;

	umat 		connect; // matrix specifying the number of "connections" (feature matches) between image pairs
	unsigned 	maxconnect; //matrix number of connections between to 2 images
	unsigned	NI; // number of images;
	vector<CvPoint> idcenter; // centers of image nodes on graph
	int 		node_r; // radius of the image nodes
	unsigned 	selectedPair[2]; // images selected from the connectivity graph
	unsigned 	selectInd; // index of which in the pair is selected next (0 or 1)
	unsigned	moveNode; // image node that will be moved within the graph
	unsigned 	moveEvent; // if ==1, a node is currently being moved
};

// mouse callbacks (cannot be class functions)
void HighlightFeatureCallback(int event, int x, int y, int flags, void *param); //highlights a single matching feature for a pair of images
void ConnectivityGraphCallback(int event, int x, int y, int flags, void *param); //selects which images will be shown from the image connectivity graph

// support functions
int SURFCompareHessian(const void* a, const void* b, void* userdata);
#endif

