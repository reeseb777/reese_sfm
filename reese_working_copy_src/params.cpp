/********************************
 * SfM - Structure from Motion
 * Nick Richard
 * The Aerospace Corp, 2011
 *
 * This file contains the Params class, which
 * is used to parse through the script file
 * and pass pertinent details onto the remainder of the
 * program.
 *
 *********************************/

#include "params.h"

Params::Params(const string &script){

	SetIndependentParamsToDefault( );

	scriptFname = script;

	scriptLines.clear();
	ifstream fin( scriptFname.data(), ios::in );

	// Read the script file into scriptLines if possible
	if ( !fin.fail() ){
		while( !fin.eof() ) {
			char str[300];
			fin.getline( str, 300 );
			scriptLines.push_back( str );
		}
		fin.close();
	}
	else {
		cout << "\nCould not open script file: " << scriptFname << flush;
		cout << endl; exit(1);
	}

	// Check for errors by processing all lines while skipping over those that
	// that are empty, or begin with the word "Run", or are commented out (% is the comment character in the config file)
	for (unsigned i=0; i<scriptLines.size(); i++) {
		istringstream line_ss( scriptLines[i] );
		string word1;
		line_ss >> word1;
		if ( ( word1 != "Run" )&&( word1[0] != '%' )&&( word1 != "") )
			ProcessScriptLine( i );
	}

	// Make copy of the script file for record keeping
	ofstream fout( (outputFnameRoot+".script.dat").data(), ios::out );
	for (unsigned i=0; i<scriptLines.size(); i++) {
		fout << "\n" << scriptLines[i] << flush;
	}
	fout.close();

	// Set the following pointer to the beginning of the script.  This pointer will
	// be incremented by one after each line of the script has been executed.
	scriptLineNum = 0;

	SetIndependentParamsToDefault( );
}
Params::~Params(){

}

bool Params::RunScriptFile( ) {
	for (unsigned i=scriptLineNum; i<scriptLines.size(); i++)
	{
		istringstream line_ss( scriptLines[i] );
		string word1, word2, word3, word4;
		line_ss >> word1 >> word2 >> word3 >> word4;
		++scriptLineNum;

		if ( word1 == "Run" )
		{
			UpdateParams();
			return false;
		}
		else if ( word1 == "SimReset" )
		{
			// Set params to default values
			SetIndependentParamsToDefault( );
		}
		else if (( word1[0] == '%' )||( word1=="" ))
		{
			// Ignore by doing nothing
		}
		else
		{
			// Process command
			ProcessScriptLine( i );
		}
	}

	return true;
}

void Params::SetIndependentParamsToDefault() {

	imgDir				= ".";	// current directory

	ANNk				= 2;
	MatchRatio			= 0.36;
	MinMatchesPerPair	= 16;

	show 				= 0; 	// don't show any images
	showImageTime 		= 0;	// wait until keystroke to close original images
	showGrayTime 		= 0;	// wait until keystroke to close grayscale images with features on them
	viewMaxFeatures		= 100;	// show up to 100 features
	viewFeatureBox		= 1;	// use boxes around features
	showMatchTime		= 0; 	// wait for keystroke
	viewMaxMatches		= -1;	// show all matches
	viewMatchBox		= 0x02; // points connected by lines
	viewMatches			= 0;	// don't show any matches

	nxtImg_ImgSpecs = 0;
	nxtImg_FeatureExtraction = 0;
	static SurfFeatureDetector sfd;
	static SurfDescriptorExtractor sde;
	featureDetector		= &(sfd);
	descriptorExtractor = &(sde);
}

void Params::ProcessScriptLine( int line_num ) {
	istringstream line_ss( scriptLines[ line_num ] );

//	string word1, word2, word3, word4;
//	string word5, word6, word7, word8, word9;
//	string word10, word11, word12, word13, word14, word15;
//	line_ss >> word1 >> word2 >> word3 >> word4;
//	line_ss >> word5 >> word6 >> word7 >> word8 >> word9;
//	line_ss >> word10 >> word11 >> word12 >> word13 >> word14 >> word15;

	svec words(15);
	for(unsigned i=0; i<words.size(); i++){
		line_ss >> words[i];
	}

	if ( words[0] == "ANN") {
		if (words[1] == "k") {
			ANNk = atoi(words[2].data());
			if(ANNk < 2){
				cout << "\nEncountered an error in line #";
				cout << line_num << " of the script:\n";
				cout << ">> " << scriptLines[ line_num ];
				cout << endl;
				exit(1);
			}
		}
		else {
			cout << "\nEncountered an error in line #";
			cout << line_num << " of the script:\n";
			cout << ">> " << scriptLines[ line_num ];
			cout << endl;
			cout << "Parameter '" << words[1] << "' not supported for ANN";
			cout << endl;
			exit(1);
		}
	}
	else if ( words[0] == "ImageDirectories" ) {
		imgDir = words[1];
	//	word_ss >> imgDirs;
	}
	else if ( words[0] == "MatchRatio") {
		MatchRatio = atof(words[1].data());
		if((MatchRatio < 0.0) || (MatchRatio > 1.0)){
			cout << "\nEncountered an error in line #";
			cout << line_num << " of the script:\n";
			cout << ">> " << scriptLines[ line_num ];
			cout << endl;
			cout << "MatchRatio must be between 0.0 and 1.0";
			cout << endl;
			exit(1);
		}
	}
	else if ( words[0] == "MinMatchesPerPair") {
		MinMatchesPerPair = atof(words[1].data());
		if((MinMatchesPerPair < 0) || (MinMatchesPerPair > 100)){
			cout << "\nEncountered an error in line #";
			cout << line_num << " of the script:\n";
			cout << ">> " << scriptLines[ line_num ];
			cout << endl;
			cout << "MinMatchesPerPair must be between 0 and 100";
			cout << endl;
			exit(1);
		}
	}
	else if ( words[0] == "DetectionAlgo") {
		if (words[1] == "surf" || words[1] == "SURF") {
			static SurfFeatureDetector sfd(400,3,4);
			static SurfDescriptorExtractor sde;
			featureDetector		= &sfd;
			descriptorExtractor = &sde;
		}
		else if (words[1] == "sift" || words[1] == "SIFT") {
			static SiftFeatureDetector sfd(0.04/2, 10);
			static SiftDescriptorExtractor sde;
			featureDetector		= &sfd;
			descriptorExtractor = &sde;
		}
		else if (words[1] == "star" || words[1] == "STAR") {
			static StarFeatureDetector sfd(0.04/2, 10);
			static BriefDescriptorExtractor sde;
			featureDetector		= &sfd;
			descriptorExtractor = &sde;
		}
		else if (words[1] == "fast" || words[1] == "FAST") {
			static FastFeatureDetector sfd(0.04/2, 10);
			static SurfDescriptorExtractor sde;
			featureDetector		= &sfd;
			descriptorExtractor = &sde;
		}
		else{
			cout << "\nEncountered an error in line #";
			cout << line_num << " of the script:\n";
			cout << ">> " << scriptLines[ line_num ];
			cout << endl;
			cout << "'" << words[1] << "' is not a valid command";
			cout << endl;
			exit(1);
		}
	}
	else if (words[0] == "Show") {
		if (words[1] == "Images") {
			show |= 0x01; // show images
			for(unsigned i=2; i<words.size(); i+=2){
				if(words[i] == "")
					break;
				else if(words[i] == "ShowTime"){
					showImageTime = atoi(words[i+1].data());
				}
				else{
					cout << "\nEncountered an error in line #";
					cout << line_num << " of the script:\n";
					cout << ">> " << scriptLines[ line_num ];
					cout << endl;
					cout << "'" << words[i] << "' is not a valid command";
					cout << endl;
					exit(1);
				}
			}
		}
		if (words[1] == "Features") {
			show |= 0x02; // show feature images
			for(unsigned i=2; i<words.size(); i+=2){
				if(words[i] == "")
					break;
				else if(words[i] == "ShowTime"){ // amount of time to show image before destroying the window (can destroy window manually by pressing keystroke)
					showGrayTime = atoi(words[i+1].data());
				}
				else if(words[i] == "ViewMax"){
					viewMaxFeatures = atoi(words[i+1].data());
				}
				else if(words[i] == "Marker"){
					if((words[i+1] == "points") || (words[i+1] == "point") || (words[i+1] == "Points") || (words[i+1] == "Point"))
						viewFeatureBox = 0;
					else if((words[i+1] == "boxes") || (words[i+1] == "box") || (words[i+1] == "Boxes") || (words[i+1] == "Box"))
						viewFeatureBox = 1;
					else{
						cout << "\nEncountered an error in line #";
						cout << line_num << " of the script:\n";
						cout << ">> " << scriptLines[ line_num ];
						cout << endl;
						cout << "Marker must be followed by a valid expression";
						cout << endl;
						exit(1);
					}
				}
				else{
					cout << "\nEncountered an error in line #";
					cout << line_num << " of the script:\n";
					cout << ">> " << scriptLines[ line_num ];
					cout << endl;
					cout << "'" << words[i] << "' is not a valid command";
					cout << endl;
					exit(1);
				}
			}
		}
		if (words[1] == "Matches") {
			show |= 0x04;
			unsigned i=3; // word to start the loop
			if(words[2] == "Connectivity"){
				viewMatches = 1;
			}
			else if(words[2] == "OnlyMatches"){
				viewMatches = 2;
			}
			else if(words[2] == "AllMatches"){
				viewMatches = 3;
			}
			else if(words[2] == "Trajectory"){
				viewMatches = 4;
			}
			else{ // assume that we'll show the interactive connectivity graph
				viewMatches = 1;
				i = 2; // start on the second word
			}
			// loop through the words remaining on this line
			// these determine how the data is displayed
			for( ; i<words.size(); i+=2){
				if(words[i] == "")
					break;
				else if(words[i] == "ShowTime"){
					showMatchTime  = atoi(words[i+1].data());
				}
				else if(words[i] == "ViewMax"){
					viewMaxMatches = atoi(words[i+1].data());
				}
				else if(words[i] == "Marker"){
					if((words[i+1] == ".points") || (words[i+1] == ".point") || (words[i+1] == ".Points") || (words[i+1] == ".Point") ||
					   (words[i+1] == "points") || (words[i+1] == "point") || (words[i+1] == "Points") || (words[i+1] == "Point"))
						viewMatchBox = 0;
					else if((words[i+1] == ".boxes") || (words[i+1] == ".box") || (words[i+1] == ".Boxes") || (words[i+1] == ".Box") ||
							(words[i+1] == "boxes") || (words[i+1] == "box") || (words[i+1] == "Boxes") || (words[i+1] == "Box"))
						viewMatchBox = 1;
					else if((words[i+1] == "_points") || (words[i+1] == "_point") || (words[i+1] == "_Points") || (words[i+1] == "_Point"))
						viewMatchBox = 2;
					else if((words[i+1] == "_boxes") || (words[i+1] == "_box") || (words[i+1] == "_Boxes") || (words[i+1] == "_Box"))
						viewMatchBox = 3;
					else{
						cout << "\nEncountered an error in line #";
						cout << line_num << " of the script:\n";
						cout << ">> " << scriptLines[ line_num ];
						cout << endl;
						cout << "Marker must be followed by a valid expression";
						cout << endl;
						exit(1);
					}
				}
				else{
					cout << "\nEncountered an error in line #";
					cout << line_num << " of the script:\n";
					cout << ">> " << scriptLines[ line_num ];
					cout << endl;
					cout << "'" << words[i] << "' is not a valid command";
					cout << endl;
					exit(1);
				}
			}
		}
		else{
			cout << "\nEncountered an error in line #";
			cout << line_num << " of the script:\n";
			cout << ">> " << scriptLines[ line_num ];
			cout << endl;
			cout << "'" << words[1] << "' is not a valid command";
			cout << endl;
			exit(1);
		}
	}
	else {
		cout << "\nEncountered an error in line #";
		cout << line_num << " of the script:\n";
		cout << ">> " << scriptLines[ line_num ];
		cout << endl;
		exit(1);
	}
}

void Params::UpdateParams(){
}

FeatureDetector* Params::getFeatureDetector(){
	return featureDetector;
}
DescriptorExtractor* Params::getDescriptorExtractor(){
	return descriptorExtractor;
}
