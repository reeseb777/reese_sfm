#include "sfmFeatureMatch.h"

SfmFeatureMatch::SfmFeatureMatch(Params *sim){
	knn = sim->ANNk;
	thresh = sim->MatchRatio;
	min_matches = sim->MinMatchesPerPair;
	consecutive=-1;
}

SfmFeatureMatch::SfmFeatureMatch(Params *sim, uvec all_ids, vector<cv::Mat> allDescriptors ){
	knn = sim->ANNk;
	thresh = sim->MatchRatio;
	min_matches = sim->MinMatchesPerPair;
	consecutive=-1;
	m_descriptor=allDescriptors;
	IDs=all_ids;
}
SfmFeatureMatch::~SfmFeatureMatch(){

}

void SfmFeatureMatch::AddImageFeatures(SfmImageFeatures *feats){
	int length = (int)(feats->descriptors->elem_size/sizeof(float));
	cv::Mat descr(feats->descriptors->total, length, CV_32F);

	CvSeqReader img_reader;
	float* img_ptr = descr.ptr<float>(0);
	cvStartReadSeq(feats->descriptors, &img_reader);
	for (int i=0; i<feats->descriptors->total; i++){
		const float* descriptor = (const float*)img_reader.ptr;
		CV_NEXT_SEQ_ELEM(img_reader.seq->elem_size, img_reader);
		memcpy(img_ptr, descriptor, length*sizeof(float));
		img_ptr += length;
	}
	m_descriptor.push_back(descr);
	IDs.push_back(feats->imID); // IDs[i] gives the ID# corresponding to the descriptors located at m_descriptor[i]

}

int SfmFeatureMatch::Match(unsigned i1, unsigned i2, cv::Mat &m_indices, cv::Mat &m_dists){
	if(i1 >= m_descriptor.size())
		return -1;
	if(i2 >= m_descriptor.size())
		return -1;
	if(m_descriptor[i1].rows == 0)
		return 0;
	if(m_descriptor[i2].rows == 0)
		return 0;
	cv::flann::Index flann_index(m_descriptor[i1], cv::flann::KDTreeIndexParams(4)); // using 4 randomized kdtrees
	flann_index.knnSearch(m_descriptor[i2], m_indices, m_dists, 2, cv::flann::SearchParams(64)); // maximum number of leafs checked
	return 1;
}

void SfmFeatureMatch::MatchAll(){
	SfmMatch match_temp;
	for(unsigned i=0; i<m_descriptor.size(); i++){
		if(m_descriptor[i].rows == 0)// no features found in this image
			continue;
		if(match_id_index_list.size() <= IDs[i])
			match_id_index_list.resize(IDs[i]+1);

		// create the ANN search tree for the descriptors in image IDs[i]
		cv::flann::Index flann_index(m_descriptor[i], cv::flann::KDTreeIndexParams(4));
#ifdef FEATURE_MATCHING_SANITY_CHECK
		unsigned j=i;
		{
#else
		for(unsigned j=0; j<i; j++){
#endif
			if(match_id_index_list[IDs[i]].size() <= IDs[j])
				match_id_index_list[IDs[i]].resize(IDs[j]+1);
			match_id_index_list[IDs[i]][IDs[j]] = matches.size(); // note the starting location in matches for descriptor matches between images IDs[i] and IDs[j]
			if(match_id_index_list[IDs[j]].size() <= IDs[i])
				match_id_index_list[IDs[j]].resize(IDs[i]+1);
			match_id_index_list[IDs[j]][IDs[i]] = matches.size(); // make sure you can access list regardless of sort of indices
			if(m_descriptor[j].rows == 0)
				continue;

			// allocate space for the matching descriptor indices.
			// Note: m_indices[k][l] holds the l-th nearest feature index of image IDs[i] to feature k of image IDs[j]
			cv::Mat m_indices(m_descriptor[j].rows, knn, CV_32S);
			// allocate space for the matching descriptor distances
			cv::Mat m_dists(m_descriptor[j].rows, knn, CV_32F);
			flann_index.knnSearch(m_descriptor[j], m_indices, m_dists, knn, cv::flann::SearchParams(64));

			int *indices_ptr = m_indices.ptr<int>(0);
			float *dists_ptr = m_dists.ptr<float>(0);
			vector<SfmMatch> cur_matches;
			for(int k=0; k<m_indices.rows; k++){
				if(dists_ptr[knn*k]<thresh*dists_ptr[knn*k+1]){ // if the nearest neighbor is sufficiently closer to the feature than the 2nd nearest neighbor
					match_temp.id1 = IDs[i]; // save image indices
					match_temp.id2 = IDs[j];
					match_temp.f1 = indices_ptr[knn*k]; // save index of the feature in image IDs[i]
					match_temp.f2 = k; // save the index of the feature in image IDs[j]
					cur_matches.push_back(match_temp);
				}
			}
			// ensure that there are no duplicate matches
			for(unsigned k=0; k<cur_matches.size(); k++){
				ivec duplicates;
				for(unsigned l=k+1; l<cur_matches.size(); l++){
					if(cur_matches[k].f1 == cur_matches[l].f1){
						duplicates.push_back(l);
					}
				}
				if(duplicates.size()>0){
					// remove duplicates, starting with the last ones
					for(unsigned l=duplicates.size(); l>0; --l){
						cur_matches.erase(cur_matches.begin()+duplicates[l]);
					}
					cur_matches.erase(cur_matches.begin()+k);
				}
			}
			// only keep matches if there are at least min_matches between these two images
			if(cur_matches.size() >= min_matches){
				matches.insert(matches.end(),cur_matches.begin(),cur_matches.end());
			}
		}
	}
}

void SfmFeatureMatch::MatchAll_(const DescriptorMatcher&matcher, const vector<Mat>&allDescriptors){
		SfmMatch match_temp;
		for(unsigned i=0; i<m_descriptor.size(); i++){
			if(m_descriptor[i].rows == 0)// no features found in this image
				continue;
			if(match_id_index_list.size() <= IDs[i])
				match_id_index_list.resize(IDs[i]+1);

			// create the ANN search tree for the descriptors in image IDs[i]
			//cv::flann::Index flann_index(m_descriptor[i], cv::flann::KDTreeIndexParams(4));
	#ifdef FEATURE_MATCHING_SANITY_CHECK
			unsigned j=i;
			{
	#else
			for(unsigned j=0; j<i; j++){
	#endif
				if(i-j > consecutive) continue;
				if(match_id_index_list[IDs[i]].size() <= IDs[j])
					match_id_index_list[IDs[i]].resize(IDs[j]+1);
				match_id_index_list[IDs[i]][IDs[j]] = matches.size(); // note the starting location in matches for descriptor matches between images IDs[i] and IDs[j]
				if(match_id_index_list[IDs[j]].size() <= IDs[i])
					match_id_index_list[IDs[j]].resize(IDs[i]+1);
				match_id_index_list[IDs[j]][IDs[i]] = matches.size(); // make sure you can access list regardless of sort of indices
				if(allDescriptors[j].rows == 0)
					continue;

				vector< vector<DMatch> > d_matches;
				matcher.knnMatch(allDescriptors[i], allDescriptors[j], d_matches, knn);
				vector<SfmMatch> cur_matches;
				unsigned count=0;
				for(unsigned fnum=0;fnum<d_matches.size();++fnum){
					if(d_matches[fnum][0].distance < thresh * d_matches[fnum][1].distance){ // if the nearest neighbor is sufficiently closer to the feature than the 2nd nearest neighbor
						match_temp.id1 = IDs[i]; // save image indices
						match_temp.id2 = IDs[j];
						match_temp.f1 = d_matches[fnum][0].queryIdx;
						match_temp.f2 = d_matches[fnum][0].trainIdx;
						cur_matches.push_back(match_temp);
						++count;
					}
				}
				printf(" -- Images %d and %d --\n",i,j);
				printf("      %d matches\n",count);
				// ensure that there are no duplicate matches
				for(unsigned k=0; k<cur_matches.size(); k++){
					ivec duplicates;
					for(unsigned l=k+1; l<cur_matches.size(); l++){
						if(cur_matches[k].f1 == cur_matches[l].f1){
							duplicates.push_back(l);
						}
					}
					if(duplicates.size()>0){
						// remove duplicates, starting with the last ones
						for(unsigned l=duplicates.size(); l>0; --l){
							cur_matches.erase(cur_matches.begin()+duplicates[l]);
						}
						cur_matches.erase(cur_matches.begin()+k);
					}
				}
				// only keep matches if there are at least min_matches between these two images
				if(cur_matches.size() >= min_matches){
					matches.insert(matches.end(),cur_matches.begin(),cur_matches.end());
					vector<int> vec;
					vec.push_back(IDs[i]);
					vec.push_back(IDs[j]);
					vec.push_back(cur_matches.size());
					weighted_match_list.push_back(vec);
				}
			}
		}
	}
