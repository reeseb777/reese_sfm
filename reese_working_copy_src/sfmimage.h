#ifndef _SFM_IMAGE_H_
#define _SFM_IMAGE_H_

#include "typedef.h"
//#include "jhead.h"
#include <math.h>

#define MAX_COMMENT_SIZE 2000
#define MAX_DATE_COPIES 10

class SfmImage{

public:
	SfmImage();
	SfmImage(string name);
	~SfmImage();

	void ReadImage();
	void ReadImage(string name);

	string imgName;
	IplImage *imgData;
	unsigned imID;

	time_t FileDateTime;

	unsigned FileSize;
	char  CameraMake   [32];
	char  CameraModel  [40];
	char  DateTime     [20];
	int   Height, Width;
	int   Orientation;
	int   IsColor;
	int   Process;
	int   FlashUsed;
	float FocalLength;
	int   FocalLength35mmEquiv; // Exif 2.2 tag - usually not present.
	float ExposureTime;
	float ApertureFNumber;
	float Distance;
	float CCDWidth;
	float ExposureBias;
	float DigitalZoomRatio;
	int   Whitebalance;
	int   MeteringMode;
	int   ExposureProgram;
	int   ExposureMode;
	int   ISOequivalent;
	int   LightSource;
	int   DistanceRange;

	float xResolution;
	float yResolution;
	int   ResolutionUnit;

	char  Comments[MAX_COMMENT_SIZE];
	int   CommentWidthchars; // If nonzero, widechar comment, indicates number of chars.

	unsigned ThumbnailOffset;          // Exif offset to thumbnail
	unsigned ThumbnailSize;            // Size of thumbnail.
	unsigned LargestExifOffset;        // Last exif data referenced (to check if thumbnail is at end)

	char  ThumbnailAtEnd;              // Exif header ends with the thumbnail
									   // (we can only modify the thumbnail if its at the end)
	int   ThumbnailSizeOffset;

	int  DateTimeOffsets[MAX_DATE_COPIES];
	int  numDateTimeTags;

	int GpsInfoPresent;
	char GpsLat[31];
	char GpsLong[31];
	char GpsAlt[20];
};

#endif
