/*
 * camera_init.h
 *
 *  Created on: Jun 24, 2011
 *      Author: bmr27715
 */

#ifndef CAMERA_INIT_H_
#define CAMERA_INIT_H_


#include "../typedef.h"
#include "../points_init/points_init.h"
#include "cv.h"
using namespace cv;

#ifndef PI
#define PI 3.14159
#endif

void readCamTextFile(vector<string>&fnames,
						vector<CvMat*>&centers,
						vector<CvMat*>&rots,
						vector<CvMat*>&fovs,
						vector<CvMat*>&dists);

/*int projectionMatFromCor( //Computes P2, the second projection matrix, based on the first and a set of N correspondences
		vector<Point2f> points1, //input
		vector<Point2f> points2, //input
		float focal_length1, //input
		float focal_length2, //input
		CvMat *P1, //input
		CvMat *P2, //output
		int method, //input
		double param1, //input
		double param2 //input
		);*/

int projectionMatFromCor( //Computes P2, the second projection matrix, based on the first and a set of N correspondences
		vector<Point2f> points1, //input
		vector<Point2f> points2, //input
		float focal_length1, //input
		float focal_length2, //input
		float pixel_centerx, //input
		float pixel_centery, //input
		CvMat * &P2, //output
		CvMat * status,
		int method, //input
		double param1, //input
		double param2 //input
		);

int projectionMatFromCor( //Computes P2 relative to P1
		vector<Point2f> points1, //input
		vector<Point2f> points2, //input
		float focal_length1, //input
		float focal_length2, //input
		float pixel_centerx, //input
		float pixel_centery, //input
		CvMat * P1, //input
		CvMat * &P2, //output
		CvMat * status,
		int method, //input
		double param1, //input
		double param2 //input
		);

void printCvMat(CvMat* m, int r, int c);
void printCvMat(CvMat* m);
#endif /* CAMERA_INIT_H_ */
