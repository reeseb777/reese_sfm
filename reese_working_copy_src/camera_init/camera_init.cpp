/*
 * camera_init.cpp
 *
 *  Created on: Jun 23, 2011
 *      Author: ngr27187
 */
#include "camera_init.h"

void readCamTextFile(vector<string>&fnames,
						vector<CvMat*>&centers,
						vector<CvMat*>&rots,
						vector<CvMat*>&fovs,
						vector<CvMat*>&dists){
	char filename[]="images/dino_data/camera_data.txt";
	unsigned num_cams=36;
	vector<string> cam_fnames;
	vector<CvMat*> cam_centers;
	vector<CvMat*> cam_rots;
	vector<CvMat*> cam_fovs;
	vector<CvMat*> cam_dists;

	for(unsigned i=0; i<num_cams; i++){
		CvMat *center = cvCreateMat(3,1,CV_32FC1);
		cam_centers.push_back(cvCloneMat(center));

		CvMat *rot = cvCreateMat(3,3,CV_32FC1);
		cam_rots.push_back(cvCloneMat(rot));

		CvMat *fov = cvCreateMat(2,1,CV_32FC1);
		cam_fovs.push_back(cvCloneMat(fov));

		CvMat *dist = cvCreateMat(2,1,CV_32FC1);
		cam_dists.push_back(cvCloneMat(dist));
	}
	FILE* file = fopen(filename, "r" );
	char str[50];
	float num;
	for(int i=0; i<36; ++i){
		fscanf(file, "%s", str);
		//cam_fnames.push_back(str);
		for(int j=0; j<3; ++j){
			fscanf(file, "%f", &num);
			cvmSet(cam_centers[i],j,0,num);
		}
		//GLfloat A[3][3];			// copy used to compute matrix inverse
		for(int j=0; j<9; ++j){
			fscanf(file, "%f", &num);
			cvmSet(cam_rots[i],j/3,j%3,num);
			//printf("%f ", num);
			//if(j%3==2) printf("\n");
		}
		//printf("\n");
		for(int j=0; j<2; ++j){
			fscanf(file, "%f", &num);
			cvmSet(cam_fovs[i],j,0,num);
		}

		for(int j=0; j<2; ++j){
			fscanf(file, "%f", &num);
			cvmSet(cam_dists[i],j,0,num);
		}
		//printf("\n");
	}
	fnames=cam_fnames;
	centers=cam_centers;
	rots=cam_rots;
	fovs=cam_fovs;
	dists=cam_dists;
}

#ifdef MAIN_CAMERA_INIT
int main( int argc, char** argv ) {

	float dist = 1.0; // distance between axis of rotation of the cameras and the camera centers
	unsigned num_cams = 36;
	float elev_angle = 22.0/180.0*PI;
	float inc_rot_angle = 10.0/180.0*PI;
	char filename[] = "images/dino_data/camera_data.txt";
	vector<CvMat*> cam_centers;
	vector<CvMat*> cam_rots;
	vector<CvMat*> cam_fovs;
	vector<CvMat*> cam_dists;

	for(unsigned i=0; i<num_cams; i++){
		CvMat *center = cvCreateMat(3,1,CV_32FC1);
		cam_centers.push_back(cvCloneMat(center));

		CvMat *rot = cvCreateMat(3,3,CV_32FC1);
		cam_rots.push_back(cvCloneMat(rot));

		CvMat *fov = cvCreateMat(2,1,CV_32FC1);
		cam_fovs.push_back(cvCloneMat(fov));

		CvMat *dist = cvCreateMat(2,1,CV_32FC1);
		cam_dists.push_back(cvCloneMat(dist));
	}
	// initialize first camera
	cvmSet(cam_centers[0],0,0, 0.0);
	cvmSet(cam_centers[0],1,0, cos(elev_angle)*dist);
	cvmSet(cam_centers[0],2,0, sin(elev_angle)*dist);

	CvMat *init_rot = cvCreateMat(3,3,CV_32FC1);
	cvSetIdentity(init_rot);
	cvmSet(init_rot,1,1, cos(elev_angle+PI/2.0));
	cvmSet(init_rot,1,2, -sin(elev_angle+PI/2.0));
	cvmSet(init_rot,2,1, sin(elev_angle+PI/2.0));
	cvmSet(init_rot,2,2, cos(elev_angle+PI/2.0));

	CvMat *inc_rot = cvCreateMat(3,3,CV_32FC1);
	cvSetIdentity(inc_rot);
	cvmSet(inc_rot,0,0, cos(inc_rot_angle));
	cvmSet(inc_rot,0,1, -sin(inc_rot_angle));
	cvmSet(inc_rot,1,0, sin(inc_rot_angle));
	cvmSet(inc_rot,1,1, cos(inc_rot_angle));

	cam_rots[0] = cvCloneMat(init_rot);
	for(unsigned i=1; i<num_cams; i++){
		cvMatMul(inc_rot,cam_centers[i-1],cam_centers[i]);
		cvMatMul(inc_rot,cam_rots[i-1],cam_rots[i]);
		cam_fovs[i] = cvCloneMat(cam_fovs[i-1]);
		cam_dists[i] = cvCloneMat(cam_dists[i-1]);
	}

	FILE *fp = fopen(filename,"w");
	for(unsigned i=0; i<num_cams; i++){
		fprintf(fp,"dino%02d.jpg ",i);
		for(unsigned j=0; j<3; j++)
			fprintf(fp,"%f ",cvmGet(cam_centers[i],j,0));
		for(unsigned j=0; j<9; j++)
			fprintf(fp,"%f ",cvmGet(cam_rots[i],j/3,j%3));
		for(unsigned j=0; j<2; j++)
			fprintf(fp, "%f ",cvmGet(cam_fovs[i],j,0));
		for(unsigned j=0; j<2; j++)
			fprintf(fp, "%f ",cvmGet(cam_dists[i],j,0));
		fprintf(fp,"\n");

	}


}

#endif



int projectionMatFromCor(
		vector<Point2f> points1, //input
		vector<Point2f> points2, //input
		float focal_length1, //input
		float focal_length2, //input
		float pixel_centerx, //input
		float pixel_centery, //input
		CvMat * &P2, //output
		CvMat * status,
		int method=CV_FM_RANSAC, //input
		double param1=1., //input
		double param2=0.9999 //input
		)
{
	//Computes the second projection matrix, based on the first and a set of N correspondences
	//

	/* Inputs:
	 *	points1 - a vector of the correspondance point locations for the first image (points1.size() >=8 needed)
	 *	points2 - a vector of the correspondance point locations for the second image (points2.size() == points1.size())
	 *	focal_length1/2 - use this to specify focal lengths
	 *		technically, the solution only depends on the ratio of these two numbers is
	 *		(scale will remain arbitrary) so setting these both to 1 is valid if focal
	 *		lengths are equal.
	 *	P1 - projection matrix P1 = [R1 | t1] for the first view (P2 will be computed
	 *		relative to this one)
	 *	method - can be CV_FM_RANSAC, CV_FM_8POINT, or CV_FM_LMEDS
	 *	param1 - related to # of standard deviations to exclude as outliers
	 *	param2 -
	 */

	/* Outputs:
		 *	P2 - projection matrix P2 = [R2 | t2] for the second view (P2 will be computed
		 *		relative to P1)
		 */


		//transfer the vector of points to the appropriate opencv matrix structures
			int i1;
			int numPoints =points1.size();
			CvMat* pts1;
			CvMat* pts2;
			CvMat* fundMatr;
			pts1 = cvCreateMat(2,numPoints,CV_32F);
			pts2 = cvCreateMat(2,numPoints,CV_32F);
			status = cvCreateMat(1,numPoints,CV_8UC1);
			for ( i1 = 0; i1 < numPoints; i1++) {

				cvSetReal2D(pts1,0,i1,points1[i1].x);//-pixel_centerx);
				cvSetReal2D(pts1,1,i1,points1[i1].y);//-pixel_centery);

				cvSetReal2D(pts2,0,i1,points2[i1].x);//-pixel_centerx);
				cvSetReal2D(pts2,1,i1,points2[i1].y);//-pixel_centery);
			}

			//create the output fundamental matrix
			fundMatr = cvCreateMat(3,3,CV_32F);

			//see opencv manual for other options in computing the fundamental matrix
			int num = cvFindFundamentalMat(pts1,pts2,fundMatr,method,param1,param2,status);

#ifdef DEBUG
			if( num == 1 )
			{
				printf("Fundamental matrix was found\n");
				printCvMat(fundMatr, 3, 3);
			}
			else
			{
				printf("Fundamental matrix was not found\n");
				return -1;

			}
#endif
#ifdef DEBUG2
			for( int i = 0; i < numPoints; i++ )
						{
							// Computing x2'*F*x1 as a diagnostic step -- should be approximately zero
							CvMat *x1, *x1T, *x2, *x2T, *Fx, *xFx;
							x1 = cvCreateMat(3,1,CV_32F);
							x2 = cvCreateMat(3,1,CV_32F);
							x1T = cvCreateMat(1,3,CV_32F);
							x2T = cvCreateMat(1,3,CV_32F);
							xFx = cvCreateMat(1,1,CV_32F);
							Fx = cvCreateMat(3,1,CV_32F);
							cvSetReal2D(x1,0,0,points1[i].x);
							cvSetReal2D(x1,1,0,points1[i].y);
							cvSetReal2D(x1,2,0,1);
							cvSetReal2D(x2,0,0,points2[i].x);
							cvSetReal2D(x2,1,0,points2[i].y);
							cvSetReal2D(x2,2,0,1);
							cvTranspose(x1, x1T);
							cvTranspose(x2, x2T);
							cvMatMul(fundMatr,x1,Fx);//F*x1;
							cvMatMul(x2T,Fx,xFx);//x2.t()*Fx;
							//printf("x1=%f, y1=%f\n", cvmGet(x1,0,0), cvmGet(x1,1,0));
							printCvMat(xFx,1,1);
			}
#endif
			CvMat *temp33; // 3x3 matrix that is recycled to do computations
						temp33 = cvCreateMat(3,3,CV_32F);
						bool SYMMETRIC_K=true; // convenience variable to skip taking transpose

						CvMat *K1, *K2; //camera matrix
						K1 = cvCreateMat(3,3,CV_32F);
						K2 = cvCreateMat(3,3,CV_32F);
						cvSetIdentity(K1);
						cvSetIdentity(K2);
						float m1=0, m2=0; // aspect ratio term
						float h1[]={0,0}, h2[]={0,0};
						//set the elements of K1
						cvmSet(K1,0,0,focal_length1);
						cvmSet(K1,1,1,focal_length1*(1+m1));
						cvmSet(K1,0,2,h1[0]);
						cvmSet(K1,1,2,h1[1]);
						//set the elements of K2
						cvmSet(K2,0,0,focal_length2);
						cvmSet(K2,1,1,focal_length2*(1+m2));
						cvmSet(K2,0,2,h2[0]);
						cvmSet(K2,1,2,h2[1]);

						CvMat *E; //essential matrix
						E = cvCreateMat(3,3,CV_32F);

						// Computes Essential Matrix E=K2'*F*K1
						cvMatMul(fundMatr,K1,temp33);
						if(SYMMETRIC_K){
							cvMatMul(K2,temp33,E);
						}else{
							cvTranspose(K2,E);
							cvMatMul(E,temp33,E);
						}

						CvMat *Z,*W; // 3 by 3 skew symmetric matrices used for computing S and R

						int W_SIGN_SWAP=1; // =-1;  For keeping track of the transpose of W
						int Z_SIGN_SWAP=1; // =-1;  For keeping track of the transpose of Z
						int E_SIGN_SWAP=1;
						Z = cvCreateMat(3,3,CV_32F);
						W = cvCreateMat(3,3,CV_32F);

						// set elements of Z and W
						/*
						 * W = [0,-1,0
						 * 		1, 0,0
						 * 		0, 0,1]
						 *
						 * Z = [ 0, 1,0
						 * 		-1, 0,0
						 * 		 0, 0,0]
						 */
						for(int i=0;i<3;++i)
							for(int j=0;j<3;++j){
								if(i==1 && j==0){
									cvmSet(Z,i,j,-1*Z_SIGN_SWAP);
									cvmSet(W,i,j,1*W_SIGN_SWAP);
								}
								else if(i==0 && j==1){
									cvmSet(Z,i,j,1*Z_SIGN_SWAP);
									cvmSet(W,i,j,-1*W_SIGN_SWAP);
								}
								else if(i==2 && j==2){
									cvmSet(Z,i,j,0);
									cvmSet(W,i,j,1);
								}
								else{
									cvmSet(Z,i,j,0);
									cvmSet(W,i,j,0);
								}
							}
						CvMat *SVD_diag; //SVD term
						CvMat 	*R, //rotation matrix
								*S, //translation cross product matrix
								*U, // SVD term
								*UT,// SVD term
								*VT,// SVD term
								*V; // SVD term
									// 3 by 3 matrices used for computing Projection Matrices
						R = cvCreateMat(3,3,CV_32F);
						S = cvCreateMat(3,3,CV_32F);
						U = cvCreateMat(3,3,CV_32F);
						V = cvCreateMat(3,3,CV_32F);
						UT = cvCreateMat(3,3,CV_32F);
						VT = cvCreateMat(3,3,CV_32F);
						SVD_diag = cvCreateMat(3,3,CV_32F);

						cvSVD(E,SVD_diag, UT, VT, CV_SVD_U_T | CV_SVD_V_T);
						cvTranspose(UT,U);
						cvTranspose(VT,V);
#ifdef DEBUG2
							printf("Original E:\n");
							printCvMat(E, 3, 3);
							printf("SVD Product:\n");
							cvMatMul(SVD_diag,VT,temp33);
							cvMatMul(U,temp33,temp33);
							printCvMat(temp33, 3, 3);
#endif
						cvMatMul(Z,VT,temp33);
						cvMatMul(V,temp33,S);
						cvMatMul(W,VT,temp33);
						cvMatMul(U,temp33,R);


						/* There will be two valid rotation solutions:
						 * 	one derived from W and the other derived from W^-1 = W'
						 * 	Only one of these two will yield points that are physically
						 * 	in front of both cameras.  The cheriality check to pick the
						 * 	correct rotation is simple: one will be a small (<90 deg) rotation,
						 * 	while the other will be a large (>90) rotation.  Take the trace
						 * 	of the matrix: the correct rotation should have trace > 1 (or, with noisy data, aproximately 1)
						 */
						float R_det_approx=cvDet( R );
						if(R_det_approx < 0){
#ifdef DEBUG2
							printf("Original R (det=%f):\n", R_det_approx);
							printCvMat(R, 3, 3);
							printf("Old U:\n");
							printCvMat(U, 3, 3);
#endif
							E_SIGN_SWAP=-1;
							for(int r=0;r<3;++r){
								for(int c=0;c<3;++c)
									cvmSet(U,r,c,-cvmGet(U,r,c)); // we want to change the sign of E but since E = U*diag*V^T, flipping the sign of U does the same thing
								}
							cvTranspose(U,UT);
							cvMatMul(W,VT,temp33);
							cvMatMul(U,temp33,R);
#ifdef DEBUG2
							printf("New U:\n");
							printCvMat(U, 3, 3);
							R_det_approx=cvDet( R );//cvmGet(R,0,0)*cvmGet(R,1,1)*cvmGet(R,2,2);
							printf("New det(R) = %f\n", R_det_approx);
#endif
						}

						float R_trace=cvmGet(R,0,0)+cvmGet(R,1,1)+cvmGet(R,2,2);
						if(R_trace < 1){ // trace > 1 means angle is less than 90 degrees
#ifdef DEBUG2
							printf("Old R:\n");
							printCvMat(R, 3, 3);
							printf("Old W:\n");
							printCvMat(W, 3, 3);
#endif
							W_SIGN_SWAP*=-1;
							cvmSet(W,1,0,-cvmGet(W,1,0));
							cvmSet(W,0,1,-cvmGet(W,0,1));
#ifdef DEBUG2
							printf("W:\n");
							printCvMat(W, 3, 3);
							printf("Z:\n");
							printCvMat(Z, 3, 3);
#endif
							cvMatMul(W,VT,temp33);
							cvMatMul(U,temp33,R);
							R_trace=cvmGet(R,0,0)+cvmGet(R,1,1)+cvmGet(R,2,2);
						}

						if(E_SIGN_SWAP*W_SIGN_SWAP==-1){
							Z_SIGN_SWAP*=-1;
							cvmSet(Z,1,0,-cvmGet(Z,1,0));
							cvmSet(Z,0,1,-cvmGet(Z,0,1));
							cvMatMul(Z,VT,temp33);
							cvMatMul(V,temp33,S);
						}
						/* There will be infinitely many translation solutions, because any
						 * 	scalar times the computed translation vector is also a valid solution.
						 */
						//float SVD_scale=( cvmGet(SVD_diag,0,0)+cvmGet(SVD_diag,1,1) )/2;
#ifdef DEBUG
						printf("\nEssential Matrix:\n");
						printCvMat(E, 3, 3);
						printf("SVD:\n");
						printCvMat(U, 3, 3);
						printCvMat(SVD_diag, 3, 3);
						printCvMat(VT, 3, 3);
						printf("S:\n");
						printCvMat(S, 3, 3);
						printf("R:\n");
						printCvMat(R, 3, 3);
						if(W_SIGN_SWAP < 0){
							printf("Tr(R) = %f.  W transposed for correct cheriality.", R_trace);
						}
						else{
							printf("Tr(R) = %f.", R_trace);
						}
						if(E_SIGN_SWAP < 0) printf("  E negated for correct sign of determinant.");
						printf("\nAngle(R) = %f", 180/PI*acos((R_trace-1)/2) );
						printf("\n");
#endif
						P2=cvCreateMat(3,4,CV_32F);
						for(int i=0; i<3; ++i){
							for(int j=0; j<3; ++j){
								cvmSet(P2,i,j,cvmGet(R,i,j));
							}
						}
						cvmSet(P2,0,3,-30*cvmGet(S,1,2)); //  			S = [	0	-z	y
						cvmSet(P2,1,3, 30*cvmGet(S,0,2));  //					z	0	-x
						cvmSet(P2,2,3,-30*cvmGet(S,0,1)); //					-y	x	0	]

					return 1; // finished successfully
}

int projectionMatFromCor( //Computes P2 relative to P1
		vector<Point2f> points1, //input -- could include outliers
		vector<Point2f> points2, //input
		float focal_length1, //input
		float focal_length2, //input
		float pixel_centerx, //input -- should be relative to CENTER of image
		float pixel_centery, //input
		CvMat * P1, //input
		CvMat * &P2, //output
		CvMat * status, //output
		int method=CV_FM_RANSAC, //input
		double param1=1., //input
		double param2=0.99 //input
		){

	int num=projectionMatFromCor(points1, points2, focal_length1, focal_length2, pixel_centerx, pixel_centery, P2, status, method, param1, param2);
	CvMat *transf;
	transf=cvCreateMat(4,4,CV_32F);
	for(int r=0; r<3; ++r)	// Copy to 4x4
		for(int c=0;c<4; ++c)
			cvmSet(transf,r,c,cvmGet(P2,r,c));
	cvmSet(transf,3,0,0);	// set last row to identity
	cvmSet(transf,3,1,0);
	cvmSet(transf,3,2,0);
	cvmSet(transf,3,3,1);
	//printCvMat(P1);
	//printCvMat(transf);
	cvMatMul(P1,transf,P2);			// perform transformation P2 on the matrix P1
	//for(int r=0; r<3; ++r)	// Copy to 3x4
	//	for(int c=0;c<4; ++c)
	//		cvmSet(P2,r,c,cvmGet(transf,r,c));
	return num;
}
void printCvMat(CvMat *m, int r, int c){
	printf(" [");
	for(int i=0; i<r; ++i){
		for(int j=0; j<c; ++j){
			printf("\t%f", cvmGet(m,i,j));
		}
		if(i==r-1) printf(" ]");
		printf("\n");
	}
}
void printCvMat(CvMat *m){
	printCvMat(m, m->rows, m->cols);
}
