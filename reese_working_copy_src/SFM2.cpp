//============================================================================
// Name        : SFM2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "cv.h"
#include "highgui.h"
#include "params.h"
#include "sfmDirectory.h"
#include "sfmimage.h"
#include "sfmExifExtractor.h"
#include "sfmViewImages.h"
#include "sfmFeatureDetector.h"
#include "sfmImageFeatures.h"
#include "sfmFeatureMatch.h"
#include "3D/pcloud_viewer.h"
#include "camera_init/camera_init.h"
#include "points_init/points_init.h"
#include "typedef.h"
#ifdef MAIN_SFM2_CPP

// all the new API is put into "cv" namespace. Export its content
using namespace cv;

int main( int argc, char** argv ) {

	string script_fname;

	script_fname = argv[1]; // linux: use filename is the command line argument

	Params sim( script_fname ); //copies configuration data into structure
	printf("Reading script file...\n");
	bool ExitFlag = false;
	while ( !ExitFlag )
	{
		ExitFlag = sim.RunScriptFile(); //runs each line of the configuration data

		if ( !ExitFlag )
		{
			break;
		}
	}

	SfmDirectory imgdir;
	printf("Reading directory of images...\n");
	imgdir.ReadDirectory(sim.imgDir); // read in the directory of images
	int img_width, img_height;

	for(int iter=0; iter<27; ++iter){							/////////////////////////////////////////////<<<<<<<<<<<
		 // feature matching object
			SfmFeatureMatch myFeatMatch(&sim);
			FlannBasedMatcher matcher; // feature matching object
			vector< vector<KeyPoint> > allKeypoints;
			vector<Mat> allDescriptors;
			uvec ids;
			SfmFeatureMatchViewer myFeatMatchView(&sim); // viewing object for the feature matches


			// construct a matched-image graph, represented as a vector of indices
			vector<vector<int> > image_tree;

			printf("Starting feature detection...\n");
			unsigned image_num=iter+1;
			unsigned skip=4; // take every nth frame			/////////////////////////////////////////////<<<<<<<<<<<
			if(image_num*skip > imgdir.files.size()) image_num=imgdir.files.size()/skip;
			//for(unsigned i=0; i<imgdir.files.size(); i++){
			for(unsigned i=0; i<image_num; i++){
				printf("\tImporting image %d of %d: %s\n",i,(int)imgdir.files.size(),imgdir.files[i].data());
				// import image from file
				SfmImage img(imgdir.files[i*skip]);
				img.imID = i; //assign ID number

				img_width=img.Width;
				img_height=img.Height;


				// view original image
				SfmViewImages imgViewer(&sim);
				imgViewer.ImportImage(&img);
				imgViewer.ViewImage();
				// copy original image into feature  Match Viewer
				myFeatMatchView.ImportImage(&img);
				// copy original image into Match Connectivity Viewer

				printf("\tExtracting EXIF tag info\n");
				// extractor EXIF tag info
				SfmExifExtractor exif;
				exif.Extract(&img); // extract EXIF tag information
				printf("Img width: %d\n",img_width);

				// detect features
				printf("\tDetecting features\n");

				SfmImageFeatures myFeats(img.imID);
				FeatureDetector *fdetector=(FeatureDetector*)( sim.getFeatureDetector() );
				DescriptorExtractor *dextractor=(DescriptorExtractor*)(sim.getDescriptorExtractor());
				Mat mask;
				Mat descriptors;
				vector<KeyPoint> keypoints;
				(*fdetector).detect(Mat(img.imgData,true),keypoints,mask);
				(*dextractor).compute(Mat(img.imgData,true),keypoints,descriptors);
				allKeypoints.push_back(keypoints);
				allDescriptors.push_back(descriptors);

					CvMemStorage* featureStorage = cvCreateMemStorage(0);
					CvSeq * featureSeq = cvCreateSeq(CV_SEQ_ELTYPE_POINT, sizeof(CvSeq),
					sizeof(CvPoint), featureStorage);
					cvSeqPushMulti( featureSeq, &keypoints, keypoints.size() );
					CvMemStorage* descriptorStorage = cvCreateMemStorage(0);
					CvSeq * descriptorSeq = cvCreateSeq(CV_SEQ_ELTYPE_POINT, sizeof(CvSeq),
					sizeof(CvPoint), descriptorStorage);
					cvSeqPush( descriptorSeq, &descriptors );
					myFeats.descriptors=descriptorSeq;
					myFeats.features=featureSeq;

				ids.push_back(img.imID);
				printf("%d keypoints found!\n", (int)keypoints.size());

				// view image used for feature detector and features overlaid
				SfmFeatureViewer imgFeatureViewer(&sim);
				imgFeatureViewer.ImportData(img.imgData, img.imID, keypoints);
				imgFeatureViewer.ViewFeatures();

				// copy features into feature Match Viewer
					myFeatMatchView.ImportKeyPoints(img.imID, keypoints);

				// add features to matching object
					myFeatMatch.AddImageFeatures(&myFeats);


			}

			printf("Completed feature detection\n");
			printf("Starting feature matching\n");
			// Match features


					/*for(unsigned k=1;k<allDescriptors.size();++k){//
						vector< vector<DMatch> > matches;
						matcher.knnMatch(allDescriptors[3], allDescriptors[k], matches, sim.ANNk);
						//for(unsigned j=0;j<matches.size();++j)
						//	printf("Match1 %d: %f\n", j, matches[j][0].distance);
						//printf("\n\n");
						printf(" -- Images %d and %d --\n",3,k);
						unsigned count=0;
						for(unsigned fnum=0;fnum<matches.size();++fnum){
							if(matches[fnum][0].distance < sim.MatchRatio * matches[fnum][1].distance && k!=3){
								printf("  - Feature %d -\n",fnum);
								for(unsigned j=0;j<matches[fnum].size();++j){
									printf("Match %d: %f\n", j, matches[fnum][j].distance);
								}
								++count;
							}
						}
						printf("%d matches\n\n",count);
					}*/
			myFeatMatch.consecutive=2;
			myFeatMatch.MatchAll_(matcher, allDescriptors);
			printf("Completed feature matching\n");

			// copy results into feature match viewer
				myFeatMatchView.ImportFeatureMatches(&myFeatMatch);

			// view matches
				myFeatMatchView.View();





		// add bundle adjust


				// initialize point locations
				vector<string> cam_fnames;
				vector<CvMat*> cam_centers;
				vector<CvMat*> cam_rots;
				vector<CvMat*> cam_fovs;
				vector<CvMat*> cam_dists;
				readCamTextFile(cam_fnames, cam_centers, cam_rots, cam_fovs, cam_dists);
				vector<vector<GLfloat> > pcloud;
				char pfname[100];
				sprintf(pfname, "images/dino_data/point_data%d.txt",iter);
				char avgfname[100];
				sprintf(avgfname, "images/dino_data/point_data_averaged%d.txt",iter);
				pcloud=average_points(myFeatMatch,
						solve_points(myFeatMatch, myFeatMatchView , cam_centers, cam_rots, 50, img_width, img_height,
												allKeypoints, pfname), avgfname );

				//init_openGL(argc, argv);
				//drawPointCloud(pcloud, PI/100/4, pcloud.size());
				//readCameraTextFile();
				//start_openGL();
	}
	return 0;
}
#endif
