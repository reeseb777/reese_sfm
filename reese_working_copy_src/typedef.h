#ifndef _TYPEDEF_H_
#define _TYPEDEF_H_

#include <vector>
#include <string.h>
#include "highgui.h"
using namespace std;

#define MAIN_SFM // will use the main function in main.cpp
//#define MAIN_SFM2_CPP // will use the main function in SFM2.cpp
//#define MAIN_PCLOUD_VIEWER // will use the main function in pcloud_viewer.cpp
//#define MAIN_CAMERA_INIT // will use the main function in camera_init.cpp

//#define FEATURE_MATCHING_SANITY_CHECK

#define PBA_NO_GPU
#define DISABLE_CPU_SSE

#define DEBUG // enable all debug print statements
#define DEBUG2 // enable all debug print statements

#define DEG2RAD 0.0174532925
#define RAD2DEG 57.2957795

typedef unsigned char uchar;
typedef vector<uchar> ucvec;
typedef vector<string> svec;
typedef vector<int> ivec;
typedef vector<unsigned> uvec;
typedef vector<uvec> umat;
typedef vector<ivec> imat;
typedef vector<float> fvec;
typedef vector<fvec> fmat;

typedef float SfmType;

typedef struct {
	unsigned id1; // image 1 id
	unsigned id2; // image 2 id
	int f1;  // feature id from image 1
	int f2;  // feature id from image 2
}SfmMatch;

typedef struct {
	uvec cam_ids; // ids of the camera that view the match
	uvec kpt_ids; // ids of the keypoints associated with the camera view
}SfmMatch_;

// Relating to EXIF Tag Functions
#ifndef TRUE
    #define TRUE 1
    #define FALSE 0
#endif
//--------------------------------------------------------------------------
// This structure is used to store jpeg file sections in memory.
typedef struct {
    uchar *  Data;
    int      Type;
    unsigned Size;
}Section_t;

typedef enum {
    READ_METADATA = 1,
    READ_IMAGE = 2,
    READ_ALL = 3
}ReadMode_t;

typedef struct {
	unsigned short Tag;
	string Desc;
}TagTable_t;

#endif
